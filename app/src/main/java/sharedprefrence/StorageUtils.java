package sharedprefrence;

import android.content.Context;
import android.content.SharedPreferences;

public class StorageUtils {
    private String myprefrence = "Details";
    SharedPreferences sharedPreferences;

    public void setDetails(Context context, String key, String value) {
        sharedPreferences = context.getSharedPreferences(myprefrence, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public String getDetails(Context context, String key) {
        sharedPreferences = context.getSharedPreferences(myprefrence, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public void clearSharedPrefrence(Context context) {
        sharedPreferences = context.getSharedPreferences(myprefrence, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }
}
