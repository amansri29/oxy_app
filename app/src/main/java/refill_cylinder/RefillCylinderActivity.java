package refill_cylinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityRefillCylinderBinding;

import java.util.ArrayList;

import adapters.RefilCylinderAdapter;
import listener.CartQtyIncrementDecrement;
import modal.CylinderRefilModal;
import place_order.PlaceOrderScheduleActivity;
import place_order.PlacedOrderAddressActivity;

public class RefillCylinderActivity extends AppCompatActivity {
    ActivityRefillCylinderBinding binding;
    ArrayList<CylinderRefilModal> arrayList;
    RefilCylinderAdapter cartAdapter;
    int quantity = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_refill_cylinder);

        arrayList = new ArrayList<>();

        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RefillCylinderActivity.this, Refillschedule.class));
            }
        });

        arrayList.add(new CylinderRefilModal("1", "Oxygen Cylinder",
                "400", "3", "C"));

        binding.refilRecycler.setHasFixedSize(true);

        cartAdapter = new RefilCylinderAdapter(this, arrayList, new CartQtyIncrementDecrement() {
            @Override
            public void quantityIncrement(int position, TextView qty, String price) {
//                String totalQty = arrayList.get(position).getQuantiy();
                quantity = Integer.parseInt(qty.getText().toString());
                quantity += 1;
                if (quantity > 10) {
                    Toast.makeText(RefillCylinderActivity.this, "Maximum quantiaty of this product is 10 !", Toast.LENGTH_SHORT).show();
//                    maxiMumLimitAlert();
                } else {
                    qty.setText(String.valueOf(quantity));
                }
            }

            @Override
            public void quantityDecrement(int position, TextView qty, String price) {
                quantity = Integer.parseInt(qty.getText().toString());
                quantity -= 1;
                if (quantity < 1) {
                    Toast.makeText(RefillCylinderActivity.this, "Minimum quantiaty of this product is 1 !", Toast.LENGTH_SHORT).show();
                } else {
                    qty.setText(String.valueOf(quantity));
                }
            }

            @Override
            public void finalPrice(int finalPrice) {

            }
        });
        binding.refilRecycler.setAdapter(cartAdapter);

    }
}