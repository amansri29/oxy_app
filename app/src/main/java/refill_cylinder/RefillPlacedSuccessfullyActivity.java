package refill_cylinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityRefillPlacedSuccessfullyBinding;

public class RefillPlacedSuccessfullyActivity extends AppCompatActivity {
    ActivityRefillPlacedSuccessfullyBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_refill_placed_successfully);

        binding.btnGoToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RefillPlacedSuccessfullyActivity.this, MainActivity.class));
            }
        });
    }
}