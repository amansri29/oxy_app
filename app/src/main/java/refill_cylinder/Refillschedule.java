package refill_cylinder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityRefillCylinderBinding;
import com.spacetexhdemo.databinding.ActivityRefillscheduleBinding;

import java.util.ArrayList;

import adapters.RefillDateAdapter;
import modal.DeliveryDateModal;

public class Refillschedule extends AppCompatActivity {
    ActivityRefillscheduleBinding binding;
    RefillDateAdapter refillDateAdapter;
    ArrayList<DeliveryDateModal> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_refillschedule);
        arrayList = new ArrayList<>();

        binding.tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Refillschedule.this, RefillPaymentActivity.class));
            }
        });

        binding.navswRepeatDelivery.setOnClickListener(v -> {
            if (binding.navswRepeatDelivery.isChecked()) {
                binding.repeatQuantatyLayout.setVisibility(View.VISIBLE);
                binding.addressDateLayout.setVisibility(View.VISIBLE);
//                textView_onOff.setText(R.string.Online);
            } else {
                binding.repeatQuantatyLayout.setVisibility(View.GONE);
                binding.addressDateLayout.setVisibility(View.GONE);
//                textView_onOff.setText(R.string.Offline);
            }
        });

//        arrayList.add(new DeliveryDateModal("14 May",""));
//        arrayList.add(new DeliveryDateModal("15 May"));
//        arrayList.add(new DeliveryDateModal("16 May"));
//        arrayList.add(new DeliveryDateModal("17 May"));
//        arrayList.add(new DeliveryDateModal("18 May"));
//        arrayList.add(new DeliveryDateModal("19 May"));

        binding.refillDateRecycler.setHasFixedSize(true);
        refillDateAdapter = new RefillDateAdapter(this, arrayList);
        binding.refillDateRecycler.setAdapter(refillDateAdapter);
    }
}