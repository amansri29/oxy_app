package retrofit;

import com.google.gson.JsonObject;

import java.util.List;

import modal.AllAddressModal;
import modal.AllDealersModal;
import modal.AllOrdersGetModal;
import modal.OTPSendModal;
import modal.OTPSubmitModal;
import modal.Order;
import modal.PaymentResponceTokenModal;
import modal.PlacedOrderModal;
import modal.UpdateOrder;
import modal.UpdateOrderPaymentId;
import modal.UserDetailsModal;
import modal.UserRegisterAddressModal;
import okhttp3.MultipartBody;
import retrofit.models.Doctor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    @POST("order")
    Call<PaymentResponceTokenModal> getPaymentToken(@Header("Content-Type") String deviceReg,
                                                    @Header("x-client-id") String deviceInfo,
                                                    @Header("x-client-secret") String deviceType, @Body JsonObject jsonObject);

    @POST("send-otp/")
    Call<OTPSendModal> getPhonenumberOtp(@Body JsonObject jsonObject);

    @POST("login/")
    Call<OTPSubmitModal> submitPhonenumberOtp(@Body JsonObject jsonObject);

    @POST("address/")
    Call<UserRegisterAddressModal> setUserAddress(@Header("Authorization") String token, @Body JsonObject jsonObject);


    @POST("doctor/")
    Call<Doctor> setDoctorData(@Header("Authorization") String token, @Body Doctor doctor);

    @POST("customer/")
    Call<UserDetailsModal> setUserDetails(@Header("Authorization") String token, @Body JsonObject jsonObject);

    @PATCH("customer/{userDetailsId}/")
    Call<UserDetailsModal> updateCustomerDetails(@Header("Authorization") String token,
                                                 @Body JsonObject jsonObject,
                                                 @Path("userDetailsId") String userDetailsId);

    @Multipart
    @PATCH("customer/{userMedicalReport}/")
    Call<UserDetailsModal> updateMedicalReport(@Header("Authorization") String token,
                                               @Part MultipartBody.Part part
            , @Path("userMedicalReport") String userMedicalReport);

    @GET("dealer/")
    Call<List<AllDealersModal>> getAllDealers(@Header("Authorization") String token);

    @GET("dealer/{userDealerId}/")
    Call<AllDealersModal> getOneDealersDetails(@Header("Authorization") String token
            , @Path("userDealerId") String userDealerId);

    @PATCH("customer/{userDetailsId}/")
    Call<UserDetailsModal> updateDealer(@Header("Authorization") String token,
                                        @Body JsonObject jsonObject, @Path("userDetailsId") String userDetailsId);

    @GET("dealer/{userDetailsId}/")
    Call<AllDealersModal> getAllDealersProduct(@Header("Authorization") String token
            , @Path("userDetailsId") String userDetailsId);

//    @POST("order/")
//    Call<PlacedOrderModal> placedOrder(@Header("Authorization") String token
//            , @Body JsonObject allorderDetails);

    @POST("address/")
    Call<AllAddressModal> saveDefaultAddress(@Header("Authorization") String token
            , @Body JsonObject saveAddress);

    @DELETE("address/{addressId}/")
    Call<List<AllAddressModal>> removeAddress(@Header("Authorization") String token
            , @Path("addressId") String userDetailsId);

    @GET("address/")
    Call<List<AllAddressModal>> getAllSelectedAddress(@Header("Authorization") String token);

    @GET("order/")
    Call<List<AllOrdersGetModal>> getAllorders(@Header("Authorization") String token);

    @GET("order/customer_order/")
    Call<List<AllOrdersGetModal>> getAllOrdersDashboard(@Header("Authorization") String token);

    @GET("order/")
    Call<List<AllOrdersGetModal>> getCurrentDatesorder(@Header("Authorization") String token,
                                                       @Query("expected_delivery_date__lte") String sDate,
                                                       @Query("expected_delivery_date__gte") String eDate);

    @GET("order/")
    Call<List<AllOrdersGetModal>> getLastSevenDayesOrder(@Header("Authorization") String token,
                                                         @Query("expected_delivery_date__lte") String sDate,
                                                         @Query("expected_delivery_date__gte") String eDate);

    @GET("order/")
    Call<List<AllOrdersGetModal>> getLastMonthOrder(@Header("Authorization") String token,
                                                    @Query("expected_delivery_date__lte") String sDate,
                                                    @Query("expected_delivery_date__gte") String eDate);

    @GET("order/")
    Call<List<AllOrdersGetModal>> getDateRangeOrder(@Header("Authorization") String token,
                                                    @Query("expected_delivery_date__lte") String sDate,
                                                    @Query("expected_delivery_date__gte") String eDate);

    @POST("cancle_order/")
    Call<Void> orderCanceled(@Header("Authorization")
                                                        String token, @Body JsonObject orderId);

    @POST("cancle_order/")
    Call<JsonObject> orderCanceledWithMsg(@Header("Authorization")
                                                        String token, @Body JsonObject orderId);


    @POST("order/")
    Call<AllOrdersGetModal> placedOrder(@Header("Authorization") String token
            , @Body Order order);

    @PATCH("order/{orderId}/")
    Call<AllOrdersGetModal> updatePaymentId(@Header("Authorization") String token
            , @Body UpdateOrderPaymentId order, @Path("orderId") int orderId);

    @PATCH("order/{orderId}/")
    Call<AllOrdersGetModal> updateOrdereliveryDate(@Header("Authorization") String token
            , @Body JsonObject orderDate, @Path("orderId") int orderId);

    @GET("order/{orderId}/")
    Call<Order> getOrderDetail(@Header("Authorization") String token
            ,@Path("orderId") String orderId);

    @PATCH("order/{orderId}/")
    Call<Order> updateOrder(@Header("Authorization") String token
            , @Path("orderId") String orderId, @Body UpdateOrder updateOrder);

    @FormUrlEncoded
    @PATCH("order/{orderId}/")
    Call<Order> updateOrderForPickUp(@Header("Authorization") String token, @Path("orderId") int orderID, @Field("return_date") String returnDate, @Field("status") String status);

}
