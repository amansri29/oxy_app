package retrofit;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import modal.PaymentResponceTokenModal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentTokenApiContoller {
    private APIInterface apiInterface;
    private PaymentTokenResponceListener paymentTokenResponceListener;

    public PaymentTokenApiContoller(PaymentTokenResponceListener paymentTokenResponceListener) {
        this.paymentTokenResponceListener = paymentTokenResponceListener;
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getPaymentTokenUrl().create(APIInterface.class);
        }
    }


    public void getPaymentToken(String appId, String appSecreat,
                                String orderID, String amount, String currancy) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("orderId", orderID);
        jsonObject.addProperty("orderAmount", amount);
        jsonObject.addProperty("orderCurrency", currancy);

        Call<PaymentResponceTokenModal> call = apiInterface.getPaymentToken("application/json"
                , appId, appSecreat, jsonObject);
        call.enqueue(new Callback<PaymentResponceTokenModal>() {
            @Override
            public void onResponse(@NotNull Call<PaymentResponceTokenModal> call,
                                   @NotNull Response<PaymentResponceTokenModal> response) {
                PaymentResponceTokenModal detailsModal = response.body();
                if (detailsModal != null) {
                    if (detailsModal.getStatus().equals("OK")) {
                        paymentTokenResponceListener.onSuccess(ApiConstant.PAYMENT_TOKEN,
                                detailsModal);
                    } else {
                        paymentTokenResponceListener.onFailure(detailsModal.getMessage());
                    }
                } else {
                    paymentTokenResponceListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<PaymentResponceTokenModal> call, @NotNull Throwable t) {
                paymentTokenResponceListener.onError(t.getMessage());
            }
        });
    }

}
