package retrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.util.List;

import listener.DealerResponceListener;
import modal.AllOrdersGetModal;
import modal.OTPSendModal;
import modal.OTPSubmitModal;
import modal.Order;
import modal.OrderItem;
import modal.UpdateOrderPaymentId;
import modal.UserDetailsModal;
import modal.UserRegisterAddressModal;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class ApiController {
    private APIInterface apiInterface;
    private ApiResponseListener apiResponseListener;
    private DealerResponceListener dealerResponceListener;
    StorageUtils storageUtils;
    private static final String TAG = "ApiController";

    public ApiController(ApiResponseListener apiResponseListener) {
        this.apiResponseListener = apiResponseListener;
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
    }

    public void getPhoneNumberOtpSend(String number) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", "CUST_" + number);
        Call<OTPSendModal> call = apiInterface.getPhonenumberOtp(jsonObject);
        call.enqueue(new Callback<OTPSendModal>() {
            @Override
            public void onResponse(@NotNull Call<OTPSendModal> call,
                                   @NotNull Response<OTPSendModal> response) {
                OTPSendModal detailsModal = response.body();
                if (detailsModal != null) {
                    if (detailsModal.getMsg().equals("OTP send")) {
                        apiResponseListener.onSuccess(StringUtils.PhoneNumber, detailsModal.getMsg());
                    } else {
                        apiResponseListener.onFailure(detailsModal.getMsg());
                    }
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OTPSendModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void getPhoneNumberOtpSubmit(String number, String otp, StorageUtils storageUtils, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", "CUST_" + number);
        jsonObject.addProperty("password", otp);
        Call<OTPSubmitModal> call = apiInterface.submitPhonenumberOtp(jsonObject);
        call.enqueue(new Callback<OTPSubmitModal>() {
            @Override
            public void onResponse(@NotNull Call<OTPSubmitModal> call,
                                   @NotNull Response<OTPSubmitModal> response) {
                OTPSubmitModal detailsModal = response.body();
                if (detailsModal != null) {
                    storageUtils.setDetails(context, StringUtils.NewUser, String.valueOf(detailsModal.isIs_new_user()));
                    apiResponseListener.onSuccess(String.valueOf(detailsModal.isIs_new_user()), String.valueOf(detailsModal.getKey()));

                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OTPSubmitModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }


    public void setAddress(String basicAddress, String landmark, String city, String areaPincode,
                           String state, String country, double latitude, double longitude, Context context,
                           StorageUtils storageUtils, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("basic_address", basicAddress);
        jsonObject.addProperty("landmark", landmark);
        jsonObject.addProperty("city", city);
        jsonObject.addProperty("pincode", areaPincode);
        jsonObject.addProperty("state", state);
        jsonObject.addProperty("country", country);
        jsonObject.addProperty("latitude", latitude);
        jsonObject.addProperty("longitude", longitude);
        String headerToken = "Token " + token;

        Call<UserRegisterAddressModal> call = apiInterface.setUserAddress(headerToken, jsonObject);
        call.enqueue(new Callback<UserRegisterAddressModal>() {
            @Override
            public void onResponse(@NotNull Call<UserRegisterAddressModal> call,
                                   @NotNull Response<UserRegisterAddressModal> response) {

                Log.i(TAG, "onResponse: " + response.code());

                UserRegisterAddressModal detailsModal = response.body();

                if (detailsModal != null) {
//                    if (detailsModal.isIs_new_user()) {
                    apiResponseListener.onSuccess(StringUtils.Address, String.valueOf(detailsModal.getId()));
//                    ApiController.this.storageUtils.setDetails(context, StringUtils.NewUser,
//                            String.valueOf(detailsModal.isIs_new_user()));
//                    } else {
//                        apiResponseListener.onFailure("Failed");
//                    }
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserRegisterAddressModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
                Log.e(TAG, "onFailure: " +  t.getMessage() );
            }
        });
    }

    public void setUserDetails(String name, String mobileNo,
                               String deafultAddress, String token,
                               String contactPersonName, String contactPersonNumber,
                               String gst) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("contact_person_name", contactPersonName);
        jsonObject.addProperty("contact_person_mobile", contactPersonNumber);
        jsonObject.addProperty("gst", gst);
        jsonObject.addProperty("mobile", mobileNo);
        jsonObject.addProperty("default_address", deafultAddress);

        String headerToken = "Token " + token;

        Call<UserDetailsModal> call = apiInterface.setUserDetails(headerToken, jsonObject);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(@NotNull Call<UserDetailsModal> call,
                                   @NotNull Response<UserDetailsModal> response) {
                UserDetailsModal detailsModal = response.body();
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.UserDetails, String.valueOf(detailsModal.getId()));

                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserDetailsModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void updateAadharCardNumber(String number, String userDetailsid, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("aadhar_number", number);

        String headerToken = "Token " + token;

        Call<UserDetailsModal> call = apiInterface.updateCustomerDetails(headerToken, jsonObject, userDetailsid);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(@NotNull Call<UserDetailsModal> call,
                                   @NotNull Response<UserDetailsModal> response) {
                UserDetailsModal detailsModal = response.body();
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.AadharCardUpdate, String.valueOf(detailsModal.getId()));
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserDetailsModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void updateMedicalReport(MultipartBody.Part part, String userDetailsid, String token) {

        String headerToken = "Token " + token;
        Call<UserDetailsModal> call = apiInterface.updateMedicalReport(headerToken, part, userDetailsid);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(@NotNull Call<UserDetailsModal> call,
                                   @NotNull Response<UserDetailsModal> response) {

                Log.i(TAG, "onResponse: medical report " + response.code());
                UserDetailsModal detailsModal = response.body();
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.UpdateMedicalReport, String.valueOf(detailsModal.getId()));
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserDetailsModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
                Log.e(TAG, "onFailure: medical report " + t.getMessage());
            }
        });
    }

    public void uploadDoctorPrescription(MultipartBody.Part part, String userDetailsid, String token) {

        String headerToken = "Token " + token;
        Call<UserDetailsModal> call = apiInterface.updateMedicalReport(headerToken, part, userDetailsid);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(@NotNull Call<UserDetailsModal> call,
                                   @NotNull Response<UserDetailsModal> response) {
                UserDetailsModal detailsModal = response.body();
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.AadharCardUpdate, String.valueOf(detailsModal.getId()));
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserDetailsModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void updateCylinderDealer(int number, String userDetailsid, String token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("default_dealer", number);

        String headerToken = "Token " + token;

        Call<UserDetailsModal> call = apiInterface.updateDealer(headerToken, jsonObject, userDetailsid);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(@NotNull Call<UserDetailsModal> call,
                                   @NotNull Response<UserDetailsModal> response) {
                UserDetailsModal detailsModal = response.body();
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.UpdateDealer, String.valueOf(detailsModal.getId()));
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserDetailsModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }


    public void placedOrder(String totalAmount, String expectedDeliveryDate
            , String deliveryAddress, String paymentMode, String repeatOrderDays
            , String repeatOrderTill, String token, List<OrderItem> orderItemsList,
                            int paymentId, String placedOrdertype, int selectedDealerId) throws JSONException {


        Log.i(TAG, "placedCodOrder: called");

        Order order = new Order();
        order.setItems(orderItemsList);
        order.setTotal_amount(Double.parseDouble(totalAmount));
        order.setExpected_delivery_date(expectedDeliveryDate);
        order.setRepeat_till(repeatOrderTill);
        order.setDelivery_address(Integer.parseInt(deliveryAddress));
        order.setPayment_mode(paymentMode);
        if (paymentMode.equalsIgnoreCase("Cash on delivery")) {
            order.setStatus("Confirmed");
        } else {
            order.setStatus("Payment Pending");
        }
        Log.i(TAG, "placedOrder: " + placedOrdertype);
        order.setOrder_type(placedOrdertype);
        order.setPayment_id(paymentId);
        order.setOrder_dealer(selectedDealerId);

        String headerToken = "Token " + token;

        Log.e("Order", String.valueOf(order));
        Log.e("orderItemsList", String.valueOf(orderItemsList));

        Call<AllOrdersGetModal> call = apiInterface.placedOrder(headerToken, order);
        call.enqueue(new Callback<AllOrdersGetModal>() {
            @Override
            public void onResponse(@NotNull Call<AllOrdersGetModal> call,
                                   @NotNull Response<AllOrdersGetModal> response) {
                AllOrdersGetModal detailsModal = response.body();
                Log.d("Respoce", String.valueOf(response.code()));
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.UpdateDealer, "Order Placed Success");
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllOrdersGetModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void placedOnlineOrder(String totalAmount, String expectedDeliveryDate
            , String deliveryAddress, String paymentMode, String repeatOrderDays
            , String repeatOrderTill, String token, List<OrderItem> orderItemsList,
                                  int paymentId, String placedOrdertype, int selectedDealerId,
                                  ProgressDialog progressDialog) throws JSONException {

        Order order = new Order();
        order.setItems(orderItemsList);
        order.setTotal_amount(Double.parseDouble(totalAmount));
        order.setExpected_delivery_date(expectedDeliveryDate);
        order.setRepeat_till(repeatOrderTill);
        order.setDelivery_address(Integer.parseInt(deliveryAddress));
        order.setPayment_mode(paymentMode);
        order.setStatus("Payment Pending");
        order.setOrder_type(placedOrdertype);
        order.setPayment_id(paymentId);
        Log.i(TAG, "placedOnlineOrder: dealer id " + selectedDealerId);
        order.setOrder_dealer(selectedDealerId);
        String headerToken = "Token " + token;

        Log.e("Order", String.valueOf(order));
        Log.e("orderItemsList", String.valueOf(orderItemsList));

        Call<AllOrdersGetModal> call = apiInterface.placedOrder(headerToken, order);
        call.enqueue(new Callback<AllOrdersGetModal>() {
            @Override
            public void onResponse(@NotNull Call<AllOrdersGetModal> call,
                                   @NotNull Response<AllOrdersGetModal> response) {
                AllOrdersGetModal detailsModal = response.body();
                Log.d("Respoce", String.valueOf(response.code()));
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(String.valueOf(detailsModal.getId()), String.valueOf(detailsModal.getPayment_token()));
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<AllOrdersGetModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    public void updateOrderPaymentId(int paymentId, String token,
                                     int orderId) throws JSONException {

        UpdateOrderPaymentId updeOrderId = new UpdateOrderPaymentId();
        updeOrderId.setPayment_id(paymentId);

        String headerToken = "Token " + token;

        Log.e("Order", String.valueOf(updeOrderId));

        Call<AllOrdersGetModal> call = apiInterface.updatePaymentId(headerToken, updeOrderId, orderId);
        call.enqueue(new Callback<AllOrdersGetModal>() {
            @Override
            public void onResponse(@NotNull Call<AllOrdersGetModal> call,
                                   @NotNull Response<AllOrdersGetModal> response) {
                AllOrdersGetModal detailsModal = response.body();
                Log.d("Respoce", String.valueOf(response.code()));
                if (detailsModal != null) {
                    apiResponseListener.onSuccess(StringUtils.UpdatePaymentId, "UpdatePaymentId");
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllOrdersGetModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

}
