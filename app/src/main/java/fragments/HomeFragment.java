package fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.SplashActivity;
import com.spacetexhdemo.animater.ViewAnimation;
import com.spacetexhdemo.databinding.FragmentHomeBinding;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adapters.ConfirmedOrderAdapter;
import cart.CartAdapter;
import cart.CartSectionActivity;
import listener.CartQtyIncrementDecrement;
import listener.OrderConfirmedInterface;
import modal.AllDealersModal;
import modal.AllOrdersGetModal;
import refill_cylinder.RefillCylinderActivity;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import select_dealer.SelectDealerActivity;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class HomeFragment extends Fragment {
    FragmentHomeBinding binding;
    ConfirmedOrderAdapter confirmedOrderAdapter;
    boolean isRotate = false;
    Boolean isAllFabsVisible;
    int totalItemCount = 0;
    Context context;
    private APIInterface apiInterface;
    ArrayList<AllOrdersGetModal> arrayList;
    String token;
    StorageUtils storageUtils;
    ProgressDialog progressDialog;
    private Parcelable list_state;
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd-MMM-yy");
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_home, container, false));
        context = getActivity();
        arrayList = new ArrayList<>();
        storageUtils = new StorageUtils();
        token = storageUtils.getDetails(context, StringUtils.UserToken);

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        //progressDialog.show();



        assert binding != null;
        binding.btnPlaceFirstOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CartSectionActivity.class));
            }
        });

        context = getActivity();

        binding.tvrefillcylinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), RefillCylinderActivity.class));
                isRotate = ViewAnimation.rotateFab(view, !isRotate);
                startActivity(new Intent(getActivity(), CartSectionActivity.class));
                storageUtils.setDetails(context, StringUtils.OrderType, "Refill");
                binding.floatingLayout.setVisibility(View.GONE);
            }
        });

        binding.tvOrderNewCylinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRotate = ViewAnimation.rotateFab(view, !isRotate);
                startActivity(new Intent(getActivity(), CartSectionActivity.class));
                storageUtils.setDetails(context, StringUtils.OrderType, "New");
                binding.floatingLayout.setVisibility(View.GONE);
            }
        });

        isAllFabsVisible = false;

        binding.fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRotate = ViewAnimation.rotateFab(view, !isRotate);

                if (isRotate) {
                    binding.floatingLayout.setVisibility(View.VISIBLE);
                    ViewAnimation.showIn(binding.tvrefillcylinder);
                    binding.fabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,
                            R.color.color3B6EFF)));
                    binding.fabAdd.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), PorterDuff.Mode.SRC_IN);

                } else {
                    ViewAnimation.showOut(binding.tvrefillcylinder);

                    binding.floatingLayout.setVisibility(View.GONE);

                    binding.fabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.
                            getColor(context, R.color.color3B6EFF)));
                    binding.fabAdd.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), PorterDuff.Mode.SRC_IN);

                }
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllOrders(token);
    }

    private void getAllOrders(String token) {

        if(!progressDialog.isShowing()){
            progressDialog.show();
        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getAllOrdersDashboard(Token);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                List<AllOrdersGetModal> allDealersModals = response.body();
                List<AllOrdersGetModal.ItemsBean> securityItems = new ArrayList<>();

                for(AllOrdersGetModal obj : allDealersModals){
                    int security_deposit = 0;
                    securityItems.clear();
                    try {
                        if (obj.getReturnDate() != null) {
                            Date d = serverDateFormat.parse(obj.getReturnDate());
                            obj.setPrettyReturnDate(prettyDateFormat.format(d));
                        }
                    }
                    catch (Exception e){

                    }
                    if(obj.getOrder_type().equals("New")) {
                        for (AllOrdersGetModal.ItemsBean item : obj.getItems()) {
                            if (item.getSecurity_deposit() > 0) {
                                securityItems.add(item);
                                security_deposit = security_deposit + (item.getSecurity_deposit() * item.getQuantity());
                            }
                        }
                        obj.setSecurityItems(securityItems);
                        if (security_deposit > 0) {
                            obj.addSecurityDeposit(security_deposit);
                        }
                    }
                }

                if (allDealersModals != null) {
                    binding.allOrdersRecycler.setHasFixedSize(true);
                    confirmedOrderAdapter = new ConfirmedOrderAdapter(getActivity(), allDealersModals, new OrderConfirmedInterface() {
                        @Override
                        public void orderConfirmed(int position) {
//                            binding.tvSelectSchedule.setTextColor(ContextCompat.getColor(getContext(), R.color.color222B45));
//                            binding.tvOutForDelivery.setTextColor(ContextCompat.getColor(getContext(), R.color.color3B6EFF));
                        }

                        @Override
                        public void orderCanceled(int position) {
                            /*ProgressDialog progressDialog1 = new ProgressDialog(context);
                            progressDialog.setTitle("Loading.....");
                            progressDialog.setCancelable(false);
                            progressDialog.show();*/

                            int orderId = allDealersModals.get(position).getId();

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                            builder1.setTitle("Order Canceled ?");
                            builder1.setMessage("Are you sure want to cancel this order?");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ordercanceled(orderId,position);
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setNegativeButton(
                                    "No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    });
                    try {
                        binding.allOrdersRecycler.getLayoutManager().onRestoreInstanceState(list_state);
                    }catch (Exception e){

                    }
                    binding.allOrdersRecycler.setAdapter(confirmedOrderAdapter);
                    totalItemCount = confirmedOrderAdapter.getItemCount();
                    if (totalItemCount == 0) {
                        binding.placeFirstOrderLayout.setVisibility(View.VISIBLE);
//                                binding.tvOrderIdLayout.setVisibility(View.GONE);
//                                binding.tabsLayout.setVisibility(View.GONE);
//                        binding.floatingLayout.setVisibility(View.GONE);
                        binding.fabAdd.setVisibility(View.GONE);
                    } else {
                        binding.placeFirstOrderLayout.setVisibility(View.GONE);
//                                binding.tvOrderIdLayout.setVisibility(View.VISIBLE);
//                                binding.tabsLayout.setVisibility(View.VISIBLE);
//                        binding.floatingLayout.setVisibility(View.VISIBLE);
                        binding.fabAdd.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
//                apiResponseListener.onError(t.getMessage());
//                binding.tvOrderIdLayout.setVisibility(View.GONE);
//                binding.tabsLayout.setVisibility(View.GONE);
//                binding.floatingLayout.setVisibility(View.GONE);
                binding.fabAdd.setVisibility(View.GONE);

                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ordercanceled(int orderId, int position) {
        progressDialog.show();
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", String.valueOf(orderId));
        Call<JsonObject> call = apiInterface.orderCanceledWithMsg(Token, jsonObject);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call,
                                   @NotNull Response<JsonObject> response) {
                //progressDialog.dismiss();
                /*List<AllOrdersGetModal> allDealersModals = response.body();*/


                if (response.body().get("msg").getAsString().equalsIgnoreCase("cancelled")) {
                    Toast.makeText(context, "Order Canceled!", Toast.LENGTH_SHORT).show();
                    getAllOrders(token);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {

                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        try{
            list_state = binding.allOrdersRecycler.getLayoutManager().onSaveInstanceState();
        }catch (Exception e){

        }
    }
}