package fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spacetexhdemo.R;
import com.spacetexhdemo.animater.ViewAnimation;
import com.spacetexhdemo.databinding.FragmentConfirmdBinding;

import adapters.ConfirmedOrderAdapter;
import listener.OrderConfirmedInterface;
import refill_cylinder.RefillCylinderActivity;
import select_dealer.SelectDealerActivity;

public class ConfirmdFragment extends Fragment {
    FragmentConfirmdBinding binding;
    ConfirmedOrderAdapter confirmedOrderAdapter;
    boolean isRotate = false;
    Boolean isAllFabsVisible;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_confirmd, container, false));
        context = getActivity();
        binding.confirmdOrder.setHasFixedSize(true);

        binding.tvrefillcylinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RefillCylinderActivity.class));
            }
        });

        binding.tvOrderNewCylinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SelectDealerActivity.class));
            }
        });

        isAllFabsVisible = false;

        binding.confirmdOrder.setAdapter(confirmedOrderAdapter);

        binding.fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRotate = ViewAnimation.rotateFab(view, !isRotate);

                if (isRotate) {
                    binding.floatingLayout.setVisibility(View.VISIBLE);
//                    ViewAnimation.showIn(binding.addnewpurchase);
//                    ViewAnimation.showIn(binding.uploadbills);
                    ViewAnimation.showIn(binding.tvrefillcylinder);
//                    ViewAnimation.showIn(binding.tvuploadBills);
//                    ViewAnimation.showIn(binding.tvclose);

                    binding.fabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,
                            R.color.color3B6EFF)));
                    binding.fabAdd.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), PorterDuff.Mode.SRC_IN);


                } else {
//                    ViewAnimation.showOut(binding.addnewpurchase);
//                    ViewAnimation.showOut(binding.uploadbills);
                    ViewAnimation.showOut(binding.tvrefillcylinder);
//                    ViewAnimation.showOut(binding.tvuploadBills);
//                    ViewAnimation.showOut(binding.tvclose);

                    binding.floatingLayout.setVisibility(View.GONE);

                    binding.fabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.
                            getColor(context, R.color.color3B6EFF)));
                    binding.fabAdd.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), PorterDuff.Mode.SRC_IN);


                }
            }
        });


        return binding.getRoot();
    }
}