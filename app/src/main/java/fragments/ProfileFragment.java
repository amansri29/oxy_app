package fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.FragmentProfileBinding;

import java.text.SimpleDateFormat;
import java.util.Date;

import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class ProfileFragment extends Fragment {
    FragmentProfileBinding binding;
    StorageUtils storageUtils;
    String userName, userDob;
    String formattedUserDob ="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_profile, container, false));
        storageUtils = new StorageUtils();

        userName = storageUtils.getDetails(getActivity(), StringUtils.UserName);
        userDob = storageUtils.getDetails(getActivity(), StringUtils.UserDob);
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(userDob);
            sdf = new SimpleDateFormat("dd-MMM-yyyy");
            formattedUserDob = sdf.format(d);
        }
        catch (Exception e){

        }
        binding.editName.setText(userName);
        binding.editDob.setText(formattedUserDob);

        return binding.getRoot();
    }
}