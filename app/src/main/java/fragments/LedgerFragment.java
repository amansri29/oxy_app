package fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.BottomsheetfilterLayoutBinding;
import com.spacetexhdemo.databinding.DatepickerBottomsheetBinding;
import com.spacetexhdemo.databinding.FragmentLedgerBinding;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import adapters.ConfirmedOrderAdapter;
import adapters.LedgerFragmentAdapter;
import listener.BottomsheetSelectedItemListener;
import listener.OrderConfirmedInterface;
import modal.AllOrdersGetModal;
import pickup.AddPickupDetailActivity;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class LedgerFragment extends Fragment {
    FragmentLedgerBinding binding;
    private static final String TAG = "LedgerFragment";
    LedgerFragmentAdapter dealersAdapter;
    String selectedString;
    String setSelectedString;
    BottomsheetSelectedItemListener bottomsheetSelectedItemListener;
    String token, currentDate, finalCurrentDate, sevenDayBeforeDate, firstaRangeDate,
            lastRangedate, firstDateOFPrevMonth, lastDateOFPrevMonth;
    StorageUtils storageUtils;
    private APIInterface apiInterface;
    ArrayList<AllOrdersGetModal> arrayList;
    ConfirmedOrderAdapter confirmedOrderAdapter;
    List<AllOrdersGetModal> allOrders = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_ledger, container, false));

        arrayList = new ArrayList<>();
        storageUtils = new StorageUtils();
        token = storageUtils.getDetails(getActivity(), StringUtils.UserToken);

        bottomsheetSelectedItemListener = new BottomsheetSelectedItemListener() {
            @Override
            public void bottomsheetSelectedItem(String name) {
                binding.tvSelectedFilter.setText(name);
            }
        };
        binding.filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Filter Clicked", Toast.LENGTH_SHORT).show();
                boottomSheetfilter();
            }
        });

        currentDay();
        getSevenDaysBeforeDate();
        getPreviousMonthDate();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);

        /*getAllOrders(token, progressDialog);*/
        getLastSevenDaysOrders(token,progressDialog);

        return binding.getRoot();

    }

    private void boottomSheetfilter() {
        final BottomsheetfilterLayoutBinding binding =
                DataBindingUtil.bind(((FragmentActivity) getActivity()).getLayoutInflater().
                        inflate(R.layout.bottomsheetfilter_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(),
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        /*binding.tvAllTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvAllTime.getText().toString();
                Toast.makeText(getContext(), "selected string" + selectedString, Toast.LENGTH_SHORT).show();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();

                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle("Loading.....");
                progressDialog.setCancelable(false);
                progressDialog.show();
                getAllOrders(token, progressDialog);

            }
        });*/
        binding.tvLastSevenDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvLastSevenDays.getText().toString();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();

                getSevenDaysBeforeDate();
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle("Loading.....");
                progressDialog.setCancelable(false);
                progressDialog.show();
                getLastSevenDaysOrders(token, progressDialog);
            }
        });
        binding.tvLastMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvLastMonth.getText().toString();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();

                getPreviousMonthDate();

                getPreviousMonthDate();
                getLastMonthOrders(token, progressDialog);
            }
        });
            binding.tvCustomDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                    final DatepickerBottomsheetBinding binding =
                            DataBindingUtil.bind(((FragmentActivity) getActivity()).getLayoutInflater().
                                    inflate(R.layout.datepicker_bottomsheet,
                                            null));
                    final BottomSheetDialog custombottomSheetDialog = new BottomSheetDialog(getActivity(),
                            R.style.BottomSheetDialog);

                    assert binding != null;
                    binding.imgBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            custombottomSheetDialog.dismiss();
                        }
                    });

                    binding.btnApply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            int day = binding.startDatePicker.getDayOfMonth();
                            int month = binding.startDatePicker.getMonth();
                            int year = binding.startDatePicker.getYear() - 1900;
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                            Date d = new Date(year, month, day);
                            firstaRangeDate = dateFormatter.format(d);

                            int day1 = binding.endDatePicker.getDayOfMonth();
                            int month1 = binding.endDatePicker.getMonth();
                            int year1 = binding.endDatePicker.getYear() - 1900;
                            SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
                            Date d1 = new Date(year1, month1, day1);
                            lastRangedate = dateFormatter1.format(d1);

                            String finalRangeDate = firstaRangeDate + " to " + lastRangedate;

                            bottomsheetSelectedItemListener.bottomsheetSelectedItem(finalRangeDate);

                            getRangeDateOrders(token, progressDialog, firstaRangeDate, lastRangedate);

                            custombottomSheetDialog.dismiss();
                        }
                    });
                    assert binding != null;
                    custombottomSheetDialog.setContentView(binding.getRoot());
                    custombottomSheetDialog.show();
                }
            });

        bottomSheetDialog.show();
    }

    private void getAllOrders(String token, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getAllorders(Token);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                List<AllOrdersGetModal> allDealersModals = response.body();

                if (allDealersModals != null) {
                    binding.ledgerRecycler.setHasFixedSize(true);
                    dealersAdapter = new LedgerFragmentAdapter(getActivity(), allDealersModals, new OrderConfirmedInterface() {
                        @Override
                        public void orderConfirmed(int position) {
//                            binding.tvSelectSchedule.setTextColor(ContextCompat.getColor(getContext(), R.color.color222B45));
//                            binding.tvOutForDelivery.setTextColor(ContextCompat.getColor(getContext(), R.color.color3B6EFF));
                        }

                        @Override
                        public void orderCanceled(int position) {

                        }
                    });
                    binding.ledgerRecycler.setAdapter(dealersAdapter);
                    int totalCount = dealersAdapter.getItemCount();
                    if (totalCount == 0) {
                        binding.tvNoOrder.setVisibility(View.VISIBLE);
                        binding.ledgerRecycler.setVisibility(View.GONE);
                    } else {
                        binding.tvNoOrder.setVisibility(View.GONE);
                        binding.ledgerRecycler.setVisibility(View.VISIBLE);

                    }
                } else {
                    Toast.makeText(getActivity(), "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {

            }
        });
    }

    private void getLastSevenDaysOrders(String token, ProgressDialog progressDialog) {
        if(!progressDialog.isShowing()){
            progressDialog.show();

        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getLastSevenDayesOrder(Token,
                currentDate, sevenDayBeforeDate);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                if (response.code() == 200 && response.isSuccessful()) {

                    allOrders.clear();
                    allOrders = response.body();
                    List<AllOrdersGetModal> allDealersModals = processOrder();


                    if (allDealersModals != null) {
                        binding.ledgerRecycler.setHasFixedSize(true);
                        dealersAdapter = new LedgerFragmentAdapter(getActivity(), allDealersModals, new OrderConfirmedInterface() {
                            @Override
                            public void orderConfirmed(int position) {
//                            binding.tvSelectSchedule.setTextColor(ContextCompat.getColor(getContext(), R.color.color222B45));
//                            binding.tvOutForDelivery.setTextColor(ContextCompat.getColor(getContext(), R.color.color3B6EFF));
                                onInitiateRefundButtonClicked(position, allDealersModals);
                            }

                            @Override
                            public void orderCanceled(int position) {

                            }
                        });
                        binding.ledgerRecycler.setAdapter(dealersAdapter);
                        int totalCount = dealersAdapter.getItemCount();
                        if (totalCount == 0) {
                            binding.tvNoOrder.setVisibility(View.VISIBLE);
                            binding.ledgerRecycler.setVisibility(View.GONE);
                        } else {
                            binding.tvNoOrder.setVisibility(View.GONE);
                            binding.ledgerRecycler.setVisibility(View.VISIBLE);

                        }
                    } else {
                        Toast.makeText(getActivity(), "Error occurred!", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {

            }
        });
    }

    private List<AllOrdersGetModal> processOrder() {
        List<AllOrdersGetModal> allDealersModals = new ArrayList<>();
        for (AllOrdersGetModal order : allOrders) {
            int security_deposit = 0;
            for (AllOrdersGetModal.ItemsBean item : order.getItems()) {
                security_deposit = security_deposit + item.getSecurity_deposit() * item.getQuantity();
            }
            Log.d(TAG, "onResponse: "+security_deposit + order.getOrder_type());
            if(security_deposit > 0 && order.getOrder_type().equals("New") && order.getStatus().equals("Delivered") && order.getReturnDate() == null){
                order.setShowRefundButton(true);
            }
            if(security_deposit > 0) {
                order.addSecurityDeposit(security_deposit);
            }
            allDealersModals.add(order);
        }
        return allDealersModals;
    }

    private void getLastMonthOrders(String token, ProgressDialog progressDialog) {
        if(!progressDialog.isShowing()){
            progressDialog.show();

        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getLastMonthOrder(Token, lastDateOFPrevMonth,
                firstDateOFPrevMonth);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    allOrders.clear();
                    allOrders = response.body();
                    List<AllOrdersGetModal> allDealersModals = processOrder();

                    if (allDealersModals != null) {
                        binding.ledgerRecycler.setHasFixedSize(true);
                        dealersAdapter = new LedgerFragmentAdapter(getActivity(), allDealersModals, new OrderConfirmedInterface() {
                            @Override
                            public void orderConfirmed(int position) {
//                            binding.tvSelectSchedule.setTextColor(ContextCompat.getColor(getContext(), R.color.color222B45));
//                            binding.tvOutForDelivery.setTextColor(ContextCompat.getColor(getContext(), R.color.color3B6EFF));
                                onInitiateRefundButtonClicked(position, allDealersModals);
                            }

                            @Override
                            public void orderCanceled(int position) {

                            }
                        });
                        binding.ledgerRecycler.setAdapter(dealersAdapter);
                        int totalCount = dealersAdapter.getItemCount();
                        if (totalCount == 0) {
                            binding.tvNoOrder.setVisibility(View.VISIBLE);
                            binding.ledgerRecycler.setVisibility(View.GONE);
                        } else {
                            binding.tvNoOrder.setVisibility(View.GONE);
                            binding.ledgerRecycler.setVisibility(View.VISIBLE);
                        }

                    } else {
                        Toast.makeText(getActivity(), "Error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getRangeDateOrders(String token, ProgressDialog progressDialog, String fRDate, String lRDate) {
        if(!progressDialog.isShowing()){
            progressDialog.show();

        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }

        String Token = "Token " + token;

        Call<List<AllOrdersGetModal>> call = apiInterface.getDateRangeOrder(Token, lRDate, fRDate);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    allOrders.clear();
                    allOrders = response.body();
                    List<AllOrdersGetModal> allDealersModals = processOrder();

                    if (allDealersModals != null) {
                        binding.ledgerRecycler.setHasFixedSize(true);
                        dealersAdapter = new LedgerFragmentAdapter(getActivity(), allDealersModals, new OrderConfirmedInterface() {
                            @Override
                            public void orderConfirmed(int position) {
//                            binding.tvSelectSchedule.setTextColor(ContextCompat.getColor(getContext(), R.color.color222B45));
//                            binding.tvOutForDelivery.setTextColor(ContextCompat.getColor(getContext(), R.color.color3B6EFF));
                                onInitiateRefundButtonClicked(position, allDealersModals);
                            }

                            @Override
                            public void orderCanceled(int position) {

                            }
                        });
                        binding.ledgerRecycler.setAdapter(dealersAdapter);
                        int totalCount = dealersAdapter.getItemCount();
                        if (totalCount == 0) {
                            binding.tvNoOrder.setVisibility(View.VISIBLE);
                            binding.ledgerRecycler.setVisibility(View.GONE);
                        } else {
                            binding.tvNoOrder.setVisibility(View.GONE);
                            binding.ledgerRecycler.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void currentDay() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat passDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        currentDate = df.format(c);
        finalCurrentDate = passDate.format(c);
        Log.d(TAG, "currentDay: " + finalCurrentDate);
    }

    public void getSevenDaysBeforeDate() {
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, -7);
        Date daysBeforeDate = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        sevenDayBeforeDate = df.format(daysBeforeDate);
//        Toast.makeText(getActivity(), "sevenDayBeforeDate--" + sevenDayBeforeDate, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "currentDay: " + sevenDayBeforeDate);

    }

    private void getPreviousMonthDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        lastDateOFPrevMonth = df.format(lastDateOfPreviousMonth);
//        Toast.makeText(getContext(), "lastDateOFPrevMonth of prev month --" + lastDateOFPrevMonth, Toast.LENGTH_SHORT).show();

        aCalendar.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        firstDateOFPrevMonth = df1.format(firstDateOfPreviousMonth);
//        Toast.makeText(getContext(), "firstDateOFPrevMonth of prev month --" + firstDateOFPrevMonth, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: "+requestCode +"---"+resultCode);
        if(requestCode == 10 && resultCode == Activity.RESULT_OK){
            String selectedFilterText = binding.tvSelectedFilter.getText().toString();
            if(selectedFilterText.equals("Last 7 days")){
                getLastSevenDaysOrders(token, progressDialog);
            }
            else if(selectedFilterText.equals("Last month")){
                getLastMonthOrders(token, progressDialog);
            }
            else{
                getRangeDateOrders(token, progressDialog, firstaRangeDate, lastRangedate);
            }
        }
    }

    private void onInitiateRefundButtonClicked(int position, List<AllOrdersGetModal> allDealersModals){
        Intent intent = new Intent(getActivity(), AddPickupDetailActivity.class);
        intent.putExtra("orderID", allDealersModals.get(position).getId());
        List<AllOrdersGetModal.ItemsBean> items = allDealersModals.get(position).getItems();
        intent.putExtra("Security", allDealersModals.get(position).getItems().get(items.size() - 1 ).getAmount());
        startActivityForResult(intent,10);
    }
}