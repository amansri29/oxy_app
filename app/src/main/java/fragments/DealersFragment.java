package fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.FragmentDealersBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import adapters.DealersAdapter;
import listener.DealersCallListeners;
import listener.SelectCylinderInterface;
import modal.AllDealersModal;
import modal.DealersModal;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import select_dealer.CylinderDelaerAdapter;
import select_dealer.SelectDealerActivity;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class DealersFragment extends Fragment {
    FragmentDealersBinding binding;
    DealersAdapter dealersAdapter;
    ArrayList<DealersModal> dealersModalArrayList;
    String dealerId, token;
    StorageUtils storageUtils;
    private APIInterface apiInterface;
    int moblieNumber;
    private double lat, lon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_dealers, container, false));
        dealersModalArrayList = new ArrayList<>();
        storageUtils = new StorageUtils();

        dealerId = storageUtils.getDetails(getActivity(), StringUtils.DealerId);
        token = storageUtils.getDetails(getActivity(), StringUtils.UserToken);
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String headerToken = "Token " + token;
        getOneDealers(headerToken, dealerId, progressDialog);

        binding.btChangeDealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(getActivity(), SelectDealerActivity.class);
                startActivity(callIntent);
            }
        });

//        dealersModalArrayList.add(new DealersModal("1", "Jhon dealer", "8619368171",
//                "B - 93 shivpuri colony sanaganeer thana jaipur pratap nagar jaipur", "3 Km"));
//        binding.dealersRecycler.setHasFixedSize(true);
//        dealersAdapter = new DealersAdapter(getActivity(), dealersModalArrayList, new DealersCallListeners() {
//            @Override
//            public void dealersCall(int position) {
//                String number = dealersModalArrayList.get(position).getDealersNumber();
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:" + number));
//                startActivity(callIntent);
//            }
//        });
//        binding.dealersRecycler.setAdapter(dealersAdapter);
        try {
            lat = Double.parseDouble(storageUtils.getDetails(getActivity(), StringUtils.myAddressLat));
            lon = Double.parseDouble(storageUtils.getDetails(getActivity(), StringUtils.myAddressLong));
        }catch (Exception e){

        }
        return binding.getRoot();
    }

    public void getOneDealers(String token, String dealerId, ProgressDialog progressDialog) {

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        Call<AllDealersModal> call = apiInterface.getOneDealersDetails(token, dealerId);
        call.enqueue(new Callback<AllDealersModal>() {
            @Override
            public void onResponse(@NotNull Call<AllDealersModal> call,
                                   @NotNull Response<AllDealersModal> response) {
                progressDialog.dismiss();
                AllDealersModal detailsModal = response.body();

                if (detailsModal != null) {
                    String distance = String.valueOf(detailsModal.getDistance());
                    String moblieNumber = detailsModal.getMobile();
                    String basicAddress = String.valueOf(detailsModal.getDealer_address().getBasic_address());
                    String pincode = String.valueOf(detailsModal.getDealer_address().getPincode());
                    String cityName = String.valueOf(detailsModal.getDealer_address().getCity());
                    String finalAddress = basicAddress + " , " + cityName + " , " + pincode;
                    float[] distanceArr = new float[3];
                    Location.distanceBetween(detailsModal.getDealer_address().getLatitude(), detailsModal.getDealer_address().getLongitude(),lat,lon, distanceArr);
                    double distanceInKms = distanceArr[0]/1000;
                    binding.dealerDistance.setText(String.format("%.2f Kms",distanceInKms));
                    binding.dealerLocation.setText(finalAddress);
                    binding.dealerName.setText(detailsModal.getCompany_name());

                    binding.callLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + moblieNumber));
                            startActivity(callIntent);
                        }
                    });


                } else {
                    Toast.makeText(getActivity(), "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllDealersModal> call, @NotNull Throwable t) {
//                apiResponseListener.onError(t.getMessage());
                Toast.makeText(getActivity(), "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}