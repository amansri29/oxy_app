package fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.spacetexhdemo.AddNewAddressActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.FragmentAddressBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import adapters.AddressAdapter;
import adapters.RefillDateAdapter;
import adapters.SelectAddressAdapter;
import listener.AddressRemoveEditListener;
import modal.AddressModal;
import modal.AllAddressModal;
import modal.DeliveryDateModal;
import place_order.PlacedOrderAddressActivity;
import refill_cylinder.RefillPaymentActivity;
import refill_cylinder.Refillschedule;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class AddressFragment extends Fragment {
    FragmentAddressBinding binding;
    private static final String TAG = "AddressFragment";
    AddressAdapter addressAdapter;
    String token, amount, defaultAddressId;
    StorageUtils storageUtils;
    APIInterface apiInterface;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_address, container, false));
        storageUtils = new StorageUtils();
        amount = storageUtils.getDetails(getActivity(), StringUtils.Payment);
        defaultAddressId = storageUtils.getDetails(getActivity(), StringUtils.DefaultAddress);
        token = storageUtils.getDetails(getActivity(), StringUtils.UserToken);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading....");
        progressDialog.show();

        getAllAddress();

        binding.btAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddNewAddressActivity.class));
            }
        });
        return binding.getRoot();
    }

    public void getAllAddress() {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllAddressModal>> call = apiInterface.getAllSelectedAddress(Token);
        call.enqueue(new Callback<List<AllAddressModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllAddressModal>> call,
                                   @NotNull Response<List<AllAddressModal>> response) {
                progressDialog.dismiss();
                List<AllAddressModal> allDealersModals = response.body();
                if (allDealersModals != null) {
//                    binding.radioAddress.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "Address fetched successfully!",
                            Toast.LENGTH_SHORT).show();
                    binding.recyclerAddress.setHasFixedSize(true);
                    addressAdapter = new AddressAdapter(getActivity(), allDealersModals, new AddressRemoveEditListener() {
                        @Override
                        public void removeAddress(int position) {
                            ProgressDialog progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setCancelable(false);
                            progressDialog.setTitle("Loading.....");
                            progressDialog.show();

                            String addressId = String.valueOf(allDealersModals.get(position).getId());

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setTitle("Delete ?");
                            builder1.setMessage("Are you sure want to remove this address.?");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            deleteAddress(addressId, progressDialog);
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setNegativeButton(
                                    "No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }

                        @Override
                        public void editAddress(int position) {
                            Toast.makeText(getActivity(), "Address edit!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    binding.recyclerAddress.setAdapter(addressAdapter);
                } else {
                    Toast.makeText(getActivity(), "Error occurred!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllAddressModal>> call, @NotNull Throwable t) {
                Log.e("Faliour", Objects.requireNonNull(t.getMessage()));
                Toast.makeText(getActivity(), "Failure" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void deleteAddress(String addressId, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllAddressModal>> call = apiInterface.removeAddress(Token, addressId);
        call.enqueue(new Callback<List<AllAddressModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllAddressModal>> call,
                                   @NotNull Response<List<AllAddressModal>> response) {
                progressDialog.dismiss();
                AddressFragment.this.progressDialog.dismiss();
                List<AllAddressModal> allDealersModals = response.body();
//                if (allDealersModals != null) {
//                    binding.radioAddress.setVisibility(View.VISIBLE);

                Toast.makeText(getActivity(), "Address delete successfully!",
                        Toast.LENGTH_SHORT).show();
                getAllAddress();
                addressAdapter.notifyDataSetChanged();

//                } else {
//                    Toast.makeText(getActivity(), "Error occurred!",
//                            Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllAddressModal>> call, @NotNull Throwable t) {
                Log.e("Faliour", Objects.requireNonNull(t.getMessage()));
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failure" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                AddressFragment.this.progressDialog.dismiss();
            }
        });
    }
}