package pickup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityAddPickupDetailBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import adapters.DeliveryDateAdapter;
import listener.GetSelectedDeliveryDateListener;
import modal.DeliveryDateModal;
import modal.Order;
import place_order.OrderRescheduleActivity;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class AddPickupDetailActivity extends AppCompatActivity {
    ActivityAddPickupDetailBinding binding;
    int orderID;
    int refundAmount;
    DeliveryDateAdapter deliveryDateAdapter;
    ArrayList<DeliveryDateModal> arrayList;
    private String selectedPickupDate="";
    private String token;
    private StorageUtils storageUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_pickup_detail);
        storageUtils = new StorageUtils();
        Intent intent = getIntent();
        arrayList = new ArrayList<>();
        orderID = intent.getIntExtra("orderID", -1);
        refundAmount = intent.getIntExtra("Security", 0);
        token = "Token " + storageUtils.getDetails(this, StringUtils.UserToken);
        binding.tvTotlaAmount.setText(""+refundAmount);

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        SimpleDateFormat sdfullFormat = new SimpleDateFormat("yyyy-MM-dd ");
        for (int i = 1; i < 8; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            String fulldate = sdfullFormat.format(calendar.getTime());
            arrayList.add(new DeliveryDateModal(day, fulldate));
        }

        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.btnSubmit.setOnClickListener(v->{
            if(!selectedPickupDate.isEmpty()){
                processPickup();
            }
            else{
                Toast.makeText(this, "Please Select Date for Pickup the Order", Toast.LENGTH_SHORT).show();
            }


        });

        binding.deliveryDateRecycler.setHasFixedSize(true);
        deliveryDateAdapter = new DeliveryDateAdapter(this, arrayList, position -> selectedPickupDate = arrayList.get(position).getDeliveryFulldateName().trim());
        binding.deliveryDateRecycler.setAdapter(deliveryDateAdapter);
    }

    public void processPickup(){
        Call<Order> call = APIClientMain.getClient().create(APIInterface.class).updateOrderForPickUp(token,orderID,selectedPickupDate,"Return Requested");
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code() == 200 && response.isSuccessful()){
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(AddPickupDetailActivity.this, "There is some Error.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}