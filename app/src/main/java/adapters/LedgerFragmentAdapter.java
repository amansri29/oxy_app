package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.strictmode.LeakedClosableViolation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.DealersRecyclerFromatBinding;
import com.spacetexhdemo.databinding.LedgeRecyclerFormatBinding;

import java.util.List;

import listener.OrderConfirmedInterface;
import modal.AllOrdersGetModal;
import pickup.AddPickupDetailActivity;
import utils.StringUtils;

public class LedgerFragmentAdapter extends RecyclerView.Adapter<LedgerFragmentAdapter.ViewHolder> {
    Context context;
    OrderConfirmedInterface orderConfirmedInterface;
    List<AllOrdersGetModal> allDealersModals;

    public LedgerFragmentAdapter(Context context, List<AllOrdersGetModal> allDealersModals,
                                 OrderConfirmedInterface orderConfirmedInterface
    ) {
        this.context = context;
        this.orderConfirmedInterface = orderConfirmedInterface;
        this.allDealersModals = allDealersModals;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LedgeRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.ledge_recycler_format, parent, false));
        LedgerFragmentAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.tvOrderId.setText(" " + allDealersModals.get(position).getId());
        holder.binding.cylenderDeliveryTotalAmount.setText("₹ " + String.valueOf(allDealersModals.get(position).getTotal_amount()));

        LedgerAllItemAdapter ledgerAllItemAdapter = new LedgerAllItemAdapter(context,
                allDealersModals.get(position).getItems(),
                allDealersModals,position);
        holder.binding.allItemRecycler.setHasFixedSize(true);
        holder.binding.allItemRecycler.setAdapter(ledgerAllItemAdapter);
        if(allDealersModals.get(position).getStatus().equals("Returned") && allDealersModals.get(position).getReturnDate() != null){
            holder.binding.tvOrderStatus.setText(" Return Completed");
        }
        else {
            holder.binding.tvOrderStatus.setText(" " + allDealersModals.get(position).getStatus());
        }
        if(allDealersModals.get(position).isShowRefundButton()) {
            holder.binding.initiateRefund.setVisibility(View.VISIBLE);
        }
        else{
            holder.binding.initiateRefund.setVisibility(View.GONE);
        }

        holder.binding.initiateRefund.setOnClickListener(v->{
            orderConfirmedInterface.orderConfirmed(position);
        });
    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LedgeRecyclerFormatBinding binding;

        public ViewHolder(@NonNull LedgeRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
