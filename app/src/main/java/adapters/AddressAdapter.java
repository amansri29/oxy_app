package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.AddressRecyclerFormatBinding;
import com.spacetexhdemo.databinding.DeliveryDateFormatBinding;

import java.util.ArrayList;
import java.util.List;

import listener.AddressRemoveEditListener;
import modal.AddressModal;
import modal.AllAddressModal;
import modal.DeliveryDateModal;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    Context context;
    List<AllAddressModal> arrayList;
    AddressRemoveEditListener addressRemoveEditListener;

    public AddressAdapter(Context context, List<AllAddressModal>
            arrayList, AddressRemoveEditListener addressRemoveEditListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.addressRemoveEditListener = addressRemoveEditListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AddressRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.address_recycler_format, parent, false));
        AddressAdapter.ViewHolder viewHolder = new AddressAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String basicAddress = arrayList.get(position).getBasic_address();
        String cityName = arrayList.get(position).getCity();
        String stateName = arrayList.get(position).getState();
        String pincode = arrayList.get(position).getPincode();
        String finalAddress = basicAddress + " , " + cityName + " , " + pincode + " , " + stateName;
        holder.binding.tvAddressName.setText(finalAddress);

//        holder.binding.tvAddressName.setText(arrayList.get(position).getDeliveryAddressName());
        holder.binding.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressRemoveEditListener.removeAddress(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AddressRecyclerFormatBinding binding;

        public ViewHolder(@NonNull AddressRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
