package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.DeliveryDateFormatBinding;
import com.spacetexhdemo.databinding.SelectedItemRecyclerFormatBinding;

import java.io.Serializable;
import java.util.List;

import modal.SelectedProductDetails;

public class SelectedProductAdapter extends RecyclerView.Adapter<SelectedProductAdapter.ViewHolder> {
    Context context;
    List<SelectedProductDetails> selectedProductDetailsList;

    public SelectedProductAdapter(Context context, List<SelectedProductDetails> selectedProductDetailsList) {
        this.context = context;
        this.selectedProductDetailsList = selectedProductDetailsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SelectedItemRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.selected_item_recycler_format, parent, false));
        SelectedProductAdapter.ViewHolder viewHolder = new SelectedProductAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String productType = selectedProductDetailsList.get(position).getProductType();
        String productQty = String.valueOf(selectedProductDetailsList.get(position).getQuantity());
        String productPrice = String.valueOf(selectedProductDetailsList.get(position).getItemPrice());
        String productName = selectedProductDetailsList.get(position).getProductName();

        holder.binding.tvProductName.setText(productName);
        holder.binding.tvTypeQty.setText("(x" + productQty + " "+  productType + ")");
        holder.binding.tvProductPrice.setText("₹ " + productPrice);
    }

    @Override
    public int getItemCount() {
        return selectedProductDetailsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SelectedItemRecyclerFormatBinding binding;

        public ViewHolder(@NonNull SelectedItemRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
