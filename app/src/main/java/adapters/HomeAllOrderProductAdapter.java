package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ConfirmedOrderFormatBinding;
import com.spacetexhdemo.databinding.HomeOrderAllproductFormatBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import modal.AllOrdersGetModal;

public class HomeAllOrderProductAdapter extends RecyclerView.Adapter<HomeAllOrderProductAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal.ItemsBean> items;
    List<AllOrdersGetModal> allDealersModals;

    public HomeAllOrderProductAdapter(Context context, List<AllOrdersGetModal.ItemsBean> items,
                                      List<AllOrdersGetModal> allDealersModals) {
        this.context = context;
        this.items = items;
        this.allDealersModals = allDealersModals;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        HomeOrderAllproductFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.home_order_allproduct_format, parent, false));
        HomeAllOrderProductAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String itemType = items.get(position).getItem_name();
        String itemQuantity = String.valueOf(items.get(position).getQuantity());
        String itemName = String.valueOf(items.get(position).getProduct_name());
        String bookingDate = String.valueOf(items.get(position).getCreated());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date d = null;
        try {
            d = sdf.parse(bookingDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("dd-MMM-yy");
        String newDateString = sdf.format(d);

//        Toast.makeText(context, "newDateString---"
//                + newDateString, Toast.LENGTH_SHORT).show();

        if (itemType.equals("standard")) {
            itemType = "S";
            holder.binding.cylenderNameQtyType.setText(itemName + "(x" + itemQuantity + itemType + ")");
            holder.binding.orderAmount.setText("₹ " + String.valueOf(items.get(position).getAmount()));
            holder.binding.cylenderDeliveryDate.setText(newDateString);
        } else {
            holder.binding.cylenderNameQtyType.setText(itemName + "(x" + itemQuantity + itemType + ")");
            holder.binding.orderAmount.setText("₹ " + String.valueOf(items.get(position).getAmount()));
            holder.binding.cylenderDeliveryDate.setText(newDateString);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        HomeOrderAllproductFormatBinding binding;

        public ViewHolder(@NonNull HomeOrderAllproductFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
