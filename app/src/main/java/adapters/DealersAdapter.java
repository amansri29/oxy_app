package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ConfirmedOrderFormatBinding;
import com.spacetexhdemo.databinding.DealersRecyclerFromatBinding;

import java.util.ArrayList;

import listener.DealersCallListeners;
import modal.DealersModal;

public class DealersAdapter extends RecyclerView.Adapter<DealersAdapter.ViewHolder> {
    Context context;
    ArrayList<DealersModal> dealersModalArrayList;
    DealersCallListeners dealersCallListeners;

    public DealersAdapter(Context context, ArrayList<DealersModal> dealersModals, DealersCallListeners dealersCallListeners) {
        this.context = context;
        this.dealersModalArrayList = dealersModals;
        this.dealersCallListeners = dealersCallListeners;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        DealersRecyclerFromatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.dealers_recycler_fromat, parent, false));
        DealersAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvDelerNAme.setText(dealersModalArrayList.get(position).getDealersName());
        String tvNumber = dealersModalArrayList.get(position).getDealersNumber();
        holder.binding.dealerLocation.setText(dealersModalArrayList.get(position).getDealersAddress());
        holder.binding.dealerDistance.setText(dealersModalArrayList.get(position).getDealerDistance());

        holder.binding.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dealersCallListeners.dealersCall(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dealersModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        DealersRecyclerFromatBinding binding;

        public ViewHolder(@NonNull DealersRecyclerFromatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
