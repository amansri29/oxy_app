package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.CartSectionFormatBinding;

import java.util.ArrayList;

import listener.CartQtyIncrementDecrement;
import modal.CylinderRefilModal;

public class RefilCylinderAdapter extends RecyclerView.Adapter<RefilCylinderAdapter.ViewHolder> {
    Context context;
    int lastposition = -1;
    int posi;
    int count = 1;
    ArrayList<CylinderRefilModal> arrayList;
    CartQtyIncrementDecrement cartQtyIncrementDecrement;

    public RefilCylinderAdapter(Context context, ArrayList<CylinderRefilModal> arrayList, CartQtyIncrementDecrement cartQtyIncrementDecrement) {
        this.context = context;
        this.arrayList = arrayList;
        this.cartQtyIncrementDecrement = cartQtyIncrementDecrement;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CartSectionFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cart_section_format, parent, false));
        RefilCylinderAdapter.ViewHolder viewHolder = new RefilCylinderAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvProductName.setText(arrayList.get(position).getCylinderName());
        holder.binding.tvPrice.setText("₹ " + arrayList.get(position).getCylinderPrice());
        holder.binding.tvquantaty.setText(arrayList.get(position).getQuantiy());

        String size = arrayList.get(position).getSize();
        if (size.isEmpty()) {
            holder.binding.sizeLayout.setVisibility(View.GONE);
            holder.binding.tvSize.setVisibility(View.GONE);
            holder.binding.tvStarSec.setVisibility(View.GONE);
        } else {
//            if (size.equals("C")) {
//                holder.binding.tvC.setText(arrayList.get(position).getSize());
//            } else if (size.equals("D")) {
//                holder.binding.tvD.setText(arrayList.get(position).getSize());
//            } else if (size.equals("E")) {
//                holder.binding.tvE.setText(arrayList.get(position).getSize());
//            } else if (size.equals("F")) {
//                holder.binding.tvF.setText(arrayList.get(position).getSize());
//            } else {
//                holder.binding.sizeLayout.setVisibility(View.GONE);
//                holder.binding.tvStarSec.setVisibility(View.GONE);
//                holder.binding.tvSize.setVisibility(View.GONE);
//            }
        }


        holder.binding.qtyPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ttlQty = holder.binding.tvPrice.getText().toString();
                cartQtyIncrementDecrement.quantityIncrement(position,
                        holder.binding.tvquantaty, ttlQty);
            }
        });

        holder.binding.qtyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ttlQty = holder.binding.tvPrice.getText().toString();

                cartQtyIncrementDecrement.quantityDecrement(position,
                        holder.binding.tvquantaty, ttlQty);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CartSectionFormatBinding binding;

        public ViewHolder(@NonNull CartSectionFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
