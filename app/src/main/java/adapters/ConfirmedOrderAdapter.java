package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ConfirmedOrderFormatBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import listener.OrderConfirmedInterface;
import modal.AllOrdersGetModal;
import payment.RetryPaymentActivity;
import place_order.OrderRescheduleActivity;
import utils.StringUtils;

public class ConfirmedOrderAdapter extends RecyclerView.Adapter<ConfirmedOrderAdapter.ViewHolder> {
    Context context;
    OrderConfirmedInterface orderConfirmedInterface;
    List<AllOrdersGetModal> allDealersModals;
    private SimpleDateFormat deliveryDateFormat;
    private SimpleDateFormat dateReadFormat;

    public ConfirmedOrderAdapter(Context context, List<AllOrdersGetModal> allDealersModals, OrderConfirmedInterface orderConfirmedInterface) {
        this.context = context;
        this.allDealersModals = allDealersModals;
        this.orderConfirmedInterface = orderConfirmedInterface;
        deliveryDateFormat = new SimpleDateFormat("dd-MMM-yy");
        dateReadFormat = new SimpleDateFormat("yyyy-mm-dd");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ConfirmedOrderFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.confirmed_order_format, parent, false));
        ConfirmedOrderAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String bookingStatus = allDealersModals.get(position).getStatus();
        holder.binding.tvOrderId.setText(String.valueOf(allDealersModals.get(position).getId()));
        String delivery_date = "";
        try{
            Date d = dateReadFormat.parse(allDealersModals.get(position).getExpected_delivery_date());
            delivery_date = deliveryDateFormat.format(d);
        }
        catch (Exception e){
        }
        if(bookingStatus.equals("Return Requested")){
            holder.binding.cylenderDeliveryDate.setText(allDealersModals.get(position).getPrettyReturnDate());
            int size = allDealersModals.get(position).getItems().size();
            holder.binding.tvTotalAmount.setText("₹ "+allDealersModals.get(position).getItems().get(size - 1).getAmount());
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    allDealersModals.get(position).getSecurityItems(), allDealersModals);
            holder.binding.allProductRecycler.setHasFixedSize(true);
            holder.binding.allProductRecycler.setAdapter(homeAllOrderProductAdapter);
            holder.binding.tvDeliveryDate.setText("PICKUP DATE");
            holder.binding.tvdeliveryCode.setText("PICKUP CODE");
            holder.binding.tvDelerLocation.setText("PICKUP LOCATION");
            holder.binding.tvTotalAmountTitle.setText("REFUND AMOUNT");
        }
        else{
            holder.binding.cylenderDeliveryDate.setText(delivery_date);
            holder.binding.tvTotalAmount.setText("₹ "+allDealersModals.get(position).getTotal_amount());
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    allDealersModals.get(position).getItems(), allDealersModals);
            holder.binding.allProductRecycler.setHasFixedSize(true);
            holder.binding.allProductRecycler.setAdapter(homeAllOrderProductAdapter);
            holder.binding.tvDeliveryDate.setText("DELIVERY DATE");
            holder.binding.tvdeliveryCode.setText("DELIVERY CODE");
            holder.binding.tvDelerLocation.setText("DELIVERY LOCATION");
            holder.binding.tvTotalAmountTitle.setText("TOTAL AMOUNT (Inc.taxes)");
        }

        String orderType = allDealersModals.get(position).getOrder_type();
        int orderDays = allDealersModals.get(position).getRepeat_order_days();
        String finalType = orderType;
        if (orderDays > 0)
            finalType = orderType + " , " + " Repeat every " + String.valueOf(orderDays) + " Times";

        holder.binding.cylenderDeliveryType.setText(finalType);
        holder.binding.cylenderDealerName.setText(allDealersModals.get(position).getDealer_name());
        holder.binding.tvdeliveryCode.setText(String.valueOf(allDealersModals.get(position).getDelivery_code()));

        String landMark = allDealersModals.get(position).getAddress_details().getLandmark();
        String cityName = allDealersModals.get(position).getAddress_details().getCity();
        String stateName = allDealersModals.get(position).getAddress_details().getState();
        String areaPincode = String.valueOf(allDealersModals.get(position).getAddress_details().getPincode());

        String finalAddress = landMark + "," + cityName + "," + stateName + "," + areaPincode;
        holder.binding.cylenderDealerLocation.setText(finalAddress);




//        Toast.makeText(context, "bookingStatus--" + bookingStatus, Toast.LENGTH_SHORT).show();

        if (bookingStatus.equals("Delivered")) {
            highlightSelected(holder.binding.tvDeliverd, holder.binding.tvPending, holder.binding.tvOutForDelivery,holder.binding.tvSelectSchedule, holder.binding.tvPickup);
            holder.binding.cancelRescheduleLayout.setVisibility(View.GONE);
            holder.binding.deliveredLayout.setVisibility(View.VISIBLE);
            holder.binding.retryPayment.setVisibility(View.GONE);
        }
        else if(bookingStatus.equals("Confirmed")){
            highlightSelected(holder.binding.tvSelectSchedule, holder.binding.tvPending, holder.binding.tvOutForDelivery,holder.binding.tvDeliverd, holder.binding.tvPickup);
            holder.binding.retryPayment.setVisibility(View.GONE);
            holder.binding.rescheduleOrderLayout.setVisibility(View.VISIBLE);
        }
        else if(bookingStatus.equals("Payment Pending")){
            highlightSelected(holder.binding.tvPending, holder.binding.tvDeliverd, holder.binding.tvOutForDelivery,holder.binding.tvSelectSchedule,holder.binding.tvPickup);
            holder.binding.retryPayment.setVisibility(View.VISIBLE);
            holder.binding.deliveredLayout.setVisibility(View.GONE);
            holder.binding.rescheduleOrderLayout.setVisibility(View.GONE);
        }
        else if(bookingStatus.equals("In-transit")){
            highlightSelected(holder.binding.tvOutForDelivery, holder.binding.tvDeliverd, holder.binding.tvPending,holder.binding.tvSelectSchedule,holder.binding.tvPickup);
            holder.binding.retryPayment.setVisibility(View.GONE);
        }
        else {
            highlightSelected(holder.binding.tvPickup, holder.binding.tvDeliverd, holder.binding.tvOutForDelivery,holder.binding.tvSelectSchedule,holder.binding.tvPending);
            holder.binding.cancelRescheduleLayout.setVisibility(View.VISIBLE);
            holder.binding.deliveredLayout.setVisibility(View.GONE);
            holder.binding.retryPayment.setVisibility(View.GONE);
            holder.binding.rescheduleOrderLayout.setVisibility(View.VISIBLE);
        }

        holder.binding.tvCancelORder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderConfirmedInterface.orderCanceled(position);
//                holder.binding.cancelRescheduleLayout.setVisibility(View.GONE);
//                orderConfirmedInterface.orderConfirmed(position);
            }
        });
        holder.binding.rescheduleORder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String orderId = String.valueOf(allDealersModals.get(position).getId());
                String totalAmount = String.valueOf(allDealersModals.get(position).getTotal_amount());
                String date = String.valueOf(allDealersModals.get(position).getExpected_delivery_date());
                String status = allDealersModals.get(position).getStatus();
                String return_date = allDealersModals.get(position).getReturnDate();
//                Toast.makeText(context, "orerID--" + orderId, Toast.LENGTH_SHORT).show();
//                holder.binding.cancelRescheduleLayout.setVisibility(View.GONE);
//                orderConfirmedInterface.orderConfirmed(position);
                context.startActivity(new Intent(context, OrderRescheduleActivity.class)
                        .putExtra("orerID", orderId).putExtra("totalAmount", totalAmount).putExtra("date",date).putExtra("status",status).putExtra("return_date",return_date));
            }
        });

        holder.binding.retryPayment.setOnClickListener(v->{
            String orderID = String.valueOf(allDealersModals.get(position).getId());
            String totalAmount = String.valueOf(allDealersModals.get(position).getTotal_amount());
            context.startActivity(new Intent(context, RetryPaymentActivity.class)
                    .putExtra("orderID", orderID)
                    .putExtra("totalAmount",totalAmount));
        });

    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ConfirmedOrderFormatBinding binding;

        public ViewHolder(@NonNull ConfirmedOrderFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void highlightSelected(TextView selTxView, TextView unselTxView1, TextView unselTxView2, TextView unselTxView3, TextView unselTxView4){
        selTxView.setTextColor(context.getResources().getColor(R.color.color3B6EFF));
        unselTxView1.setTextColor(context.getResources().getColor(R.color.colorABABAB));
        unselTxView2.setTextColor(context.getResources().getColor(R.color.colorABABAB));
        unselTxView3.setTextColor(context.getResources().getColor(R.color.colorABABAB));
        unselTxView4.setTextColor(context.getResources().getColor(R.color.colorABABAB));
    }
}
