package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.AddressRecyclerFormatBinding;
import com.spacetexhdemo.databinding.RadioAddressRecyclerFormatBinding;

import java.util.List;

import listener.GetSelectedAddressIdListerner;
import modal.AllAddressModal;

public class SelectAddressAdapter extends RecyclerView.Adapter<SelectAddressAdapter.ViewHolder> {
    Context context;
    List<AllAddressModal> addressModalList;
    private RadioButton lastCheckedRB = null;
    GetSelectedAddressIdListerner getSelectedAddressIdListerner;
    //    RadioButton checked_rb;
    int lastposition = -1;

    public SelectAddressAdapter(Context context, List<AllAddressModal> allDealersModals,
                                GetSelectedAddressIdListerner getSelectedAddressIdListerner) {
        this.context = context;
        this.addressModalList = allDealersModals;
        this.getSelectedAddressIdListerner = getSelectedAddressIdListerner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RadioAddressRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.radio_address_recycler_format, parent, false));
        SelectAddressAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String basicAddress = addressModalList.get(position).getBasic_address();
//        String cityName = addressModalList.get(position).getCity();
//        String stateName = addressModalList.get(position).getState();
//        String pincode = addressModalList.get(position).getPincode();

        String finalAddress = basicAddress;
        holder.binding.radioAddress.setText(finalAddress);

        holder.binding.radioAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == addressModalList.size() - 1) {
                    getSelectedAddressIdListerner.openAddNewAddressLayout(position);
                    lastposition = position;
                    notifyDataSetChanged();
//                    Toast.makeText(context, "Add address clicked!", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(context, "Add address not clicked!", Toast.LENGTH_SHORT).show();
                    getSelectedAddressIdListerner.getSelectedAddressId(position, finalAddress);
                    lastposition = position;
                    notifyDataSetChanged();
                }
            }
        });

        holder.binding.radioAddress.setChecked(lastposition == position);

    }

    @Override
    public int getItemCount() {
        return addressModalList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RadioAddressRecyclerFormatBinding binding;

        public ViewHolder(@NonNull RadioAddressRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
