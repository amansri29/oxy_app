package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.DealersRecyclerFromatBinding;
import com.spacetexhdemo.databinding.LedgerAllitemRecyclerBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import modal.AllOrdersGetModal;

public class LedgerAllItemAdapter extends RecyclerView.Adapter<LedgerAllItemAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal.ItemsBean> allDealersModals;
    List<AllOrdersGetModal> orderDetails;
    SimpleDateFormat serverDateFormat;
    SimpleDateFormat prettyDateFormat;
    private int ledgerAllItemPosition = -1;

    public LedgerAllItemAdapter(Context context, List<AllOrdersGetModal.ItemsBean> allDealersModals,
                                List<AllOrdersGetModal> orderDetails, int ledgerAllItemPosition) {
        this.context = context;
        this.allDealersModals = allDealersModals;
        this.orderDetails = orderDetails;
        serverDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        prettyDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        this.ledgerAllItemPosition = ledgerAllItemPosition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LedgerAllitemRecyclerBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.ledger_allitem_recycler, parent, false));
        LedgerAllItemAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String itemType = allDealersModals.get(position).getItem_name();
        int itemQuantity = allDealersModals.get(position).getQuantity();
        int amount = allDealersModals.get(position).getAmount();
        String itemName = String.valueOf(allDealersModals.get(position).getProduct_name());

        if (itemType.equals("standard")) {
            itemType = "S";
            holder.binding.cylenderNameQtyType.setText(itemName + "(x" + itemQuantity + itemType + ")");
            holder.binding.orderType.setText("₹ " + amount*itemQuantity);
//            String edd = orderDetails.get(position).getExpected_delivery_date();
//            Toast.makeText(context, "edd--" + edd, Toast.LENGTH_SHORT).show();

        } else {
            holder.binding.cylenderNameQtyType.setText(itemName + "(x" + itemQuantity + itemType + ")");
            holder.binding.orderType.setText("₹ " + amount*itemQuantity);
//            String edd = orderDetails.get(position).getExpected_delivery_date();
//            Toast.makeText(context, "edd--" + edd, Toast.LENGTH_SHORT).show();

        }
        String deliveryDate = "";
        try{
            Date d = serverDateFormat.parse(orderDetails.get(position).getExpected_delivery_date());
            deliveryDate = prettyDateFormat.format(d);
        }
        catch (Exception e){

        }
        holder.binding.cylenderDeliveryDate.setText(deliveryDate);
    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LedgerAllitemRecyclerBinding binding;

        public ViewHolder(@NonNull LedgerAllitemRecyclerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
