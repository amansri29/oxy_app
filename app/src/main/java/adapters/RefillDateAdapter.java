package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.DeliveryDateFormatBinding;

import java.util.ArrayList;

import modal.DeliveryDateModal;

public class RefillDateAdapter extends RecyclerView.Adapter<RefillDateAdapter.ViewHolder> {
    Context context;
    int posi;
    int count = 1;
    ArrayList<DeliveryDateModal> arrayList;

    public RefillDateAdapter(Context context, ArrayList<DeliveryDateModal> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        DeliveryDateFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.delivery_date_format, parent, false));
        RefillDateAdapter.ViewHolder viewHolder = new RefillDateAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.delivertDateTExt.setText(arrayList.get(position).getDeliverydateName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count--;
                posi = position;
                notifyDataSetChanged();
            }
        });

        if (count == 1) {
            holder.binding.deliveryDateLayout.setBackground(context.getResources().getDrawable(R.drawable.delivery_date_background));
            holder.binding.delivertDateTExt.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
        } else {
            if (posi == position) {
                holder.binding.deliveryDateLayout.setBackground(context.getResources().getDrawable(R.drawable.select_delivery_date_background));
                holder.binding.delivertDateTExt.setTextColor(ContextCompat.getColor(context, R.color.color3B6EFF));
            } else {
                holder.binding.deliveryDateLayout.setBackground(context.getResources().getDrawable(R.drawable.delivery_date_background));
                holder.binding.delivertDateTExt.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
            }
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        DeliveryDateFormatBinding binding;

        public ViewHolder(@NonNull DeliveryDateFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
