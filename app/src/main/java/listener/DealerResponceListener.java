package listener;

public interface DealerResponceListener {
    void onSuccess(String tag, String superClassCastBean);

    void onFailure(String msg);

    void onError(String msg);

    void onPerformCode(int code);
}
