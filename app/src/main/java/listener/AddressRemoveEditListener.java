package listener;

public interface AddressRemoveEditListener {
    void removeAddress(int position);
    void editAddress(int position);
}
