package listener;

public interface BottomsheetSelectedItemListener {
    void bottomsheetSelectedItem(String name);
}
