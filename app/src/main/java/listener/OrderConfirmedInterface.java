package listener;

public interface OrderConfirmedInterface {
    public void orderConfirmed(int position);

    public void orderCanceled(int position);
}
