package listener;

public interface SelectSizeListener {
    public void selectedSize(int position);
}
