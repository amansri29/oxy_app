package listener;

public interface GetSelectedDeliveryDateListener {
    public void getDeliveryDate(int date);
}
