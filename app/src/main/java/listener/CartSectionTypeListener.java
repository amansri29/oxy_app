package listener;

public interface CartSectionTypeListener {
    public void getPrice(float position, int sizeId, String sizeName, float securityAmount);
}
