package listener;

import android.widget.TextView;

public interface CartQtyIncrementDecrement {
    public void quantityIncrement(int position, TextView qty, String totalPrice);

    public void quantityDecrement(int position, TextView qty, String totalPrice);

    public void finalPrice(int finalPrice);
}
