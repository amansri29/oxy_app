package listener;

public interface GetSelectedAddressIdListerner {
    public void getSelectedAddressId(int position, String finalAddress);

    public void openAddNewAddressLayout(int position);
}
