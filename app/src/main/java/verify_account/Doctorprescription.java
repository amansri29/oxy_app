package verify_account;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;
import com.shockwave.pdfium.PdfDocument;
import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.activity.BaseActivity;
import com.spacetexhdemo.databinding.ActivityDoctorprescriptionBinding;
import com.spacetexhdemo.databinding.ChooseDocumentUploadBinding;

import java.io.File;
import java.io.IOException;
import java.util.List;

import modal.UserDetailsModal;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import retrofit.models.Doctor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import select_dealer.SelectDealerActivity;
import sharedprefrence.StorageUtils;
import utils.FilePath;
import utils.StringUtils;

public class Doctorprescription extends BaseActivity implements ApiResponseListener, OnPageChangeListener, OnLoadCompleteListener {
    ActivityDoctorprescriptionBinding binding;
    private static final int PICK_IMAGE_REQUEST = 1;
    Context context;
    private Bitmap bitmap;
    private String filePath = "", token, userID;
    private ApiController apiController;
    StorageUtils storageUtils;
    ProgressDialog progressDialog;
    private static final String TAG = "Doctorprescription";
    Boolean isValidFile;
    Integer pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_doctorprescription);
        context = Doctorprescription.this;

        storageUtils = new StorageUtils();
        apiController = new ApiController(this);
        token = storageUtils.getDetails(context, StringUtils.UserToken);
        userID = storageUtils.getDetails(context, StringUtils.UserId);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edittextDoctorName.getText().toString().equals("")) {
                    Toast.makeText(Doctorprescription.this, "Please provide your doctor name", Toast.LENGTH_SHORT).show();
                } else if (binding.edittextDoctorMobile.getText().toString().equals("")) {
                    Toast.makeText(Doctorprescription.this, "Please provide your doctor mobile no.", Toast.LENGTH_SHORT).show();
                } else if (filePath.equals("")) {
                    Toast.makeText(Doctorprescription.this, "Please select the document!", Toast.LENGTH_SHORT).show();
                } else {
                    createDoctor(binding.edittextDoctorName.getText().toString(),
                            binding.edittextDoctorMobile.getText().toString());
//                    startActivity(new Intent(Doctorprescription.this, SelectDealerActivity.class));
                }
            }
        });

        binding.imgPrescriptionUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseDocumentUploadBinding binding = DataBindingUtil.bind(getLayoutInflater()
                        .inflate(R.layout.choose_document_upload, null));
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context, R.style.BottomSheetDialog);

                assert binding != null;
                bottomSheetDialog.setContentView(binding.getRoot());

                binding.selectImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, 100);
                        bottomSheetDialog.dismiss();
                    }
                });
                binding.selectPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("application/pdf");
                        startActivityForResult(intent, 200);
                        bottomSheetDialog.dismiss();
                    }
                });
                bottomSheetDialog.show();
            }
        });

        binding.btnSkip.setOnClickListener(v->{
            Log.d(TAG, "onCreate: Button Click");
            startActivity(new Intent(Doctorprescription.this, SelectDealerActivity.class));
            storageUtils.setDetails(context, StringUtils.DoctorPrescription, StringUtils.Yes);
        });
        allowpermisions();
    }

    private void createDoctor(String name, String mobile) {
        String headerToken = "Token " + token;
        Doctor doctor = new Doctor(name, mobile);
        Call<Doctor> call = APIClientMain.getClient().create(APIInterface.class).setDoctorData(headerToken, doctor);
        call.enqueue(new Callback<Doctor>() {
            @Override
            public void onResponse(Call<Doctor> call, Response<Doctor> response) {
                Log.i(TAG, "onResponse: set doctor " + response.code());
                if (response.isSuccessful()) {
                    updateDoctorId(response.body().getId());
                }
            }

            @Override
            public void onFailure(Call<Doctor> call, Throwable t) {
                Log.e(TAG, "onFailure: doctor put " + t.getMessage());
            }
        });
    }

    private void updateDoctorId(int doctorID) {
        String headerToken = "Token " + token;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("prescribe_by", doctorID);
        Call<UserDetailsModal> call = APIClientMain.getClient().create(APIInterface.class).updateCustomerDetails(headerToken, jsonObject, customerID);
        call.enqueue(new Callback<UserDetailsModal>() {
            @Override
            public void onResponse(Call<UserDetailsModal> call, Response<UserDetailsModal> response) {
                Log.i(TAG, "onResponse: update doctor " + response.code());
                startActivity(new Intent(Doctorprescription.this, SelectDealerActivity.class));
            }

            @Override
            public void onFailure(Call<UserDetailsModal> call, Throwable t) {
                Log.e(TAG, "onFailure: update " + t.getMessage());

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 200) {
            binding.imgSelectedImage.setVisibility(View.GONE);
            binding.pdfView.setVisibility(View.VISIBLE);
            Uri uri = data.getData();
            displayFromUri(uri);
            assert uri != null;
            filePath = uri.getPath();

//            if (filePath.isEmpty()) {
//                Toast.makeText(context, "Please select the pdf file", Toast.LENGTH_SHORT).show();
//            } else {
//                uploadMedicalDocument();
//            }

            isValidFile = checkOtherFileType(filePath);
            if (isValidFile) {
                uploadDoctorPrescriptionDocument();

                Toast.makeText(this, "pdfPath--" + filePath,
                        Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(context, "Please select the pdf file", Toast.LENGTH_SHORT).show();
            }
        }
        if (resultCode == RESULT_OK && requestCode == 100) {
            binding.imgSelectedImage.setVisibility(View.VISIBLE);
            binding.pdfView.setVisibility(View.GONE);
            Uri uri = data.getData();
//            filePath = getPDFPath(this, data.getData());
            filePath = FilePath.getPath(context, uri);
            Toast.makeText(this, "image Path--" + filePath,
                    Toast.LENGTH_SHORT).show();
            binding.imgSelectedImage.setImageURI(uri);
            if (filePath != null) {
                uploadDoctorPrescriptionDocument();
            } else {
                Toast.makeText(context, R.string.no_image_selected, Toast.LENGTH_LONG).show();
            }
        }


        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
//                data != null && data.getData() != null) {
//        Uri picUri = data.getData();
//        filePath = FilePath.getPath(context, picUri);
//            binding.imgPrescriptionUpload.setImageURI(picUri);
//            binding.tvFilePath.setText(filePath);
//
//            if (filePath != null) {
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), picUri);
//                    uploadDoctorPrescriptionDocument();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Toast.makeText(context, R.string.no_image_selected, Toast.LENGTH_LONG).show();
//            }
//        }
    }


    public String getPDFPath(Context context, Uri uri) {
        Cursor returnCursor =
                context.getContentResolver().query(uri,
                        null, null, null,
                        null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        return returnCursor.getString(nameIndex);
    }

    private Boolean checkOtherFileType(String filePath) {
        if (!filePath.isEmpty() && filePath != null) {
            String filePathInLowerCase = filePath.toLowerCase();
            if (filePathInLowerCase.endsWith(".pdf")) {
                return true;
            }
        }
        return false;
    }


    private void uploadDoctorPrescriptionDocument() {
//        Toast.makeText(context, "Image Uploaded successfully!", Toast.LENGTH_SHORT).show();
        progressDialog = new ProgressDialog(Doctorprescription.this);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*|application/pdf/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("prescription_document", file.getName(), requestBody);
        RequestBody someData = RequestBody.create(MediaType.parse("text/plain"), "This is a new image");
        apiController.uploadDoctorPrescription(part, userID, token);
    }


    private void allowpermisions() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};

        if (ContextCompat.checkSelfPermission(context,
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,
                permissions[1]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,
                permissions[2]) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Permission Granted!", Toast.LENGTH_SHORT).show();
            //  setupViewPager();
        } else {
            ActivityCompat.requestPermissions(Doctorprescription.this,
                    permissions,
                    1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        allowpermisions();
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context.getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    allowpermisions();
                } else {
                    Toast.makeText(context.getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onSuccess(String tag, String superClassCastBean) {
        progressDialog.dismiss();
        Toast.makeText(context, "Upload successfully!", Toast.LENGTH_SHORT).show();
//         startActivity(new Intent(Doctorprescription.this, PlaceOrderScheduleActivity.class));
        storageUtils.setDetails(context, StringUtils.DoctorPrescription, StringUtils.Yes);
//        startActivity(new Intent(Doctorprescription.this, MainActivity.class));
    }

    @Override
    public void onFailure(String msg) {
        progressDialog.dismiss();
//        Toast.makeText(context, "onFailure!" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "onError!" + msg, Toast.LENGTH_SHORT).show();
//        Toast.makeText(context, "Document uploaded!", Toast.LENGTH_SHORT).show();
//        storageUtils.setDetails(context, StringUtils.DoctorPrescription, StringUtils.Yes);

    }

    @Override
    public void onPerformCode(int code) {

    }

    private void displayFromUri(Uri uri) {
//        pdfFileName = getFileName(uri);
        binding.pdfView.fromUri(uri)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .load();
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = binding.pdfView.getDocumentMeta();
        printBookmarksTree(binding.pdfView.getTableOfContents(), "-");
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
//        pageNumber = page;
//        setTitle(String.format("%s %s / %s", fileName, page + 1, pageCount));
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

//            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

}