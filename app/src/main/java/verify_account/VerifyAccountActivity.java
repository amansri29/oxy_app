package verify_account;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import select_dealer.SelectDealerActivity;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.maps.model.LatLng;
import com.spacetexhdemo.AadharCardUploadActivity;
import com.spacetexhdemo.AddMannulaAddressActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.activity.BaseActivity;
import com.spacetexhdemo.databinding.ActivityVerifyAccountBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import retrofit.ApiController;
import retrofit.ApiResponseListener;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class VerifyAccountActivity extends BaseActivity implements LocationListener, ApiResponseListener {
    ActivityVerifyAccountBinding binding;
    String chagneFormat, setDate;
    String token, state, country = "", name, dob ="", gender = "", mannualAddrss, basicAddress, landmark, city,
            areaPincode = null, finalAdress, contactPersonName, contactPersonNo, gst;
    double longitude, latitude;
    LocationManager locationManager;
    StorageUtils storageUtils;
    Context context;
    private ApiController apiController;
    ProgressDialog progressDialog;
    private static final String TAG = "VerifyAccountActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_account);
        context = VerifyAccountActivity.this;
        storageUtils = new StorageUtils();
//        token = getIntent().getStringExtra("token");
//        token = "131cbcecaee4244e100f7906ceb3012782aecae1";
        token = storageUtils.getDetails(context, StringUtils.UserToken);
//        Toast.makeText(context, "token--" + token, Toast.LENGTH_SHORT).show();
        apiController = new ApiController(this);

        progressDialog = new ProgressDialog(VerifyAccountActivity.this);
        progressDialog.setCancelable(false);
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setTitle("Loading.....");
                progressDialog.show();
                mannualAddrss = binding.editMannualAddress.getText().toString();
                landmark = binding.editFlatAddress.getText().toString();
                name = binding.editName.getText().toString();
                contactPersonName = binding.editName2.getText().toString();
                contactPersonNo = binding.editName3.getText().toString();
                gst = binding.editName4.getText().toString();
                //dob = binding.editDob.getText().toString();
                if (name.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please enter the company name", Toast.LENGTH_SHORT).show();
                } else if (contactPersonName.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please enter the contact person name", Toast.LENGTH_SHORT).show();
                } else if (contactPersonNo.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please enter the contact person number", Toast.LENGTH_SHORT).show();
//                } else if (binding.liveAddLayout.getVisibility() == View.VISIBLE || basicAddress.isEmpty()) {
//                    progressDialog.dismiss();
//                    Toast.makeText(context, "Please wait we fetching your details", Toast.LENGTH_SHORT).show();
                } else if (/*binding.manuallyAddressLayout.getVisibility() == View.VISIBLE || */mannualAddrss.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please select the address!", Toast.LENGTH_SHORT).show();
                } else if (landmark.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please enter the Landmark", Toast.LENGTH_SHORT).show();
                } else {
//                    progressDialog.dismiss();
//                    if (binding.manuallyAddressLayout.getVisibility() == View.VISIBLE) {
                    apiController.setAddress(mannualAddrss, landmark, city, areaPincode, state, country,
                            latitude, longitude, context, storageUtils, token);
//                    } else {
//                        apiController.setAddress(basicAddress, landmark, city, areaPincode, state, country,
//                                latitude, longitude, context, storageUtils, token);
//                    }
                }
            }
        });

        binding.imgCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int mYear = calendar.get(Calendar.YEAR);
                int mMonth = calendar.get(Calendar.MONTH);
                int mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(VerifyAccountActivity.this, (view1, year, month, dayOfMonth) -> {
                    calendar.set(Calendar.DATE, dayOfMonth);
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);


                    chagneFormat = year + "-" + (month + 1) + "-" + dayOfMonth;
                    String myFormat = "dd-MMM-yyyy";
                    String serverDateFormat = "yyyy-MM-dd";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    SimpleDateFormat sdf1 = new SimpleDateFormat(serverDateFormat,Locale.US);
//                    setDate = Utils.pharseDateMMtoMMM(chagneFormat);
                    dob = sdf1.format(calendar.getTime());
                    binding.editDob.setText(sdf.format(calendar.getTime()));
                }, mYear, mMonth, mDay);
                //  datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 468025136000L);
                datePickerDialog.show();
            }
        });

        binding.addressRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonLive:
                        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(VerifyAccountActivity.this, new String[]
                                    {android.Manifest.permission.ACCESS_FINE_LOCATION,
                                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
                        } else {
                            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                            locationEnabled();
                            getLocation();
                        }
                        break;
                    case R.id.radioaddManully:
                        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                        locationEnabled();
                        getLocation();
                        Intent intent = new Intent(VerifyAccountActivity.this, AddMannulaAddressActivity.class);
                        startActivityForResult(intent, 2);// Activity is started with requestCode 2

                        break;
                }
            }
        });

        binding.genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioMale:
                        gender = binding.radioMale.getText().toString();
                        break;
                    case R.id.radioFemale:
                        gender = binding.radioFemale.getText().toString();
                        break;
                    case R.id.radioOther:
                        gender = binding.radioOther.getText().toString();
                        break;
                }
            }
        });

    }

    private void locationEnabled() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!gps_enabled && !network_enabled) {
            new android.app.AlertDialog.Builder(context)
                    .setTitle("Enable GPS Service")
                    .setMessage("We need your GPS location to show Near Places around you.")
                    .setCancelable(false)
                    .setPositiveButton("Enable", new
                            DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100,
                    5, (LocationListener) this);
            Log.i(TAG, "getLocation: success ");

        } catch (SecurityException e) {
            e.printStackTrace();
            Log.e(TAG, "getLocation: error " + e.getMessage());

        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        Log.i(TAG, "onLocationChanged: called");
        try {
            if(progressDialog!=null && progressDialog.isShowing())
                progressDialog.dismiss();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0);
//            String address2 = addresses.get(0).getAddressLine(2);

//            Log.i(TAG, "onLocationChanged: " + address + " " + address2);
            String local = state = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            city = addresses.get(0).getSubAdminArea();
            areaPincode = addresses.get(0).getPostalCode();
            if (areaPincode.equals("")) {
                areaPincode = null;
            }
            country = addresses.get(0).getCountryName();
            latitude = addresses.get(0).getLatitude();
            longitude = addresses.get(0).getLongitude();
//            finalAdress = local + ", " + city + ", " + state +", "  + areaPincode;

            finalAdress = address;

            binding.editMannualAddress.setText(finalAdress);

//            Toast.makeText(context, "country" + country, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context, "latitude" + latitude, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context, "longitude" + longitude, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Log.e(TAG, "onLocationChanged: Error" + e.getLocalizedMessage());

        }
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Toast.makeText(context, "Gps is not enabled!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if (data != null) {
                String message = data.getStringExtra("MESSAGE");
                LatLng latlng = data.getParcelableExtra("LATLNG");
                latitude = latlng.latitude;
                longitude = latlng.longitude;
                binding.editMannualAddress.setText(message);
            } else {

            }
        }
    }

    @Override
    public void onSuccess(String tag, String id) {
//        Toast.makeText(context, "id --" + id, Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        storageUtils.setDetails(context, StringUtils.UserId, id);

        if (tag.equals(StringUtils.Address)) {
            apiController.setUserDetails(name, mobileNo, id, token, binding.editName2.getText().toString(), binding.editName3.getText().toString(),
                    binding.editName4.getText().toString());
        } else {
            storageUtils.setDetails(context, StringUtils.UserName, name);
            storageUtils.setDetails(context, StringUtils.UserDob, dob);
            startActivity(new Intent(VerifyAccountActivity.this, SelectDealerActivity.class).
                    putExtra("userDetailsId", id));
            saveCustomerID(id);
            storageUtils.setDetails(context, StringUtils.myAddressLat, String.valueOf(latitude));
            storageUtils.setDetails(context, StringUtils.myAddressLong, String.valueOf(longitude));
            storageUtils.setDetails(context, StringUtils.AccountVerified, StringUtils.Yes);
        }
    }

    @Override
    public void onFailure(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "faliour --" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "error --" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == 101 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if(!progressDialog.isShowing()) {
                    progressDialog.show();
                    progressDialog.setTitle("Please wait Fetching Location...");
                }
                locationEnabled();
                getLocation();
            }
        } catch (Exception e) {

        }

    }
}