package verify_account;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.shockwave.pdfium.PdfDocument;
import com.spacetexhdemo.AadharCardUploadActivity;
import com.spacetexhdemo.PhoneNumberActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityMedicalReportBinding;
import com.spacetexhdemo.databinding.ChooseDocumentUploadBinding;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import sharedprefrence.StorageUtils;
import utils.FilePath;
import utils.FileUtils;
import utils.StringUtils;

public class MedicalReport extends AppCompatActivity implements ApiResponseListener, OnPageChangeListener, OnLoadCompleteListener {
    ActivityMedicalReportBinding binding;
    private static final int PICK_IMAGE_REQUEST = 1;
    Context context;
    private Bitmap bitmap;
    Boolean isValidFile;
    Integer pageNumber = 0;
    private String filePath, token;
    String id;
    private ApiController apiController;
    StorageUtils storageUtils;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_medical_report);
        context = MedicalReport.this;
        storageUtils = new StorageUtils();
        apiController = new ApiController(this);

        token = storageUtils.getDetails(context, StringUtils.UserToken);
//        Toast.makeText(context, "token--" + token, Toast.LENGTH_SHORT).show();
        id = storageUtils.getDetails(context, StringUtils.UserId);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MedicalReport.this, Doctorprescription.class));
            }
        });
        binding.imgReportUPload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseDocumentUploadBinding binding = DataBindingUtil.bind(getLayoutInflater()
                        .inflate(R.layout.choose_document_upload, null));
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context, R.style.BottomSheetDialog);
                assert binding != null;
                bottomSheetDialog.setContentView(binding.getRoot());

                binding.selectImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, 100);
                        bottomSheetDialog.dismiss();
                    }
                });
                binding.selectPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("application/pdf");
                        try {
                            startActivityForResult(intent, 200);
                            bottomSheetDialog.dismiss();
                        } catch (ActivityNotFoundException e) {
                            //alert user that file manager not working
                            Toast.makeText(MedicalReport.this,
                                    " Please select the file!", Toast.LENGTH_SHORT).show();
                            bottomSheetDialog.dismiss();
                        }
                    }
                });
                bottomSheetDialog.show();

//
//               Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
////                intent.setType("*/*");
//                intent.setType("image/*|application/pdf/*");
//                String[] mimetypes = {"image/*", "application/*|text/*"};
//                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
//                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });

        binding.btnSkip.setOnClickListener(v->{
            startActivity(new Intent(context, Doctorprescription.class));
            storageUtils.setDetails(context, StringUtils.MedicalReport, StringUtils.Yes);
        });
        allowpermisions();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 200) {
            binding.imgSelectedImage.setVisibility(View.GONE);
            binding.pdfView.setVisibility(View.VISIBLE);
            Uri uri = data.getData();
            displayFromUri(uri);
            assert uri != null;
            filePath = uri.getPath();

//            if (filePath.isEmpty()) {
//                Toast.makeText(context, "Please select the pdf file", Toast.LENGTH_SHORT).show();
//            } else {
//                uploadMedicalDocument();
//            }

            isValidFile = checkOtherFileType(filePath);
            if (isValidFile) {
                uploadMedicalDocument();
            } else {
                Toast.makeText(context, "Please select the pdf file", Toast.LENGTH_SHORT).show();
            }
        }
        if (resultCode == RESULT_OK && requestCode == 100) {
            binding.imgSelectedImage.setVisibility(View.VISIBLE);
            binding.pdfView.setVisibility(View.GONE);
            Uri uri = data.getData();
//            filePath = getPDFPath(this, data.getData());
            filePath = FilePath.getPath(context, uri);
            Toast.makeText(this, "image Path--" + filePath,
                    Toast.LENGTH_SHORT).show();
            binding.imgSelectedImage.setImageURI(uri);
            if (filePath != null) {
                uploadMedicalDocument();
            } else {
                Toast.makeText(context, R.string.no_image_selected, Toast.LENGTH_LONG).show();
            }
        }

//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
//                data != null && data.getData() != null) {
//            Uri picUri = data.getData();
//            filePath = FilePath.getPath(context, picUri);
////            binding.tvFilePath.setText(filePath);
//            binding.pdfView.fromAsset(filePath);
//
//            if (filePath != null) {
//                try {
////                    binding.imgReportUPload.setImageURI(picUri);
//                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), picUri);
//                    uploadMedicalDocument(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Toast.makeText(context, R.string.no_image_selected, Toast.LENGTH_LONG).show();
//            }
//        }
    }

    private void uploadMedicalDocument() {
        progressDialog = new ProgressDialog(MedicalReport.this);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*|application/pdf/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("medical_report", file.getName(), requestBody);
        apiController.updateMedicalReport(part, id, token);
    }

    private void allowpermisions() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};

        if (ContextCompat.checkSelfPermission(context,
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,
                permissions[1]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,
                permissions[2]) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Permission Granted!", Toast.LENGTH_SHORT).show();
            //  setupViewPager();
        } else {
            ActivityCompat.requestPermissions(MedicalReport.this,
                    permissions,
                    1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        allowpermisions();
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context.getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    allowpermisions();
                } else {
                    Toast.makeText(context.getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Boolean checkOtherFileType(String filePath) {
        if (!filePath.isEmpty() && filePath != null) {
            String filePathInLowerCase = filePath.toLowerCase();
            if (filePathInLowerCase.endsWith(".pdf")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onSuccess(String tag, String superClassCastBean) {
        progressDialog.dismiss();
//        startActivity(new Intent(MedicalReport.this, Doctorprescription.class)
//                /*.putExtra("userId", id)*/);
        storageUtils.setDetails(context, StringUtils.MedicalReport, StringUtils.Yes);
        Toast.makeText(context, "Upload successfully!" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "onFailure-----" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "onError--" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {

    }

    private void displayFromUri(Uri uri) {
//        pdfFileName = getFileName(uri);
        binding.pdfView.fromUri(uri)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .load();
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = binding.pdfView.getDocumentMeta();
        printBookmarksTree(binding.pdfView.getTableOfContents(), "-");
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
//        pageNumber = page;
//        setTitle(String.format("%s %s / %s", fileName, page + 1, pageCount));
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

//            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

}