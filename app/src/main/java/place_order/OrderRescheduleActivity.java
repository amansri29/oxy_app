package place_order;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;

import com.google.gson.JsonObject;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityOrderRescheduleBinding;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import adapters.DeliveryDateAdapter;
import listener.GetSelectedDeliveryDateListener;
import modal.AllOrdersGetModal;
import modal.DeliveryDateModal;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class OrderRescheduleActivity extends AppCompatActivity {
    ActivityOrderRescheduleBinding binding;
    ArrayList<DeliveryDateModal> arrayList;
    DeliveryDateAdapter deliveryDateAdapter;
    private SwitchCompat on_Off;
    String token, finalDate = "", selectedDeliverydate = "", amount;
    int repeatDays = 0;
    StorageUtils storageUtils;
    Context context;
    String chagneFormat, totalAmount;
    int orderId;
    //    List<OrderItem> orderItemsList;
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private int required_position = -1;
    private String alreadySelectedDate;
    private String status, returned_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_reschedule);
        context = OrderRescheduleActivity.this;
        storageUtils = new StorageUtils();
        amount = storageUtils.getDetails(context, StringUtils.Payment);
        token = storageUtils.getDetails(context, StringUtils.UserToken);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);

        orderId = Integer.parseInt(getIntent().getStringExtra("orerID"));
        totalAmount = getIntent().getStringExtra("totalAmount");
        alreadySelectedDate = getIntent().getStringExtra("date");
        status = getIntent().getStringExtra("status");
        returned_date = getIntent().getStringExtra("return_date");

        if(status.equals("Return Requested")){
            alreadySelectedDate = returned_date;
        }


        Log.d("reschedule", "onCreate: " + alreadySelectedDate);
//        Toast.makeText(context, "orderId--" + orderId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(context, "totalAmount--" + totalAmount, Toast.LENGTH_SHORT).show();

        binding.tvTotlaAmount.setText(totalAmount);

        arrayList = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        SimpleDateFormat sdfullFormat = new SimpleDateFormat("yyyy-MM-dd ");
        for (int i = 1; i < 8; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            String fulldate = sdfullFormat.format(calendar.getTime());
            if (fulldate.trim().equalsIgnoreCase(alreadySelectedDate)) {
                required_position = i - 1;
            }
            arrayList.add(new DeliveryDateModal(day, fulldate));
        }


        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedDeliverydate.equals("")) {
                    Toast.makeText(context, "Please select the delivery date!", Toast.LENGTH_SHORT).show();

                } else {
//                    startActivity(new Intent(OrderRescheduleActivity.this,
//                            PlacedOrderAddressActivity.class).
//                            putExtra("listitem", (Serializable) orderItemsList).
//                            putExtra("selectedProductDetails", (Serializable) selectedProductDetailsList));
                    updateOrderDeliveryDate(selectedDeliverydate.trim(), progressDialog);
                }
                repeatDays = binding.repeatDaysPicker.getValue();
                storageUtils.setDetails(OrderRescheduleActivity.this,
                        StringUtils.RepeatDays, String.valueOf(repeatDays));

                storageUtils.setDetails(OrderRescheduleActivity.this,
                        StringUtils.RepeatDate, finalDate);

                storageUtils.setDetails(OrderRescheduleActivity.this,
                        StringUtils.selectedDeliverydate, selectedDeliverydate.trim());
            }
        });

        binding.editDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });
        binding.editMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });
        binding.editYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        binding.navswRepeatDelivery.setOnClickListener(v -> {
            if (binding.navswRepeatDelivery.isChecked()) {
                binding.repeatQuantatyLayout.setVisibility(View.VISIBLE);
                binding.addressDateLayout.setVisibility(View.VISIBLE);
//                textView_onOff.setText(R.string.Online);
            } else {
                binding.repeatQuantatyLayout.setVisibility(View.GONE);
                binding.addressDateLayout.setVisibility(View.GONE);
//                textView_onOff.setText(R.string.Offline);
            }
        });


        binding.deliveryDateRecycler.setHasFixedSize(true);
        deliveryDateAdapter = new DeliveryDateAdapter(OrderRescheduleActivity.this, arrayList, new GetSelectedDeliveryDateListener() {
            @Override
            public void getDeliveryDate(int position) {
                selectedDeliverydate = arrayList.get(position).getDeliveryFulldateName();
                storageUtils.setDetails(OrderRescheduleActivity.this, StringUtils.DeliveryDate, selectedDeliverydate.trim());
            }
        });
        deliveryDateAdapter.setPosi(required_position);
        binding.deliveryDateRecycler.setAdapter(deliveryDateAdapter);
    }

    public void selectDate() {
        final Calendar calendar = Calendar.getInstance();
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(OrderRescheduleActivity.this
                , (view1, year, month, dayOfMonth) -> {
            chagneFormat = year + "-" + (month + 1) + "-" + dayOfMonth;
            String myFormat = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            finalDate = sdf.format(calendar.getTime());
            String Year = dateYear(finalDate);
            String Month = dateMonth(finalDate);
            String Day = dateDay(finalDate);

            binding.editDay.setText(Day);
            binding.editMonth.setText(Month);
            binding.editYear.setText(Year);

        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public String dateYear(String str) {
        return str.length() < 2 ? str : str.substring(0, 4);
    }

    public String dateMonth(String str) {
        return str.length() < 2 ? str : str.substring(5, 7);
    }

    public String dateDay(String str) {
        return str.length() < 2 ? str : str.substring(8, 10);
    }

    private void updateOrderDeliveryDate(String selectedDeliverydate, ProgressDialog progressDialog) {

        if(!progressDialog.isShowing()){
            progressDialog.show();
        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        JsonObject jsonObject = new JsonObject();
        if(status.equals("Return Requested")){
            jsonObject.addProperty("return_date", selectedDeliverydate);
        }
        else {
            jsonObject.addProperty("expected_delivery_date", selectedDeliverydate);
        }

        String Token = "Token " + token;
        Call<AllOrdersGetModal> call = apiInterface.updateOrdereliveryDate(Token, jsonObject, orderId);
        call.enqueue(new Callback<AllOrdersGetModal>() {
            @Override
            public void onResponse(@NotNull Call<AllOrdersGetModal> call,
                                   @NotNull Response<AllOrdersGetModal> response) {
                progressDialog.dismiss();
                AllOrdersGetModal allDealersModals = response.body();

                if (allDealersModals != null) {
                    finish();
                    Toast.makeText(OrderRescheduleActivity.this, "Order Reschedule successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllOrdersGetModal> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OrderRescheduleActivity.this, "Faliour--"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}