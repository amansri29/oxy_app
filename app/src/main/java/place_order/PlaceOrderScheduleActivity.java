package place_order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityPlaceOrderScheduleBinding;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import adapters.DeliveryDateAdapter;
import listener.GetSelectedDeliveryDateListener;
import modal.DeliveryDateModal;
import modal.OrderItem;
import modal.SelectedProductDetails;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class PlaceOrderScheduleActivity extends AppCompatActivity {
    ActivityPlaceOrderScheduleBinding binding;
    ArrayList<DeliveryDateModal> arrayList;
    DeliveryDateAdapter deliveryDateAdapter;
    private SwitchCompat on_Off;
    String token, finalDate = "", selectedDeliverydate = "", amount;
    int repeatDays = 0;
    StorageUtils storageUtils;
    Context context;
    String chagneFormat, setDate;
    List<OrderItem> orderItemsList;
    List<SelectedProductDetails> selectedProductDetailsList;
    private static final String TAG = "PlaceOrderScheduleActiv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_place_order_schedule);
        context = PlaceOrderScheduleActivity.this;
        storageUtils = new StorageUtils();
        amount = storageUtils.getDetails(context, StringUtils.Payment);

        arrayList = new ArrayList<>();
        orderItemsList = new ArrayList<>();

        orderItemsList = (List<OrderItem>) getIntent().getSerializableExtra("listitem22");

        selectedProductDetailsList = (List<SelectedProductDetails>) getIntent().getSerializableExtra("selectedProductDetails");


        Log.i(TAG, "onCreate: " +  selectedProductDetailsList.size() + " " + selectedProductDetailsList.get(0).getProductName());

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        SimpleDateFormat sdfullFormat = new SimpleDateFormat("yyyy-MM-dd ");
        for (int i = 1; i < 8; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            String fulldate = sdfullFormat.format(calendar.getTime());
            arrayList.add(new DeliveryDateModal(day, fulldate));
        }

        binding.tvTotlaAmount.setText(amount);

        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                repeatDays = binding.repeatDaysPicker.getValue();
//                Toast.makeText(context, "repeat days--" + repeatDays, Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, "finalDate--" + finalDate, Toast.LENGTH_SHORT).show();



                if (selectedDeliverydate.equals("")) {

                    Toast.makeText(context, "Please select the delivery date!", Toast.LENGTH_SHORT).show();

                } else {

                    storageUtils.setDetails(PlaceOrderScheduleActivity.this,
                            StringUtils.RepeatDays, String.valueOf(repeatDays));

                    storageUtils.setDetails(PlaceOrderScheduleActivity.this,
                            StringUtils.RepeatDate, finalDate);

                    storageUtils.setDetails(PlaceOrderScheduleActivity.this,
                            StringUtils.selectedDeliverydate, selectedDeliverydate.trim());

                    startActivity(new Intent(PlaceOrderScheduleActivity.this,
                            PlacedOrderAddressActivity.class).
                            putExtra("listitem", (Serializable) orderItemsList).
                            putExtra("selectedProductDetails", (Serializable) selectedProductDetailsList));
                }
            }
        });
        binding.editDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });
        binding.editMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });
        binding.editYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        binding.navswRepeatDelivery.setOnClickListener(v -> {
            if (binding.navswRepeatDelivery.isChecked()) {
                binding.repeatQuantatyLayout.setVisibility(View.VISIBLE);
                binding.addressDateLayout.setVisibility(View.VISIBLE);
//                textView_onOff.setText(R.string.Online);
            } else {
                binding.repeatQuantatyLayout.setVisibility(View.GONE);
                binding.addressDateLayout.setVisibility(View.GONE);
//                textView_onOff.setText(R.string.Offline);
            }
        });


        binding.deliveryDateRecycler.setHasFixedSize(true);
        deliveryDateAdapter = new DeliveryDateAdapter(PlaceOrderScheduleActivity.this, arrayList, new GetSelectedDeliveryDateListener() {
            @Override
            public void getDeliveryDate(int position) {
                selectedDeliverydate = arrayList.get(position).getDeliveryFulldateName();
//                Toast.makeText(context, "DeliveryDate----" + selectedDeliverydate, Toast.LENGTH_SHORT).show();
                storageUtils.setDetails(PlaceOrderScheduleActivity.this, StringUtils.DeliveryDate, selectedDeliverydate.trim());
            }
        });
        binding.deliveryDateRecycler.setAdapter(deliveryDateAdapter);
    }

    public void selectDate() {
        final Calendar calendar = Calendar.getInstance();
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(PlaceOrderScheduleActivity.this
                , (view1, year, month, dayOfMonth) -> {
            chagneFormat = year + "-" + (month + 1) + "-" + dayOfMonth;
            String myFormat = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            finalDate = sdf.format(calendar.getTime());
            String Year = dateYear(finalDate);
            String Month = dateMonth(finalDate);
            String Day = dateDay(finalDate);

            binding.editDay.setText(Day);
            binding.editMonth.setText(Month);
            binding.editYear.setText(Year);

        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public String dateYear(String str) {
        return str.length() < 2 ? str : str.substring(0, 4);
    }

    public String dateMonth(String str) {
        return str.length() < 2 ? str : str.substring(5, 7);
    }

    public String dateDay(String str) {
        return str.length() < 2 ? str : str.substring(8, 10);
    }
}