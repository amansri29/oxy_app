package place_order;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.cashfree.pg.CFPaymentService;
import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityPlaceOrderPaymentBinding;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import adapters.SelectedProductAdapter;
import modal.AllOrdersGetModal;
import modal.OrderItem;
import modal.SelectedProductDetails;
import modal.UpdateOrderPaymentId;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class PlaceOrderPaymentActivity extends AppCompatActivity implements ApiResponseListener {
    ActivityPlaceOrderPaymentBinding binding;
    private static final String TAG = "PlaceOrderPaymentActivi";
    StorageUtils storageUtils;
    Context context;
    int finalAmount;
    double refundAmount, deliveryChargeAmount, gstAmount;
    String placedORderDate, placedOrdertype, repeatDays, repeatDateTill, selectedAddress,
            selectedAddressId, orderId, selectedCompanyName, totalamount, userPlacedOrderId;
    List<OrderItem> orderItemsList;
    String selectedChekbox = "", token, paymentToken;
    int paymentId = 0, refrenceId = 0, selectedDealerId;
    boolean isOnline = false;
    private ApiController apiController;
    ProgressDialog progressDialog;
    private APIInterface apiInterface;
    List<SelectedProductDetails> selectedProductDetailsList;
    SelectedProductAdapter selectedProductAdapter;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_place_order_payment);
        context = PlaceOrderPaymentActivity.this;
        storageUtils = new StorageUtils();
        orderItemsList = new ArrayList<>();
        selectedProductDetailsList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setCancelable(false);

        orderItemsList = (List<OrderItem>) getIntent().getSerializableExtra("listitem");
        selectedProductDetailsList = (List<SelectedProductDetails>) getIntent().getSerializableExtra("selectedProductDetails");

        Log.i(TAG, "onCreate: " + selectedProductDetailsList.size() + " " + selectedProductDetailsList.get(0).getProductName());
        apiController = new ApiController(this);


        repeatDays = storageUtils.getDetails(PlaceOrderPaymentActivity.this, StringUtils.RepeatDays);
        placedORderDate = storageUtils.getDetails(PlaceOrderPaymentActivity.this, StringUtils.selectedDeliverydate);
        selectedAddressId = storageUtils.getDetails(PlaceOrderPaymentActivity.this, StringUtils.PlaceOrderAddress);
        selectedCompanyName = storageUtils.getDetails(context, StringUtils.SelectedDealerCompanyName);
        selectedAddress = storageUtils.getDetails(context, StringUtils.SelectedAddress);
        placedOrdertype = storageUtils.getDetails(context, StringUtils.OrderType);

        if(placedOrdertype.equals(""))
        {
            placedOrdertype = "New";
        }

        Float totalSecurityDeposit = 0.0f;

        if(placedOrdertype.equals("New"))
        {
            for(SelectedProductDetails selectedProductDetails : selectedProductDetailsList)
            {
                if(selectedProductDetails.getSecurityAmount() > 0)
                {
                    Float securityAmount = selectedProductDetails.getSecurityAmount() * selectedProductDetails.getQuantity();
                    totalSecurityDeposit = securityAmount + totalSecurityDeposit;
                    SelectedProductDetails selectedProductDetails1 = new SelectedProductDetails("Security Amount " +  selectedProductDetails.getProductName(),
                            selectedProductDetails.getProductType(),   Float.toString(securityAmount) , selectedProductDetails.getQuantity(), 0.0f);
                    selectedProductDetailsList.add(selectedProductDetails1);
                    break;
                }

            }
        }
        setUpSelectedItemRecycler(selectedProductDetailsList);

        repeatDateTill = storageUtils.getDetails(PlaceOrderPaymentActivity.this, StringUtils.RepeatDate);
        if (repeatDateTill.isEmpty()) {
            repeatDateTill = null;
        }

        token = storageUtils.getDetails(context, StringUtils.UserToken);
        totalamount = storageUtils.getDetails(context, StringUtils.Payment);
        selectedDealerId = Integer.parseInt(storageUtils.getDetails(context, StringUtils.DealerId));

//        Toast.makeText(context, "totalamount--" + totalamount, Toast.LENGTH_SHORT).show();

        refundAmount = 0;

//        todo fetch from backend
        double delivery_charge = 50;

        binding.tvDeliveryChargeAmount.setText(Double.toString(delivery_charge));

        deliveryChargeAmount = Double.parseDouble(binding.tvDeliveryChargeAmount.getText().toString());


        double gst_amount = (Double.parseDouble(totalamount) * 5.00) / 100;


        binding.tvGstAmount.setText(Double.toString(gst_amount));

        gstAmount = Double.parseDouble(binding.tvGstAmount.getText().toString());
        finalAmount = (int) (refundAmount + delivery_charge + gst_amount + Double.parseDouble(totalamount) + totalSecurityDeposit);

        binding.tvTotal.setText("₹ " + String.valueOf(finalAmount));

        binding.tvOrderType.setText(placedOrdertype);

        float totalAmountWithoutGST = Float.parseFloat(totalamount) + (Float) totalSecurityDeposit;
        binding.tvTotalWithoutGST.setText(Float.toString(totalAmountWithoutGST));

        if (Integer.parseInt(repeatDays) > 0) {
            binding.repeatContainer.setVisibility(View.VISIBLE);
            binding.tvRepeatDays.setText("Repeat every " + repeatDays + " days");
        } else {
            binding.repeatContainer.setVisibility(View.GONE);
        }


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(placedORderDate);
            SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            binding.tvSelectedOrderDate.setText(prettyDateFormat.format(d));
        }
        catch (Exception e){

        }

        binding.tvDealerCompnayName.setText(selectedCompanyName);
        binding.tvAddressName.setText(selectedAddress);

        isOnline = true;
        binding.codCheck.setChecked(false);
        selectedChekbox = binding.onlineCheck.getText().toString().trim();
        binding.btnProceedToPay.setText("PROCEED TO PAY " + String.valueOf(finalAmount));

        binding.editAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(PlaceOrderPaymentActivity.this,
                        PlacedOrderAddressActivity.class)
                        .putExtra("listitem", (Serializable) orderItemsList)
                        .putExtra("selectedProductDetails",(Serializable) selectedProductDetailsList));*/
                onBackPressed();
            }
        });

        binding.codCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.onlineCheck.setChecked(false);
                    selectedChekbox = binding.codCheck.getText().toString().trim();
                    binding.btnProceedToPay.setText("Place Order");
                    isOnline = false;
                }
            }
        });

        binding.onlineCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isOnline = true;
                    binding.codCheck.setChecked(false);
                    selectedChekbox = binding.onlineCheck.getText().toString().trim();
                    binding.btnProceedToPay.setText("PROCEED TO PAY " + String.valueOf(finalAmount));
                }
            }
        });

        binding.btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String btnText = binding.btnProceedToPay.getText().toString();
                if (btnText.equals("Place Order")) {
                    if(!progressDialog.isShowing()) {
                        progressDialog.show();
                        placedCodOrder();
                    }
                } else {
                    if(!progressDialog.isShowing()) {
                        progressDialog.show();
                        placedOnlineOrder();
                    }
                }
            }
        });

        binding.imgBAck.setOnClickListener(v->{
            onBackPressed();
        });
    }

    private void placedCodOrder() {
        try {
            Log.i(TAG, "placedCodOrder: called" );
            apiController.placedOrder(String.valueOf(finalAmount), placedORderDate, selectedAddressId,
                    "Cash on delivery", repeatDays, repeatDateTill, token, orderItemsList,
                    paymentId, placedOrdertype, selectedDealerId);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "placedCodOrder: error " +  e.getLocalizedMessage());
        }
    }

    private void placedOnlineOrder() {
        try {
            apiController.placedOnlineOrder(String.valueOf(finalAmount), placedORderDate, selectedAddressId,
                    selectedChekbox, repeatDays, repeatDateTill, token, orderItemsList,
                    paymentId, placedOrdertype, selectedDealerId, new ProgressDialog(PlaceOrderPaymentActivity.this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String orderid, String orderToken) {
        if (isOnline) {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            orderId = orderid;
            paymentToken = orderToken;
            getInputParams();
        } else {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            startActivity(new Intent(PlaceOrderPaymentActivity.this, MainActivity.class));
            finish();
            Toast.makeText(context, "Order Placed successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(String msg) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        Toast.makeText(context, "onFailure---" + msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onError(String msg) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        Toast.makeText(context, "onError---" + msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPerformCode(int code) {

    }

    private void getInputParams() {
        Log.i(TAG, "getInputParams2222: " + orderId + "  " + finalAmount);
        HashMap<String, String> params = new HashMap<>();
        params.put(CFPaymentService.PARAM_APP_ID, StringUtils.AppId);
        params.put(CFPaymentService.PARAM_ORDER_ID, orderId);
        params.put(CFPaymentService.PARAM_ORDER_AMOUNT, String.valueOf(finalAmount));


//        params.put(CFPaymentService.PARAM_CUSTOMER_NAME, "Aman Srivastav");
        params.put(CFPaymentService.PARAM_CUSTOMER_EMAIL, "dcc9dcc@gmail.com");
        params.put(CFPaymentService.PARAM_CUSTOMER_PHONE, "8619368171");


//        params.put(CFPaymentService.PARAM_NOTIFY_URL, "https://www.trucksampark.com");
        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        Log.i(TAG, "getInputParams: paymentToken " + paymentToken);
        cfPaymentService.doPayment(this, params, paymentToken, "PROD", "#3B6EFF", "#ffffff");
        //cfPaymentService.doPayment(this, params, paymentToken, "TEST", "#3B6EFF", "#ffffff");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "API Response : ");
        Log.d("responce", "API Response : " + data.getStringExtra("id"));
        if (requestCode == CFPaymentService.REQ_CODE && data != null) {
            try {
                Bundle bundle = data.getExtras();
                String responce = data.getStringExtra("id");
                if (bundle != null) {
                    status = bundle.getString("txStatus");
                    refrenceId = Integer.parseInt(bundle.getString("referenceId"));
                    updatePaymentId(status);
                } else {
                    Toast.makeText(this, "Payment failed!", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Log.d(TAG, "onActivityResult: user cancelled");
            }
        } else {
            Toast.makeText(this, "Payment failed!", Toast.LENGTH_LONG).show();
        }

    }

    public void updatePaymentId(String status) {
        updateOrderPaymentId(refrenceId, token, Integer.parseInt(orderId),status);
    }

    public void updateOrderPaymentId(int paymentId, String token,
                                     int orderId, String status) {
        if(!progressDialog.isShowing()){
            progressDialog.show();
        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }

        UpdateOrderPaymentId updeOrderId = new UpdateOrderPaymentId();
        updeOrderId.setPayment_id(paymentId);
        Log.d(TAG, "updateOrderPaymentId: "+status);
        if(status.equals("FAILED")){
            updeOrderId.setStatus("Payment Pending");
        }
        else{
            updeOrderId.setStatus("Confirmed");
        }
        String headerToken = "Token " + token;

        Log.e("Order", String.valueOf(updeOrderId));

        Call<AllOrdersGetModal> call = apiInterface.updatePaymentId(headerToken, updeOrderId, orderId);
        call.enqueue(new Callback<AllOrdersGetModal>() {
            @Override
            public void onResponse(@NotNull Call<AllOrdersGetModal> call,
                                   @NotNull Response<AllOrdersGetModal> response) {
                AllOrdersGetModal detailsModal = response.body();
                Log.d("Respoce", String.valueOf(response.code()));
                if (detailsModal != null) {
                    Toast.makeText(PlaceOrderPaymentActivity.this, "Payment id update successfully!"
                            , Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(PlaceOrderPaymentActivity.this, MainActivity.class));
                    finish();
                } else {
                    Toast.makeText(PlaceOrderPaymentActivity.this, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<AllOrdersGetModal> call, @NotNull Throwable t) {
                Toast.makeText(PlaceOrderPaymentActivity.this, "Faliour" + t.getMessage()
                        , Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void setUpSelectedItemRecycler(List<SelectedProductDetails> selectedProductDetailsList) {
        selectedProductAdapter = new SelectedProductAdapter(this, selectedProductDetailsList);
        binding.selectedRecycler.setHasFixedSize(true);
        binding.selectedRecycler.setAdapter(selectedProductAdapter);
    }
}