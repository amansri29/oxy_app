package place_order;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.gson.JsonObject;
import com.spacetexhdemo.AddMannulaAddressActivity;
import com.spacetexhdemo.AddNewAddressActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityPlacedOrderAddressBinding;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import adapters.SelectAddressAdapter;
import listener.GetSelectedAddressIdListerner;
import modal.AllAddressModal;
import modal.OrderItem;
import modal.SelectedProductDetails;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class PlacedOrderAddressActivity extends AppCompatActivity implements LocationListener {
    ActivityPlacedOrderAddressBinding binding;
    private static final String TAG = "New Address saved";
    LocationManager locationManager;
    SelectAddressAdapter selectAddressAdapter;
    private RadioButton radioButton;
    Context context;
    String city, pincode, state, country;
    int selectedDeliveryAddressID = 0;
    double longitude, latitude;
    StorageUtils storageUtils;
    String token, amount, defaultAddressId = "";
    APIInterface apiInterface;
    List<OrderItem> orderItemsList;
    List<SelectedProductDetails> selectedProductDetailsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_placed_order_address);
        context = PlacedOrderAddressActivity.this;
        storageUtils = new StorageUtils();
        amount = storageUtils.getDetails(context, StringUtils.Payment);
        defaultAddressId = storageUtils.getDetails(context, StringUtils.DefaultAddress);
        token = storageUtils.getDetails(context, StringUtils.UserToken);

//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        locationEnabled();
//        getLocation();

        binding.tvTotlaAmount.setText(amount);
        orderItemsList = new ArrayList<>();
        selectedProductDetailsList = new ArrayList<>();

        orderItemsList = (List<OrderItem>) getIntent().getSerializableExtra("listitem");
        selectedProductDetailsList = (List<SelectedProductDetails>) getIntent().getSerializableExtra("selectedProductDetails");


        Log.i(TAG, "onCreate: " + selectedProductDetailsList.size() + " " + selectedProductDetailsList.get(0).getProductName());

        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        int selectedId = binding.addressRadioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading....");
        progressDialog.show();
        getAllAddress(token, progressDialog);

        binding.btnSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Loading.....");
                progressDialog.setCancelable(false);
                progressDialog.show();
                String landmarkName = binding.editAppartmentName.getText().toString();
                String fullAddres = binding.editFullAddress.getText().toString();

                if (landmarkName.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(PlacedOrderAddressActivity.this, "Please enter the apartment name!", Toast.LENGTH_SHORT).show();
                } else if (fullAddres.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(PlacedOrderAddressActivity.this, "Please enter the street address!", Toast.LENGTH_SHORT).show();
                } else {
                    saveNewAddress(token, progressDialog, landmarkName, fullAddres);
                }
            }
        });

        binding.btnViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (defaultAddressId.equals("")) {
                    Toast.makeText(PlacedOrderAddressActivity.this,
                            "Please select the Address!" + selectedDeliveryAddressID
                            , Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(PlacedOrderAddressActivity.this,
                            PlaceOrderPaymentActivity.class).
                            putExtra("listitem", (Serializable) orderItemsList).
                            putExtra("selectedProductDetails", (Serializable) selectedProductDetailsList));
                }
            }
        });

        binding.addressRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioaddManullyAddress:
                        binding.scheduleAddressEdittExtLayout.setVisibility(View.VISIBLE);
                        binding.btnSaveAddress.setVisibility(View.VISIBLE);
                        binding.btnViewOrder.setVisibility(View.GONE);
//                        binding.nestedLayout.scrollTo(100, binding.tvAprtmentNameLayout.getBottom());
                        break;
                }
            }
        });

        binding.AddNewAddressRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonLive:
                        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(PlacedOrderAddressActivity.this, new String[]
                                    {android.Manifest.permission.ACCESS_FINE_LOCATION,
                                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
                        } else {
                            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                            locationEnabled();
                            getLocation();
                        }
                        break;
                    case R.id.radioaddManully:
                        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                        locationEnabled();
                        getLocation();
                        Intent intent = new Intent(PlacedOrderAddressActivity.this,
                                AddMannulaAddressActivity.class);
                        startActivityForResult(intent, 2);// Activity is started with requestCode 2

                        break;
                }
            }
        });

    }

    private void saveNewAddress(String token, ProgressDialog progressDialog, String landmarkName, String fullAddres) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("basic_address", fullAddres);
        jsonObject.addProperty("landmark", landmarkName);
        jsonObject.addProperty("city", city);
        jsonObject.addProperty("pincode", pincode);
        jsonObject.addProperty("state", state);
        jsonObject.addProperty("country", country);
        jsonObject.addProperty("latitude", latitude);
        jsonObject.addProperty("longitude", longitude);

        String Token = "Token " + token;
        Call<AllAddressModal> call = apiInterface.saveDefaultAddress(Token, jsonObject);
        call.enqueue(new Callback<AllAddressModal>() {
            @Override
            public void onResponse(@NotNull Call<AllAddressModal> call,
                                   @NotNull Response<AllAddressModal> response) {
                progressDialog.dismiss();
                AllAddressModal allDealersModals = response.body();
                if (allDealersModals != null) {
                    selectedDeliveryAddressID = allDealersModals.getId();
                    Toast.makeText(PlacedOrderAddressActivity.this, "Address saved successfully!",
                            Toast.LENGTH_SHORT).show();
                    binding.btnSaveAddress.setVisibility(View.GONE);
                    binding.btnViewOrder.setVisibility(View.VISIBLE);
                    binding.scheduleAddressEdittExtLayout.setVisibility(View.GONE);
//                    getAllAddress(token, progressDialog);
                    startActivity(new Intent(PlacedOrderAddressActivity.this, PlacedOrderAddressActivity.class)
                            .putExtra("listitem", (Serializable) orderItemsList));

                } else {
                    Toast.makeText(PlacedOrderAddressActivity.this, "Error occurred!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllAddressModal> call, @NotNull Throwable t) {
                Log.e("Faliour", Objects.requireNonNull(t.getMessage()));
                Toast.makeText(PlacedOrderAddressActivity.this, "Failure" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void getAllAddress(String token, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllAddressModal>> call = apiInterface.getAllSelectedAddress(Token);
        call.enqueue(new Callback<List<AllAddressModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllAddressModal>> call,
                                   @NotNull Response<List<AllAddressModal>> response) {
                progressDialog.dismiss();
                List<AllAddressModal> allDealersModals = response.body();
                //// add default address
                allDealersModals.add(new AllAddressModal(0, "", "",
                        "Add new address", "", "", "",
                        "", "", 0.0, 0.0));

                if (allDealersModals != null) {
                    Toast.makeText(PlacedOrderAddressActivity.this,
                            "Address fetched successfully!",
                            Toast.LENGTH_SHORT).show();
                    binding.addressRecyclerView.setHasFixedSize(true);
                    selectAddressAdapter = new SelectAddressAdapter(PlacedOrderAddressActivity.this,
                            allDealersModals, new GetSelectedAddressIdListerner() {
                        @Override
                        public void getSelectedAddressId(int position, String finalAddress) {
                            defaultAddressId = String.valueOf(allDealersModals.get(position).getId());
                            binding.radioaddManullyAddress.setChecked(false);
                            storageUtils.setDetails(context, StringUtils.SelectedAddress, finalAddress);
                            storageUtils.setDetails(context, StringUtils.PlaceOrderAddress, defaultAddressId);

                            binding.scheduleAddressEdittExtLayout.setVisibility(View.GONE);
                            binding.btnSaveAddress.setVisibility(View.GONE);
                            binding.btnViewOrder.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void openAddNewAddressLayout(int position) {
                            binding.scheduleAddressEdittExtLayout.setVisibility(View.VISIBLE);
                            binding.btnSaveAddress.setVisibility(View.VISIBLE);
                            binding.btnViewOrder.setVisibility(View.GONE);

                        }
                    });
                    binding.addressRecyclerView.setAdapter(selectAddressAdapter);
                } else {
                    Toast.makeText(PlacedOrderAddressActivity.this, "Error occurred!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllAddressModal>> call, @NotNull Throwable t) {
                Log.e("Faliour", Objects.requireNonNull(t.getMessage()));
                Toast.makeText(PlacedOrderAddressActivity.this, "Failure" + t.getMessage(),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void locationEnabled() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!gps_enabled && !network_enabled) {
            new android.app.AlertDialog.Builder(context)
                    .setTitle("Enable GPS Service")
                    .setMessage("We need your GPS location to show Near Places around you.")
                    .setCancelable(false)
                    .setPositiveButton("Enable", new
                            DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100,
                    5, (LocationListener) this);
            Log.i(TAG, "getLocation: success ");

        } catch (SecurityException e) {
            e.printStackTrace();
            Log.e(TAG, "getLocation: error " + e.getMessage());

        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0);
            String local = state = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            city = addresses.get(0).getSubAdminArea();
            country = addresses.get(0).getCountryName();
            latitude = addresses.get(0).getLatitude();
            longitude = addresses.get(0).getLongitude();

            binding.editFullAddress.setText(address);

        } catch (Exception e) {
            Log.e(TAG, "onLocationChanged: Error" + e.getLocalizedMessage());

        }
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Toast.makeText(context, "Gps is not enabled!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if (data != null) {
                String message = data.getStringExtra("MESSAGE");
                binding.editFullAddress.setText(message);
//                binding.buildingNoLayout.setVisibility(View.GONE);
//                binding.editTextDobLayout.setVisibility(View.GONE);
            } else {

            }
        }
    }

}