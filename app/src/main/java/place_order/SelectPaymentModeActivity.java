package place_order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.cashfree.pg.CFPaymentService;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivitySelectPaymentModeBinding;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modal.OrderItem;
import payment.PaymentActivity;
import retrofit.APIInterface;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class SelectPaymentModeActivity extends AppCompatActivity implements ApiResponseListener {
    ActivitySelectPaymentModeBinding binding;
    private static final String TAG = "SelectPaymentModeActivi";
    String selectedChekbox = "";
    StorageUtils storageUtils;
    String totalamount;
    int paymentId = 0, selectedDealerId;
    Context context;
    String placedORderDate, repeatDays, repeatDateTill = null, token,
            selectedAddressId, placedOrdertype;
    private ApiController apiController;
    ProgressDialog progressDialog;
    List<OrderItem> orderItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_payment_mode);

        context = SelectPaymentModeActivity.this;
        storageUtils = new StorageUtils();
        apiController = new ApiController(this);
        orderItemsList = new ArrayList<OrderItem>();
        orderItemsList = (List<OrderItem>) getIntent().getSerializableExtra("listitem");

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading.....");

        repeatDays = storageUtils.getDetails(SelectPaymentModeActivity.this, StringUtils.RepeatDays);
        repeatDateTill = storageUtils.getDetails(SelectPaymentModeActivity.this, StringUtils.RepeatDate);
        if (repeatDateTill.isEmpty()) {
            repeatDateTill = null;
        }
        placedORderDate = storageUtils.getDetails(SelectPaymentModeActivity.this, StringUtils.selectedDeliverydate);
        selectedAddressId = storageUtils.getDetails(SelectPaymentModeActivity.this, StringUtils.PlaceOrderAddress);
        token = storageUtils.getDetails(context, StringUtils.UserToken);
        totalamount = storageUtils.getDetails(context, StringUtils.Payment);
        placedOrdertype = storageUtils.getDetails(context, StringUtils.OrderType);
        selectedDealerId = Integer.parseInt(storageUtils.getDetails(context, StringUtils.DealerId));
//        Toast.makeText(context, "DealerId--" + selectedDealerId, Toast.LENGTH_SHORT).show();

        binding.codCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.onlineCheck.setChecked(false);
                    selectedChekbox = binding.codCheck.getText().toString().trim();
                }
            }
        });
        binding.onlineCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    binding.codCheck.setChecked(false);
                    selectedChekbox = binding.onlineCheck.getText().toString().trim();
                }
            }
        });

        binding.btnPLacedOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selectedChekbox.equals("")) {
                    ProgressDialog progressDialog2 = new ProgressDialog(SelectPaymentModeActivity.this);
                    progressDialog2.setCancelable(false);
                    progressDialog2.setTitle("Loading.....");
                    progressDialog2.show();
                    if (selectedChekbox.equals("Payment to Dealer")) {
                        placedCodOrder(progressDialog2);
                    } else {
                        placedOnlineOrder(progressDialog2);
                    }
                } else {
                    Toast.makeText(SelectPaymentModeActivity.this, "Please select the payment mode!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void placedCodOrder(ProgressDialog progressDialog) {
        try {
            Log.i(TAG, "placedCodOrder: called");
            apiController.placedOrder(totalamount, placedORderDate, selectedAddressId,
                    "Cash on delivery", repeatDays, repeatDateTill, token, orderItemsList,
                    paymentId, placedOrdertype, selectedDealerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void placedOnlineOrder(ProgressDialog progressDialog) {
        try {
            apiController.placedOnlineOrder(totalamount, placedORderDate, selectedAddressId,
                    selectedChekbox, repeatDays, repeatDateTill, token, orderItemsList,
                    paymentId, placedOrdertype, selectedDealerId, progressDialog);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String orderId, String orderToken) {
        progressDialog.dismiss();
//        if (orderToken.equals("OnlineOrder")) {
        startActivity(new Intent(SelectPaymentModeActivity.this,
                PaymentActivity.class).putExtra("orderId", orderId).putExtra("orderToken", orderToken));
//        } else {
//            Toast.makeText(context, "Order placed successfully", Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void onFailure(String msg) {
        progressDialog.dismiss();
        startActivity(new Intent(SelectPaymentModeActivity.this,
                PaymentActivity.class));
        Toast.makeText(context, "Order placed faliour", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        progressDialog.dismiss();
        Toast.makeText(context, "Order placed error occurred", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {

    }


}