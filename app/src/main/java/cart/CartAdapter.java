package cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.CartSectionFormatBinding;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import listener.CartQtyIncrementDecrement;
import listener.CartSectionTypeListener;
import modal.AllDealersModal;
import retrofit.ApiConstant;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    Context context;
    int lastposition = -1;
    int posi;
    float price = 0.0f;
    int produtSizeId;
    String typename;
    AllDealersModal allDealersModal;
    CartQtyIncrementDecrement cartQtyIncrementDecrement;
    private static final String TAG = "CartAdapter";
//    JSONArray Obj = new JSONArray();
//    JSONObject itemObject;

    private QuantityChangeCallback quantityChangeCallback;

    public CartAdapter(Context context, AllDealersModal allDealersModal, QuantityChangeCallback quantityChangeCallback) {
        this.context = context;
        this.allDealersModal = allDealersModal;
        this.cartQtyIncrementDecrement = cartQtyIncrementDecrement;
        this.quantityChangeCallback = quantityChangeCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CartSectionFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cart_section_format, parent, false));
        assert binding != null;
        CartAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvProductName.setText(allDealersModal.getProducts().get(position).getName());
//        Log.i(TAG, "onBindViewHolder: " + allDealersModal.getProducts().get(position).getIcon());
        String iconUrl = ApiConstant.BASE_URL2 + allDealersModal.getProducts().get(position).getIcon();
        Glide.with(context).load(iconUrl).into(holder.binding.imgProduct);

//        itemObject = new JSONObject();

        if (allDealersModal.getProducts().get(position).getType().size() == 0) {
            holder.binding.typeRecycler.setVisibility(View.GONE);
        } else {
            holder.binding.typeRecycler.setVisibility(View.VISIBLE);
        }

//        setting first type selected as default

        holder.binding.typeRecycler.setHasFixedSize(true);

        TypeAdapter typeAdapter = new TypeAdapter(context, allDealersModal.getProducts().get(position).getType(),
                new CartSectionTypeListener() {
                    @Override
                    public void getPrice(float productPrice, int sizeId, String typeName, float securityAmount) {
                        price = productPrice;
                        produtSizeId = sizeId;
                        typename = typeName;
                        int qty = Integer.parseInt(holder.binding.tvquantaty.getText().toString());
                        holder.binding.tvPrice.setText(Float.toString(qty * price));
                        holder.binding.tvItemId.setText(""+sizeId);
                        holder.binding.tvTypeName.setText(String.valueOf(typeName));
                        holder.binding.tvSecurityDeposit.setText((String.valueOf(securityAmount)));
                        quantityChangeCallback.onQuantityChange();
                    }
                });

        holder.binding.typeRecycler.setAdapter(typeAdapter);

        holder.binding.qtyPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.binding.tvquantaty.getText().toString());
                qty = qty + 1;
                if (qty > 100) {
                    Toast.makeText(context, "Maximum quantity of the product is 10!", Toast.LENGTH_SHORT).show();
                } else {
                    price = allDealersModal.getProducts().get(position)
                            .getType().get(typeAdapter.selectedPosition).getPrice();

                    produtSizeId = allDealersModal.getProducts().get(position)
                            .getType().get(typeAdapter.selectedPosition).getPrice_id();
//                    Toast.makeText(context, "produtSizeId--" + produtSizeId, Toast.LENGTH_SHORT).show();

                    typename = allDealersModal.getProducts().get(position)
                            .getType().get(typeAdapter.selectedPosition).getName();

                    Log.i(TAG, "onClick: " + typeAdapter.selectedPosition);
                    holder.binding.tvPrice.setText(Float.toString(qty * price));

                    holder.binding.tvItemId.setText(String.valueOf(produtSizeId));
                    holder.binding.tvTypeName.setText(typename);
                    holder.binding.tvSecurityDeposit.setText(String.valueOf(allDealersModal.getProducts().get(position).getType().get(typeAdapter.selectedPosition).getSecurity_deposit()));

                    quantityChangeCallback.onQuantityChange();
                    holder.binding.tvquantaty.setText(Integer.toString(qty));
                }
            }
        });
        holder.binding.qtyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.binding.tvquantaty.getText().toString());
                qty = qty - 1;
                if (qty < 0) {
                    Toast.makeText(context, "Minimum quality of the product is 0!", Toast.LENGTH_SHORT).show();
                } else {
                    price = allDealersModal.getProducts().get(position)
                            .getType().get(typeAdapter.selectedPosition).getPrice();

                    holder.binding.tvPrice.setText(Float.toString(qty * price));
                    quantityChangeCallback.onQuantityChange();
                    holder.binding.tvquantaty.setText(Integer.toString(qty));

                    holder.binding.tvItemId.setText(String.valueOf(produtSizeId));
                    holder.binding.tvTypeName.setText(typename);
                    holder.binding.tvSecurityDeposit.setText(String.valueOf(allDealersModal.getProducts().get(position).getType().get(typeAdapter.selectedPosition).getSecurity_deposit()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allDealersModal.getProducts().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CartSectionFormatBinding binding;

        public ViewHolder(@NonNull CartSectionFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    interface QuantityChangeCallback {
        void onQuantityChange();
    }
}
