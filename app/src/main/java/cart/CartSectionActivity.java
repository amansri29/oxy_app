package cart;

import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.spacetexhdemo.R;
import com.spacetexhdemo.activity.BaseActivity;
import com.spacetexhdemo.databinding.ActivityCartSectionBinding;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modal.AllDealersModal;
import modal.OrderItem;
import modal.SelectedProductDetails;
import place_order.PlaceOrderScheduleActivity;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class CartSectionActivity extends BaseActivity {
    ActivityCartSectionBinding binding;
    CartAdapter cartAdapter;
    Context context;
    int quantity = 1, finalPrice = 0;
    StorageUtils storageUtils;
    private APIInterface apiInterface;
    List<OrderItem> orderItemsList;
    List<SelectedProductDetails> selectedProductDetailsList;
    ArrayList<AllDealersModal> arrayList;
    String token;
    private static final String TAG = "CartSectionActivity";
    AllDealersModal allDealersModals;
    OrderItem orderItem;
    SelectedProductDetails selectedProductDetails;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cart_section);
        context = CartSectionActivity.this;

        orderItemsList = new ArrayList<>();
        selectedProductDetailsList = new ArrayList<SelectedProductDetails>();

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        arrayList = new ArrayList<>();
        storageUtils = new StorageUtils();
        token = storageUtils.getDetails(context, StringUtils.UserToken);
        dealerId = storageUtils.getDetails(context, StringUtils.DealerId);

        getDealersProducts(token, progressDialog, dealerId);

        binding.imgBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fAmount = binding.tvFinalAmount.getText().toString();

                if (fAmount.isEmpty()) {
                    Toast.makeText(context, "Please select the quantity of the product!", Toast.LENGTH_SHORT).show();
                } else if (fAmount.equals("0.0")) {
                    Toast.makeText(context, "Please select the quantity of the product!", Toast.LENGTH_SHORT).show();
                } else {

                    storageUtils.setDetails(context, StringUtils.Payment, fAmount);

//                    if user back from next screen than without bellow these two line, item will added twice
                    orderItemsList.clear();
                    selectedProductDetailsList.clear();

                    for (int i = 0; allDealersModals.getProducts().size() > i; i++) {

                        View view1 = binding.cartItemRecycler.getLayoutManager().findViewByPosition(i);

                        TextView quantity = view1.findViewById(R.id.tvquantaty);
                        TextView itemId = view1.findViewById(R.id.tvItemId);

                        TextView itemPrice = view1.findViewById(R.id.tvPrice);

                        TextView itemName = view1.findViewById(R.id.tvTypeName);

                        TextView securityAmount = view1.findViewById(R.id.tv_security_deposit);

                        TextView productName = view1.findViewById(R.id.tvProductName);





                        int qty = Integer.parseInt(quantity.getText().toString());
                        if (qty != 0) {
                            Log.d(TAG, "onClick: " + quantity.getText().toString()
                                    + "--" + itemId.getText().toString());

                            orderItem = new OrderItem(
                                    Integer.parseInt(itemId.getText().toString()),
                                    Integer.parseInt(quantity.getText().toString()));

                            String itemprice = itemPrice.getText().toString();
                            String deposit = securityAmount.getText().toString();

                            Log.i(TAG, "onClick: item type " + itemName.getText().toString());
                            selectedProductDetails = new SelectedProductDetails(productName.getText().toString()
                                    ,itemName.getText().toString(),
                                    itemprice,
                                    Integer.parseInt(quantity.getText().toString()), Float.parseFloat(deposit));


                            orderItemsList.add(orderItem);
                            selectedProductDetailsList.add(selectedProductDetails);


                        } else {

                        }
                    }
                    Log.d(TAG, "onClick: " + orderItemsList);


                    startActivity(new Intent(context, PlaceOrderScheduleActivity.class)
                            .putExtra("listitem22", (Serializable) orderItemsList)
                            .putExtra("selectedProductDetails", (Serializable) selectedProductDetailsList));
                }
            }
        });
    }

    public void getDealersProducts(String token, ProgressDialog progressDialog, String userDetailsId) {

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<AllDealersModal> call = apiInterface.getAllDealersProduct(Token, userDetailsId);
        call.enqueue(new Callback<AllDealersModal>() {
            @Override
            public void onResponse(@NotNull Call<AllDealersModal> call,
                                   @NotNull Response<AllDealersModal> response) {
                progressDialog.dismiss();

                allDealersModals = response.body();

                if (allDealersModals != null) {
                    binding.cartItemRecycler.setHasFixedSize(true);
//                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CartSectionActivity.this);
//                    binding.cartItemRecycler.setLayoutManager(linearLayoutManager);
                    cartAdapter = new CartAdapter(context, allDealersModals,
                            new CartAdapter.QuantityChangeCallback() {
                                @Override
                                public void onQuantityChange() {
                                    Log.i(TAG, "onQuantityChange: called" + allDealersModals.getProducts().size());
                                    double totalPrice = 0.0;
                                    for (int i = 0; allDealersModals.getProducts().size() > i; i++) {
                                        View view = binding.cartItemRecycler.getLayoutManager().findViewByPosition(i);
                                        TextView price = view.findViewById(R.id.tvPrice);
                                        totalPrice = totalPrice + Double.parseDouble(price.getText().toString());
                                    }
                                    binding.tvFinalAmount.setText(Double.toString(totalPrice));
                                }
                            });
                    binding.cartItemRecycler.setAdapter(cartAdapter);
                    double price = (double) finalPrice;
                    binding.tvFinalAmount.setText(String.valueOf(price));

                } else {
                    Toast.makeText(CartSectionActivity.this, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllDealersModal> call, @NotNull Throwable t) {
//                apiResponseListener.onError(t.getMessage());
                Log.e("Faliour", t.getMessage());
                Toast.makeText(CartSectionActivity.this, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}