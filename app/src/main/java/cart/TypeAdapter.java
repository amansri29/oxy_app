package cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.CartSectionFormatBinding;
import com.spacetexhdemo.databinding.CartTypeFormatBinding;

import java.util.List;

import listener.CartQtyIncrementDecrement;
import listener.CartSectionTypeListener;
import listener.SelectSizeListener;
import modal.AllDealersModal;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.ViewHolder> {
    Context context;
    int posi;
    private static final String TAG = "TypeAdapter";

    List<AllDealersModal.ProductsBean.TypeBean> typeBeanList;
    CartSectionTypeListener cartSectionTypeListener;
    SelectSizeListener selectSizeListener;
    public int selectedPosition;



    public TypeAdapter(Context context, List<AllDealersModal.ProductsBean.TypeBean> allDealersModal,
                       CartSectionTypeListener cartQtyIncrementDecrement) {
        this.context = context;
        this.typeBeanList = allDealersModal;
        this.cartSectionTypeListener = cartQtyIncrementDecrement;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CartTypeFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cart_type_format, parent, false));
        assert binding != null;
        TypeAdapter.ViewHolder viewHolder = new TypeAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.binding.tvType.setText(String.valueOf(typeBeanList.get(position).getName()));

//        cartSectionTypeListener.getPrice(typeBeanList.get(position).getPrice(),
//                typeBeanList.get(position).getPrice_id());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = position;
                Log.i(TAG, "onClick: " + position);
                cartSectionTypeListener.getPrice(typeBeanList.get(position).getPrice(),
                        typeBeanList.get(position).getPrice_id(),
                        typeBeanList.get(position).getName(), typeBeanList.get(position).getSecurity_deposit());
                notifyDataSetChanged();
            }
        });

        if (selectedPosition == position) {
            holder.binding.sizeLayout.setBackground(context.getResources().getDrawable(R.drawable.selected_size_background));
            holder.binding.tvType.setTextColor(ContextCompat.getColor(context, R.color.color3B6EFF));
        } else {
            holder.binding.sizeLayout.setBackground(context.getResources().getDrawable(R.drawable.size_background));
            holder.binding.tvType.setTextColor(ContextCompat.getColor(context, R.color.colorC7CDD1));
        }
    }

//    public int grandTotal() {
//        int totalPrice = 0;
//        for (int i = 0; i < typeBeanList.size(); i++) {
//            totalPrice += typeBeanList.get(i).getPrice();
////           totalPrice = totalPrice
//        }
//        return totalPrice;
//    }

    @Override
    public int getItemCount() {
        return typeBeanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CartTypeFormatBinding binding;

        public ViewHolder(@NonNull CartTypeFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
