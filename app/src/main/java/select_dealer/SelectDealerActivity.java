package select_dealer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;
import com.spacetexhdemo.activity.BaseActivity;
import com.spacetexhdemo.databinding.ActivitySelectCylilinderBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import listener.SelectCylinderInterface;
import modal.AllDealersModal;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class SelectDealerActivity extends BaseActivity implements ApiResponseListener {
    ActivitySelectCylilinderBinding binding;
    private Context context;
    CylinderDelaerAdapter cylinderDelaerAdapter;
    ArrayList<AllDealersModal> arrayList;
    private ApiController apiController;
    String token, userId, selectedCompanyName;
    StorageUtils storageUtils;
    int dealerId = 0;
    private APIInterface apiInterface;
    private double lat, lon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_cylilinder);
        context = SelectDealerActivity.this;
        arrayList = new ArrayList<>();
        apiController = new ApiController(this);
        storageUtils = new StorageUtils();
        token = storageUtils.getDetails(context, StringUtils.UserToken);
        userId = storageUtils.getDetails(context, StringUtils.UserId);
        try {
            lat = Double.parseDouble(storageUtils.getDetails(context, StringUtils.myAddressLat));
            lon = Double.parseDouble(storageUtils.getDetails(context, StringUtils.myAddressLong));
        }catch (Exception e){

        }

//
//        Toast.makeText(context, "userId--" + userId, Toast.LENGTH_SHORT).show();
//        Toast.makeText(context, "token--" + token, Toast.LENGTH_SHORT).show();

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String headerToken = "Token " + token;
        getAllDealers(headerToken, progressDialog);

        binding.saveDealter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Loading.....");
                progressDialog.setCancelable(false);
                progressDialog.show();

                if (dealerId != 0) {
                    apiController.updateCylinderDealer(dealerId, userId, token);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 2000);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Please select the dealer!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.editSearchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                binding.imgClear.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                cylinderDelaerAdapter.filter(charSequence.toString());
//                progressDialog.dismiss();
                if (binding.editSearchItem.getText().length() > 0) {
//                    binding.imgClear.setVisibility(View.VISIBLE);
                } else {
//                    binding.imgClear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        binding.tvNote.setText(Html.fromHtml("<b>Note:</b>  You will not be able to change the dealer after making the payment. Same dealer will be used everytime for further transaction"));
        setSpanString("Note:",
                " You will not be able to change the dealer after making the payment." +
                        " Same dealer will be used everytime for further transaction",
                binding.tvNote);
    }

    private void setSpanString(String string1, String string2, TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString txtSpannable = new SpannableString(string1);
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        txtSpannable.setSpan(boldSpan, 0, string1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(txtSpannable);
        builder.append(string2);
        textView.setText(builder, TextView.BufferType.SPANNABLE);
    }

    public void getAllDealers(String token, ProgressDialog progressDialog) {

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        Call<List<AllDealersModal>> call = apiInterface.getAllDealers(token);
        call.enqueue(new Callback<List<AllDealersModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllDealersModal>> call,
                                   @NotNull Response<List<AllDealersModal>> response) {
                progressDialog.dismiss();
                List<AllDealersModal> detailsModal = response.body();
                for(AllDealersModal dealer : detailsModal){
                    float[] distance = new float[3];
                    Location.distanceBetween(dealer.getDealer_address().getLatitude(), dealer.getDealer_address().getLongitude(),lat,lon, distance);
                    double distanceInKms = distance[0]/1000;
                    dealer.setDistance(distanceInKms);
                    Log.d("Select Dealer", "onResponse: "+distance[0]+", "+distance[1]+", "+ distance[2]);
                }
                Collections.sort(detailsModal, (o1, o2) -> (int)(o1.getDistance() - o2.getDistance()));

                if (detailsModal != null) {
//                    Toast.makeText(SelectCylilinderActivity.this, "modal--" + detailsModal
//                            , Toast.LENGTH_SHORT).show();
//                    arrayList.add(detailsModal);
                    binding.cyclernderRecycler.setHasFixedSize(true);
                    cylinderDelaerAdapter = new CylinderDelaerAdapter(context, detailsModal, new SelectCylinderInterface() {
                        @Override
                        public void selectCylinder(int position) {
                            dealerId = detailsModal.get(position).getId();
                            selectedCompanyName = detailsModal.get(position).getCompany_name();
                            Log.d("SelectDealer", "selectCylinder: "+dealerId);
                            storageUtils.setDetails(context, StringUtils.DealerId, String.valueOf(dealerId));
                            storageUtils.setDetails(context, StringUtils.SelectedDealerCompanyName, selectedCompanyName);
                            saveDealerID(Integer.toString(dealerId));
//                            Toast.makeText(SelectCylilinderActivity.this, "uid--" + uid, Toast.LENGTH_SHORT).show();
                        }
                    });
                    binding.cyclernderRecycler.setAdapter(cylinderDelaerAdapter);
                } else {
                    Toast.makeText(SelectDealerActivity.this, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(SelectCylilinderActivity.this, "modal name--" + detailsModal
//                        , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NotNull Call<List<AllDealersModal>> call, @NotNull Throwable t) {
//                apiResponseListener.onError(t.getMessage());
                Toast.makeText(SelectDealerActivity.this, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSuccess(String tag, String superClassCastBean) {
//        Toast.makeText(context, "Update-----", Toast.LENGTH_SHORT).show();
        storageUtils.setDetails(context, StringUtils.SelectCylinder, StringUtils.Yes);
        startActivity(new Intent(SelectDealerActivity.this, MainActivity.class));
    }

    @Override
    public void onFailure(String onFailure) {
        Toast.makeText(context, "onFailure-----" + onFailure, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onError(String msg) {
        Toast.makeText(context, "onError-----" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {

    }
}