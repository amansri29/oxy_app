package select_dealer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.RecyclerviewListBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import listener.SelectCylinderInterface;
import modal.AllDealersModal;
import utils.StringUtils;

public class CylinderDelaerAdapter extends RecyclerView.Adapter<CylinderDelaerAdapter.ViewHolder> {
    Context context;
    int lastposition = -1;
    int posi;
    int count = 1;
    List<AllDealersModal> arrayList;
    public List<AllDealersModal> filteredTicketList;
    SelectCylinderInterface selectCylinderInterface;


    public CylinderDelaerAdapter(Context context, List<AllDealersModal> arrayList
            , SelectCylinderInterface selectCylinderInterface) {
        this.context = context;
        this.arrayList = arrayList;
        this.filteredTicketList = new ArrayList<AllDealersModal>();
        this.filteredTicketList.addAll(arrayList);
        this.selectCylinderInterface = selectCylinderInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerviewListBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.recyclerview_list, parent, false));
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvName.setText(arrayList.get(position).getCompany_name());
        holder.binding.tvAddress.setText(String.valueOf(arrayList.get(position).getDealer_address().getBasic_address()));
//        holder.binding.tvCylinderPrice.setText(arrayList.get(position).getDealer_address().getPincode() + " / cylinder");
        DecimalFormat format = new DecimalFormat("#.##");
        holder.binding.tvNearBy.setText(format.format(arrayList.get(position).getDistance()) + " Km");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count--;
                posi = position;
                notifyDataSetChanged();
            }
        });

        if (count == 1) {
            holder.binding.ivSelected.setVisibility(View.GONE);
        } else {
            if (posi == position) {
                holder.binding.ivSelected.setVisibility(View.VISIBLE);
                selectCylinderInterface.selectCylinder(position);
            } else {
                holder.binding.ivSelected.setVisibility(View.GONE);
            }
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        arrayList.clear();
        if (charText.length() == 0) {
            arrayList.addAll(filteredTicketList);
        } else {
            for (AllDealersModal wp : filteredTicketList) {
                if (wp.getCompany_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    arrayList.add(wp);
                } else {

                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerviewListBinding binding;

        public ViewHolder(@NonNull RecyclerviewListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
