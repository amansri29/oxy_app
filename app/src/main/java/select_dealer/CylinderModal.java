package select_dealer;

public class CylinderModal {
    private String id;
    private String name;
    private String address;
    private String price;
    private String distance;

    public CylinderModal(String id, String name, String address, String price, String distance) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.price = price;
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
