package payment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cashfree.pg.CFPaymentService;
import com.spacetexhdemo.MainActivity;
import com.spacetexhdemo.R;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modal.OrderItem;
import modal.PaymentResponceTokenModal;
import okhttp3.Response;
import retrofit.APIInterface;
import retrofit.ApiController;
import retrofit.ApiResponseListener;
import retrofit.PaymentTokenApiContoller;
import retrofit.PaymentTokenResponceListener;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class PaymentActivity extends AppCompatActivity implements ApiResponseListener {
    String appId = "1251957c03a0f4c3603c4b2ca0591521";

    String token, orderId, userPlacedOrderId, userToken, amount;
    Context context;
    int refrenceId = 0;
    private static final String TAG = "PaymentActivity";
    StorageUtils storageUtils;
    ApiController orderPaymentIdUpdateController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        context = PaymentActivity.this;
        storageUtils = new StorageUtils();

        userPlacedOrderId = getIntent().getStringExtra("orderId");
        token = getIntent().getStringExtra("orderToken");
        Toast.makeText(context, "token--" + token, Toast.LENGTH_SHORT).show();
        Toast.makeText(context, "userPlacedOrderId--" + userPlacedOrderId, Toast.LENGTH_SHORT).show();
        userToken = storageUtils.getDetails(context, StringUtils.UserToken);
        amount = storageUtils.getDetails(context, StringUtils.Payment);
        orderPaymentIdUpdateController = new ApiController(this);

        getInputParams();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "API Response : ");
        Log.d("responce", "API Response : " + data.getStringExtra("id"));
        if (requestCode == CFPaymentService.REQ_CODE && data != null) {
            Bundle bundle = data.getExtras();
            String responce = data.getStringExtra("id");
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    if (bundle.getString(key) != null) {
                        Log.d("TAG", key + " : " + bundle.getString(key));
                        Log.d("TAG", bundle.getString("referenceId"));
                        Log.d("TAG", bundle.getString("txStatus"));
                        Log.d("TAG", bundle.getString("orderId"));
                        refrenceId = Integer.parseInt(bundle.getString("referenceId"));
                    } else {
                        Toast.makeText(this, "Payment failed!", Toast.LENGTH_LONG).show();
                    }
                }
                Toast.makeText(this, "Your Payment Sucessfully!", Toast.LENGTH_LONG).show();
                updatePaymentId();
            } else {

            }

        }
    }

    public void onClick(View view) {
        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(0);

        switch (view.getId()) {
            case R.id.payOnline: {
                getInputParams();
                break;
            }
        }
    }

    private void getInputParams() {
        Log.i(TAG, "getInputParams: " + orderId + "  " + amount);
        HashMap<String, String> params = new HashMap<>();
        params.put(CFPaymentService.PARAM_APP_ID, StringUtils.AppId);
        params.put(CFPaymentService.PARAM_ORDER_ID, userPlacedOrderId);
        params.put(CFPaymentService.PARAM_ORDER_AMOUNT, amount);
        params.put(CFPaymentService.PARAM_ORDER_CURRENCY, "INR");

//        todo add these parameters from backend
//        params.put(CFPaymentService.PARAM_CUSTOMER_NAME, "Aman Srivastav");

        params.put(CFPaymentService.PARAM_CUSTOMER_EMAIL, "dcc9dcc@gmail.com");
        params.put(CFPaymentService.PARAM_CUSTOMER_PHONE, "8619368171");

//        params.put(CFPaymentService.PARAM_NOTIFY_URL, "https://www.trucksampark.com");

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();

        Log.i(TAG, "getInputParams: token " + token);


        cfPaymentService.doPayment(this, params, token, "PROD", "#0000FF", "#000000");

    }



    @Override
    public void onSuccess(String tag, String superClassCastBean) {
        startActivity(new Intent(PaymentActivity.this, MainActivity.class));
        Toast.makeText(context, "Order payment id update successfully!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String msg) {
        Log.e(TAG, "onFailure: Payment " + msg );
        Toast.makeText(this, "faliour token" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        Log.e(TAG, "onError: Payment " + msg );
        Toast.makeText(this, "error token" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {
        Log.e(TAG, "onPerformcode: Payment " + code);

    }

    @Override
    protected void onStart() {
        super.onStart();

//        apiController.getPaymentToken(appId, secreatKey, orderId, amount, "INR");
    }

    public void updatePaymentId() {
        try {
            orderPaymentIdUpdateController.updateOrderPaymentId(refrenceId, userToken, Integer.parseInt(userPlacedOrderId));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}