package payment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.cashfree.pg.CFPaymentService;
import com.google.gson.Gson;
import com.spacetexhdemo.R;
import com.spacetexhdemo.databinding.ActivityRetryPaymentBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import adapters.DeliveryDateAdapter;
import adapters.SelectedProductAdapter;
import listener.GetSelectedDeliveryDateListener;
import modal.DeliveryDateModal;
import modal.Order;
import modal.OrderItem;
import modal.SelectedProductDetails;
import modal.UpdateOrder;
import retrofit.APIClientMain;
import retrofit.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class RetryPaymentActivity extends AppCompatActivity {
    private ActivityRetryPaymentBinding binding;
    private StorageUtils storageUtils;
    private ProgressDialog progressDialog;
    private String token = "Token ";
    private String orderID = "";
    private List<SelectedProductDetails> selectedProductList;
    private SelectedProductAdapter productAdapter;
    private float totalAmountWithoutGST;
    private String paymentMode = "Online", selectedDeliverydate;
    private String totalAmount = "";
    DeliveryDateAdapter deliveryDateAdapter;
    ArrayList<DeliveryDateModal> arrayList = new ArrayList<>();
    private String paymentToken;
    private String status;
    private int refrenceId;
    private static final int GST_PERCENTAGE = 5;
    private static final int DELIVERY_CHARGE = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_retry_payment);
        storageUtils = new StorageUtils();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setCancelable(false);
        selectedProductList = new ArrayList<>();
        token = token + storageUtils.getDetails(this, StringUtils.UserToken);
        orderID = getIntent().getStringExtra("orderID");
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        SimpleDateFormat sdfullFormat = new SimpleDateFormat("yyyy-MM-dd ");
        for (int i = 1; i < 8; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            String fulldate = sdfullFormat.format(calendar.getTime());
            arrayList.add(new DeliveryDateModal(day, fulldate));
        }

        binding.deliveryDateRecycler.setHasFixedSize(true);
        deliveryDateAdapter = new DeliveryDateAdapter(RetryPaymentActivity.this, arrayList, new GetSelectedDeliveryDateListener() {
            @Override
            public void getDeliveryDate(int position) {
                selectedDeliverydate = arrayList.get(position).getDeliveryFulldateName();
//                Toast.makeText(context, "DeliveryDate----" + selectedDeliverydate, Toast.LENGTH_SHORT).show();
                //storageUtils.setDetails(RetryPaymentActivity.this, StringUtils.DeliveryDate, selectedDeliverydate.trim());
            }
        });
        binding.deliveryDateRecycler.setAdapter(deliveryDateAdapter);


        getOrderData();

        binding.btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String btnText = binding.btnProceedToPay.getText().toString();
                if (btnText.equalsIgnoreCase("Place Order")) {
                    if (!progressDialog.isShowing()) {
                        progressDialog.show();
                        updatePaymentAsCod();
                    }
                } else {
                    doPayment();
                }
            }
        });

        binding.imgBAck.setOnClickListener(v->{
            onBackPressed();
        });
    }

    private void updatePaymentAsPrepaid(){
        if(!progressDialog.isShowing()){
            progressDialog.show();
        }
        UpdateOrder updateOrder = new UpdateOrder();
        updateOrder.setPayment_id(String.valueOf(refrenceId));
        updateOrder.setExpected_delivery_date(selectedDeliverydate.trim());
        updateOrder.setPayment_mode(paymentMode);
        if(status.equals("FAILED")){
            updateOrder.setStatus("Payment Pending");
        }
        else{
            updateOrder.setStatus("Confirmed");
        }
        Call<Order> call = APIClientMain.getClient().create(APIInterface.class).updateOrder(token,orderID,updateOrder);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code()== 200 && response.isSuccessful()){
                    Toast.makeText(RetryPaymentActivity.this,"Payment Successful", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void updatePaymentAsCod() {
        UpdateOrder updateOrder = new UpdateOrder();
        updateOrder.setPayment_id("");
        updateOrder.setExpected_delivery_date(selectedDeliverydate.trim());
        updateOrder.setPayment_mode(paymentMode);
        updateOrder.setStatus("Confirmed");
        Call<Order> call = APIClientMain.getClient().create(APIInterface.class).updateOrder(token,orderID,updateOrder);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code()== 200 && response.isSuccessful()){
                    Toast.makeText(RetryPaymentActivity.this,"Payment Successful", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getOrderData() {
        progressDialog.show();
        Call<Order> call = APIClientMain.getClient().create(APIInterface.class).getOrderDetail(token, orderID);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Order order = response.body();
                    Log.d("RetryPayment", "onResponse: " + new Gson().toJson(response.body()));
                    binding.tvOrderType.setText(order.getOrder_type());
                    binding.tvSelectedOrderDate.setText(order.getExpected_delivery_date());
                    totalAmount = String.valueOf(order.getTotal_amount());
                    binding.tvTotal.setText(String.valueOf(order.getTotal_amount()));
                    selectedProductList.clear();
                    totalAmountWithoutGST = 0.0f;
                    for (OrderItem orderItem : order.getItems()) {
                        totalAmountWithoutGST = totalAmountWithoutGST + (Float.parseFloat(orderItem.getItemPrice()) * orderItem.getQuantity());
                        selectedProductList.add(new SelectedProductDetails(orderItem.getName(), orderItem.getItemType(), orderItem.getItemPrice(), orderItem.getQuantity(), orderItem.getSecurity_deposit()));
                    }
                    int gst_amount = (int) (totalAmountWithoutGST * 5) / 100;
                    binding.tvGstAmount.setText(String.valueOf(gst_amount));
                    float totalSecurityDeposit = 0.0f;
                    if (order.getOrder_type().equalsIgnoreCase("new")) {
                        for (SelectedProductDetails selectedProductDetails : selectedProductList) {
                            if (selectedProductDetails.getSecurityAmount() > 0) {
                                Float securityAmount = selectedProductDetails.getSecurityAmount() * selectedProductDetails.getQuantity();
                                totalSecurityDeposit = securityAmount + totalSecurityDeposit;
                                SelectedProductDetails selectedProductDetails1 = new SelectedProductDetails("Security Amount " + selectedProductDetails.getProductName(),
                                        selectedProductDetails.getProductType(), Float.toString(securityAmount), selectedProductDetails.getQuantity(), 0.0f);
                                selectedProductList.add(selectedProductDetails1);
                                break;
                            }

                        }
                    }
                    productAdapter = new SelectedProductAdapter(RetryPaymentActivity.this, selectedProductList);
                    binding.selectedRecycler.setAdapter(productAdapter);
                    binding.tvDealerCompnayName.setText(order.getDealer_name());
                    binding.tvTotalWithoutGST.setText(String.valueOf(totalAmountWithoutGST));
                    paymentMode = order.getPayment_mode();
                    if (paymentMode.equals("Online")) {
                        binding.selectPaymentModeLayout.check(R.id.onlineCheck);
                        binding.btnProceedToPay.setText("Proceed to Pay ₹" + totalAmount);
                    } else {
                        binding.selectPaymentModeLayout.check(R.id.codCheck);
                        binding.btnProceedToPay.setText("Place Order");
                    }

                    binding.selectPaymentModeLayout.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.onlineCheck) {
                                paymentMode = binding.onlineCheck.getText().toString();
                                binding.btnProceedToPay.setText("Proceed to Pay ₹" + totalAmount);
                            } else if (checkedId == R.id.codCheck) {
                                paymentMode = binding.codCheck.getText().toString();
                                binding.btnProceedToPay.setText("Place Order");
                            }
                        }
                    });
                    int pos = 0;
                    boolean date_matched = false;
                    for (DeliveryDateModal dateModal : arrayList) {
                        if (dateModal.getDeliveryFulldateName().trim().equalsIgnoreCase(order.getExpected_delivery_date())) {
                            deliveryDateAdapter.setPosi(pos);
                            selectedDeliverydate = dateModal.getDeliveryFulldateName();
                            deliveryDateAdapter.notifyDataSetChanged();
                            date_matched = true;
                            break;
                        }
                        pos++;
                    }
                    if (!date_matched) {
                        deliveryDateAdapter.setPosi(0);
                        selectedDeliverydate = arrayList.get(0).getDeliveryFulldateName().trim();
                        deliveryDateAdapter.notifyDataSetChanged();
                    }
                    paymentToken = order.getPayment_token();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void doPayment() {
        HashMap<String, String> params = new HashMap<>();
        params.put(CFPaymentService.PARAM_APP_ID, StringUtils.AppId);
        params.put(CFPaymentService.PARAM_ORDER_ID, orderID);
        params.put(CFPaymentService.PARAM_ORDER_AMOUNT, String.valueOf(totalAmount));


//        params.put(CFPaymentService.PARAM_CUSTOMER_NAME, "Aman Srivastav");
        params.put(CFPaymentService.PARAM_CUSTOMER_EMAIL, "dcc9dcc@gmail.com");
        params.put(CFPaymentService.PARAM_CUSTOMER_PHONE, "8619368171");


//        params.put(CFPaymentService.PARAM_NOTIFY_URL, "https://www.trucksampark.com");
        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.doPayment(this, params, paymentToken, "PROD", "#3B6EFF", "#ffffff");
        //cfPaymentService.doPayment(this, params, paymentToken, "TEST", "#3B6EFF", "#ffffff");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CFPaymentService.REQ_CODE && data != null) {
            try {
                Bundle bundle = data.getExtras();
                String response = data.getStringExtra("id");
                if (bundle != null) {
                    status = bundle.getString("txStatus");
                    refrenceId = Integer.parseInt(bundle.getString("referenceId"));
                    updatePaymentAsPrepaid();
                } else {
                    Toast.makeText(this, "Payment failed!", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
            }
        } else {
            Toast.makeText(this, "Payment failed!", Toast.LENGTH_LONG).show();
        }
    }
}