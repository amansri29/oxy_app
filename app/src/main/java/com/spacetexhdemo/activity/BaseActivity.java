package com.spacetexhdemo.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    protected SharedPreferences sharedPreferences;
    protected SharedPreferences.Editor editor;
    protected String dealerId, mobileNo, customerID;
    String DEALER_KEY = "Dealer ID";
    String MOBILE_KEY = "Mobile No";
    String CUSTOMER_KEY = "Customer ID";


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();
        dealerId = sharedPreferences.getString(DEALER_KEY, "-1");
        mobileNo = sharedPreferences.getString(MOBILE_KEY, "1234567890");
        customerID = sharedPreferences.getString(CUSTOMER_KEY, "-1");

    }

    protected void saveDealerID(String dealerId) {
        editor.putString(DEALER_KEY, dealerId);
        editor.commit();
    }

    protected void saveMobileNumber(String mobile) {
        editor.putString(MOBILE_KEY, mobile);
        editor.commit();
    }

    protected void saveCustomerID(String id) {
        editor.putString(CUSTOMER_KEY, id);
        editor.commit();
    }
}
