package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.spacetexhdemo.databinding.ActivityAadharCardUploadBinding;

import retrofit.ApiController;
import retrofit.ApiResponseListener;
import sharedprefrence.StorageUtils;
import utils.StringUtils;
import verify_account.MedicalReport;
import verify_account.VerifyAccountActivity;

public class AadharCardUploadActivity extends AppCompatActivity implements ApiResponseListener {
    ActivityAadharCardUploadBinding binding;
    String userDetailsId, token;
    int count = 0;
    private ApiController apiController;
    StorageUtils storageUtils;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_aadhar_card_upload);
        context = AadharCardUploadActivity.this;
        userDetailsId = getIntent().getStringExtra("userDetailsId");
//        Toast.makeText(this, "userDetailsId--" + userDetailsId, Toast.LENGTH_SHORT).show();
        storageUtils = new StorageUtils();
        apiController = new ApiController(this);

        token = storageUtils.getDetails(context, StringUtils.UserToken);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String aadharNumber = binding.editAadharCardNumber.getText().toString();
                if (aadharNumber.isEmpty()) {
                    Toast.makeText(AadharCardUploadActivity.this, "Please enter the aadhar card number!", Toast.LENGTH_SHORT).show();
                } else {
//                    binding.tvOtpLayout.setVisibility(View.VISIBLE);
//                    binding.editTextOtpLayout.setVisibility(View.VISIBLE);
//                    String opt = binding.editAadharCardOtp.getText().toString();
//                    if (opt.isEmpty()) {
//                        Toast.makeText(AadharCardUploadActivity.this, "Please enter the otp", Toast.LENGTH_SHORT).show();
//                    } else {
                    apiController.updateAadharCardNumber(aadharNumber, userDetailsId, token);
//                    }
                }
            }
        });

        binding.editAadharCardNumber.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Remove spacing char
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                // Insert char where needed.
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                        s.insert(s.length() - 1, String.valueOf(space));
                    }
                }
            }
        });
    }

    @Override
    public void onSuccess(String tag, String superClassCastBean) {
//        binding.editTextOtpLayout.setVisibility(View.VISIBLE);
        storageUtils.setDetails(context, StringUtils.AadharCardUpload, StringUtils.Yes);
        startActivity(new Intent(AadharCardUploadActivity.this, MedicalReport.class));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, "onFailure--" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, "onError--" + msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPerformCode(int code) {

    }
}