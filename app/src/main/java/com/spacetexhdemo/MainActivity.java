package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.spacetexhdemo.databinding.ActivityMainBinding;

import fragments.AddressFragment;
import fragments.DealersFragment;
import fragments.HomeFragment;
import fragments.ConfirmdFragment;
import fragments.LedgerFragment;
import fragments.ProfileFragment;
import sharedprefrence.StorageUtils;
import utils.StringUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityMainBinding binding;
    Context context;
    StorageUtils storageUtils;
    String userName, userDob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        context = MainActivity.this;
        storageUtils = new StorageUtils();

        binding.imgNavigation.setOnClickListener(this);
        binding.navLayout.navHome.setOnClickListener(this);
        binding.navLayout.tvDealernav.setOnClickListener(this);
        binding.navLayout.navMyAddressesnav.setOnClickListener(this);
        binding.navLayout.navMyAddressesnav.setOnClickListener(this);
        binding.navLayout.Ledgernav.setOnClickListener(this);
        binding.navLayout.imgClose.setOnClickListener(this);
        binding.navLayout.tvEditProfile.setOnClickListener(this);
        binding.navLayout.navLogout.setOnClickListener(this);

        binding.contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactUs.class);
                startActivity(intent);
            }
        });
        loadFragment(new HomeFragment());
//        loadFragment(new ConfirmdFragment());

        userName = storageUtils.getDetails(this, StringUtils.UserName);
        userDob = storageUtils.getDetails(this, StringUtils.UserDob);

        binding.navLayout.tvUserName.setText(userName);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgNavigation:
                isDrawerOpen();
                break;
            case R.id.navHome:
                isDrawerOpen();
                binding.tvNavHeading.setText("Home");
                loadFragment(new HomeFragment());
                break;
            case R.id.navMyAddressesnav:
                isDrawerOpen();
                binding.tvNavHeading.setText("My Addresses");
                loadFragment(new AddressFragment());
                break;
            case R.id.tvDealernav:
                isDrawerOpen();
                binding.tvNavHeading.setText("Dealer");
                loadFragment(new DealersFragment());
                break;
            case R.id.Ledgernav:
                isDrawerOpen();
                binding.tvNavHeading.setText("Ledger");
                loadFragment(new LedgerFragment());
                break;
            case R.id.tvEditProfile:
                isDrawerOpen();
                binding.tvNavHeading.setText("Profile");
                loadFragment(new ProfileFragment());
                break;
            case R.id.imgClose:
                isDrawerOpen();
                break;
            case R.id.navLogout:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle("Logout ?");
                builder1.setMessage("Are you sure want to logout?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                storageUtils.clearSharedPrefrence(MainActivity.this);
                                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
                break;
        }
    }

    public void isDrawerOpen() {
        if (binding.drower.isDrawerOpen(Gravity.LEFT)) {
            binding.drower.closeDrawer(Gravity.LEFT);
        } else {
            binding.drower.openDrawer(Gravity.LEFT);
        }
    }

    public void loadFragment(Fragment fragmnet) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragmnet);
        fragmentTransaction.commit();
    }
}