package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.spacetexhdemo.activity.BaseActivity;
import com.spacetexhdemo.databinding.ActivityPhoneNumberBinding;

import retrofit.ApiController;
import retrofit.ApiResponseListener;
import sharedprefrence.StorageUtils;
import utils.StringUtils;
import verify_account.VerifyAccountActivity;

public class PhoneNumberActivity extends BaseActivity implements ApiResponseListener {
    ActivityPhoneNumberBinding binding;
    String number;
    private ApiController apiController;
    StorageUtils storageUtils;
    Context context;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_phone_number);
        context = PhoneNumberActivity.this;
        apiController = new ApiController(this);
        storageUtils = new StorageUtils();

        binding.btnGetOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = binding.editNumber.getText().toString();
                progressDialog = new ProgressDialog(PhoneNumberActivity.this);
                progressDialog.setTitle("Loading.....");
                progressDialog.setCancelable(false);
                progressDialog.show();

                if (number.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(PhoneNumberActivity.this, "Enter phone number!", Toast.LENGTH_SHORT).show();
                } else {
                    apiController.getPhoneNumberOtpSend(number);
                }
            }
        });
    }

    @Override
    public void onSuccess(String tag, String superClassCastBean) {
        progressDialog.dismiss();
        startActivity(new Intent(PhoneNumberActivity.this, EnterOtpActivity.class)
                .putExtra("userNumber", number));
        storageUtils.setDetails(context, StringUtils.UserNumber, number);
        saveMobileNumber(number);

    }

    @Override
    public void onFailure(String msg) {
        progressDialog.dismiss();
        Toast.makeText(this, "Failure--" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String msg) {
        progressDialog.dismiss();
        Toast.makeText(this, "Error--" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPerformCode(int code) {

    }
}