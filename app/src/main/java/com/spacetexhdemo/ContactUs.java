package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ContactUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        TextView  emailV = findViewById(R.id.email);
        TextView  phoneV = findViewById(R.id.phone);

        emailV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);//common intent
                intent.setData(Uri.parse("mailto:crm4ggpl@guljag.com"));
                startActivity(intent);

            }
        });

        phoneV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+919772500220"));
                startActivity(intent);
            }
        });
    }
}