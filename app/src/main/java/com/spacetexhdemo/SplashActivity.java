package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import select_dealer.SelectDealerActivity;
import sharedprefrence.StorageUtils;
import utils.StringUtils;
import verify_account.Doctorprescription;
import verify_account.MedicalReport;
import verify_account.VerifyAccountActivity;

public class SplashActivity extends AppCompatActivity {
    StorageUtils storageUtils;
    Context context;
    String user, accVerified, medicalReportUpload, aadharCardUpload, doctorPrescriptionUpload, selectCylinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;
        storageUtils = new StorageUtils();

        user = storageUtils.getDetails(context, StringUtils.NewUser);
        accVerified = storageUtils.getDetails(context, StringUtils.AccountVerified);
        medicalReportUpload = storageUtils.getDetails(context, StringUtils.MedicalReport);
        aadharCardUpload = storageUtils.getDetails(context, StringUtils.AadharCardUpload);
        doctorPrescriptionUpload = storageUtils.getDetails(context, StringUtils.DoctorPrescription);
        selectCylinder = storageUtils.getDetails(context, StringUtils.SelectCylinder);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!user.isEmpty()) {
                    if (accVerified.equals(StringUtils.Yes)) {

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
//                                        startActivity(new Intent(SplashActivity.this, VerifyAccountActivity.class));

                    } else {
                        startActivity(new Intent(SplashActivity.this, VerifyAccountActivity.class));
                        finish();
                    }

                } else {
                    startActivity(new Intent(SplashActivity.this, PhoneNumberActivity.class));
                    finish();
                }
            }
        }, 1200);


    }


}