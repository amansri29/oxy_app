package com.spacetexhdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.spacetexhdemo.databinding.ActivityAddMannulaBinding;


import adapters.PlacesAutoCompleteAdapter;

public class AddMannulaAddressActivity extends AppCompatActivity implements PlacesAutoCompleteAdapter.ClickListener {
    ActivityAddMannulaBinding binding;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
//    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_mannula);

        Places.initialize(this, getResources().getString(R.string.google_maps_key));

        binding.editSearchAddress.addTextChangedListener(filterTextWatcher);


        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);

        binding.placesRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        binding.placesRecyclerview.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (binding.placesRecyclerview.getVisibility() == View.GONE) {
                    binding.placesRecyclerview.setVisibility(View.VISIBLE);
                }
            } else {
                if (binding.placesRecyclerview.getVisibility() == View.VISIBLE) {
                    binding.placesRecyclerview.setVisibility(View.GONE);
                }
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    };

    @Override
    public void click(Place place) {
        Intent intent = new Intent();
        intent.putExtra("MESSAGE", place.getAddress());
        intent.putExtra("LATLNG",place.getLatLng());
        setResult(2, intent);
        finish();//finishing activity
        Toast.makeText(this, place.getAddress() + ", " +
                "" + place.getLatLng().latitude + place.getLatLng().longitude, Toast.LENGTH_SHORT).show();
    }

}