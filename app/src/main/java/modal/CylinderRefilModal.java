package modal;

public class CylinderRefilModal {
    private String id;
    private String cylinderName;
    private String cylinderPrice;
    private String quantiy;
    private String size;

    public CylinderRefilModal() {

    }

    public CylinderRefilModal(String id, String cylinderName, String cylinderPrice, String quantiy, String size) {
        this.id = id;
        this.cylinderName = cylinderName;
        this.cylinderPrice = cylinderPrice;
        this.quantiy = quantiy;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCylinderName() {
        return cylinderName;
    }

    public void setCylinderName(String cylinderName) {
        this.cylinderName = cylinderName;
    }

    public String getCylinderPrice() {
        return cylinderPrice;
    }

    public void setCylinderPrice(String cylinderPrice) {
        this.cylinderPrice = cylinderPrice;
    }

    public String getQuantiy() {
        return quantiy;
    }

    public void setQuantiy(String quantiy) {
        this.quantiy = quantiy;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
