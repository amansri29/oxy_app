package modal;

public class DealersModal {
    private String dealersId;
    private String dealersName;
    private String dealersNumber;
    private String dealersAddress;
    private String dealerDistance;

    public DealersModal() {

    }

    public DealersModal(String dealersId, String dealersName, String dealersNumber,
                        String dealersAddress, String dealerDistance) {
        this.dealersId = dealersId;
        this.dealersName = dealersName;
        this.dealersNumber = dealersNumber;
        this.dealersAddress = dealersAddress;
        this.dealerDistance = dealerDistance;
    }

    public String getDealersId() {
        return dealersId;
    }

    public void setDealersId(String dealersId) {
        this.dealersId = dealersId;
    }

    public String getDealersName() {
        return dealersName;
    }

    public void setDealersName(String dealersName) {
        this.dealersName = dealersName;
    }

    public String getDealersNumber() {
        return dealersNumber;
    }

    public void setDealersNumber(String dealersNumber) {
        this.dealersNumber = dealersNumber;
    }

    public String getDealersAddress() {
        return dealersAddress;
    }

    public void setDealersAddress(String dealersAddress) {
        this.dealersAddress = dealersAddress;
    }

    public String getDealerDistance() {
        return dealerDistance;
    }

    public void setDealerDistance(String dealerDistance) {
        this.dealerDistance = dealerDistance;
    }
}
