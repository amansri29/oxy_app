package modal;

import java.util.List;

public class PlacedOrderModal {

    /**
     * id : 87
     * items : [{"id":88,"created":"2021-07-08T08:36:24.857672+05:30","last_updated":"2021-07-08T08:36:24.857703+05:30","item_name":"standard","amount":120,"quantity":2,"item":33},{"id":89,"created":"2021-07-08T08:36:24.867784+05:30","last_updated":"2021-07-08T08:36:24.867813+05:30","item_name":"standard","amount":130,"quantity":1,"item":34}]
     * total_amount : 150
     * order_type : New
     * expected_delivery_date : 2020-10-10
     * repeat_order_days : 20
     * repeat_till : 2020-10-12
     * delivery_address : 1
     * payment_mode : Cash on delivery
     * status : Payment Pending
     */

    private int id;
    private int total_amount;
    private String order_type;
    private String expected_delivery_date;
    private int repeat_order_days;
    private String repeat_till;
    private int delivery_address;
    private String payment_mode;
    private String status;
    private List<ItemsBean> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public int getRepeat_order_days() {
        return repeat_order_days;
    }

    public void setRepeat_order_days(int repeat_order_days) {
        this.repeat_order_days = repeat_order_days;
    }

    public String getRepeat_till() {
        return repeat_till;
    }

    public void setRepeat_till(String repeat_till) {
        this.repeat_till = repeat_till;
    }

    public int getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(int delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * id : 88
         * created : 2021-07-08T08:36:24.857672+05:30
         * last_updated : 2021-07-08T08:36:24.857703+05:30
         * item_name : standard
         * amount : 120
         * quantity : 2
         * item : 33
         */

        private int id;
        private String created;
        private String last_updated;
        private String item_name;
        private int amount;
        private int quantity;
        private int item;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getItem() {
            return item;
        }

        public void setItem(int item) {
            this.item = item;
        }
    }
}
