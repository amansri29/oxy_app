package modal;

public class UpdateOrderPaymentId {
    int payment_id;
    private String status;

    public UpdateOrderPaymentId() {
    }

    public UpdateOrderPaymentId(int payment_id) {
        this.payment_id = payment_id;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
