package modal;

public class UpdateOrder {
    private String payment_mode;
    String expected_delivery_date;
    String payment_id;
    String status;

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
