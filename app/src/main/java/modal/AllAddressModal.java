package modal;

public class AllAddressModal {

    /**
     * id : 81
     * created : 2021-06-30T23:05:59.948709+05:30
     * last_updated : 2021-06-30T23:05:59.948746+05:30
     * basic_address : 64/467, Tirupati Balaji Nagar, Sanganer, Maruti Nagar, Jaipur, Rajasthan 303301, India
     * landmark : b93
     * city : Jaipur
     * pincode : 303301
     * state : Rajasthan
     * country : India
     * latitude : 26.811638
     * longitude : 75.810938
     */

    private int id;
    private String created;
    private String last_updated;
    private String basic_address;
    private String landmark;
    private String city;
    private String pincode;
    private String state;
    private String country;
    private double latitude;
    private double longitude;

    public AllAddressModal(int id, String created, String last_updated, String basic_address,
                           String landmark, String city, String pincode,
                           String state, String country,
                           double latitude, double longitude) {
        this.id = id;
        this.created = created;
        this.last_updated = last_updated;
        this.basic_address = basic_address;
        this.landmark = landmark;
        this.city = city;
        this.pincode = pincode;
        this.state = state;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getBasic_address() {
        return basic_address;
    }

    public void setBasic_address(String basic_address) {
        this.basic_address = basic_address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
