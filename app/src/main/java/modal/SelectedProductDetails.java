package modal;

import java.io.Serializable;

public class SelectedProductDetails implements Serializable {
    String productName;
    String productType;
    String itemPrice;
    int quantity;
    private float securityAmount;

    public SelectedProductDetails() {

    }

    public SelectedProductDetails(String productName, String productType, String itemPrice, int quantity, float securityAmount) {
        this.productName = productName;
        this.productType = productType;
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.securityAmount = securityAmount;
    }

    public float getSecurityAmount() {
        return securityAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
