package modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderItem implements Serializable {
    @SerializedName("product_name")
    String name;
    int item;
    int quantity;
    float security_deposit;
    @SerializedName("amount")
    String itemPrice;
    @SerializedName("item_name")
    String itemType;

    public OrderItem(int item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public float getSecurity_deposit() {
        return security_deposit;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public String getItemType() {
        return itemType;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "name='" + name + '\'' +
                ", item=" + item +
                ", quantity=" + quantity +
                ", security_deposit=" + security_deposit +
                ", itemPrice='" + itemPrice + '\'' +
                ", itemType='" + itemType + '\'' +
                '}';
    }
}
