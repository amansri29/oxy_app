package modal;

public class AddressModal {
    private String deliveryAddressId;
    private String deliveryAddressName;

    public AddressModal() {

    }

    public AddressModal(String deliveryAddressId, String deliveryAddressName) {
        this.deliveryAddressId = deliveryAddressId;
        this.deliveryAddressName = deliveryAddressName;
    }

    public String getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(String deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public String getDeliveryAddressName() {
        return deliveryAddressName;
    }

    public void setDeliveryAddressName(String deliveryAddressName) {
        this.deliveryAddressName = deliveryAddressName;
    }
}
