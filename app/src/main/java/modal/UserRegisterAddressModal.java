package modal;

public class UserRegisterAddressModal {

    /**
     * id : 3
     * created : 2021-06-17T17:34:15.277299+05:30
     * last_updated : 2021-06-17T17:34:15.277335+05:30
     * basic_address : backjsdfhkjsd
     * landmark : jaipur
     * city : kota
     * pincode : 1231212
     * state : raj
     * country : ind
     * latitude : 31.11
     * longitude : 33.11
     */

    private int id;
    private String created;
    private String last_updated;
    private String basic_address;
    private String landmark;
    private String city;
    private int pincode;
    private String state;
    private String country;
    private double latitude;
    private double longitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getBasic_address() {
        return basic_address;
    }

    public void setBasic_address(String basic_address) {
        this.basic_address = basic_address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
