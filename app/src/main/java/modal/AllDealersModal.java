package modal;

import com.google.gson.annotations.Expose;

import java.util.List;

public class AllDealersModal {

    /**
     * id : 4
     * dealer_address : {"id":1,"created":"2021-06-24T22:31:59.176244+05:30","last_updated":"2021-06-25T08:49:01.669441+05:30","basic_address":"F-11, Nemi Nagar, Vaishali Nagar","landmark":"Near Yes bank","city":"Jaipur","pincode":302021,"state":"Rajasthan","country":"India","latitude":91,"longitude":98.09}
     * distance : 5.07
     * products : [{"id":8,"type":[{"id":5,"price":800,"created":"2021-06-25T08:49:40.368558+05:30","last_updated":"2021-06-25T08:49:40.368589+05:30","name":"B"},{"id":6,"price":900,"created":"2021-06-25T08:49:59.400121+05:30","last_updated":"2021-06-25T08:49:59.400152+05:30","name":"C"},{"id":7,"price":1100,"created":"2021-06-25T08:50:06.592044+05:30","last_updated":"2021-06-25T08:50:06.592073+05:30","name":"D"}],"created":"2021-06-25T08:50:11.808418+05:30","last_updated":"2021-06-25T08:50:11.808449+05:30","icon":"/upload/files/timetable.png","name":"Oxygen Cylinder","description":"","item_code":null,"blocked":false},{"id":10,"type":[{"id":9,"price":55,"created":"2021-06-25T08:50:47.884942+05:30","last_updated":"2021-06-25T08:50:47.884972+05:30","name":"standard"}],"created":"2021-06-25T08:50:52.813083+05:30","last_updated":"2021-06-25T08:50:52.813112+05:30","icon":"/upload/files/remove-user_1.png","name":"Mask","description":"","item_code":null,"blocked":false},{"id":11,"type":[{"id":9,"price":100,"created":"2021-06-25T08:50:47.884942+05:30","last_updated":"2021-06-25T08:50:47.884972+05:30","name":"standard"}],"created":"2021-06-25T08:51:14.499355+05:30","last_updated":"2021-06-25T08:51:14.499384+05:30","icon":"/upload/files/unloack_WxCUvKK.png","name":"Water Bottel","description":"","item_code":null,"blocked":false}]
     * created : 2021-06-24T22:32:51.230343+05:30
     * last_updated : 2021-06-25T08:51:32.181811+05:30
     * company_name : Texttt Dealer
     * mobile : 9828287274
     * owner_name : null
     * dealer_code : null
     * gstn_no : null
     * pan_no : null
     * blocked : false
     * user : null
     */

    private int id;
    private DealerAddressBean dealer_address;
    @Expose(serialize = false)
    private double distance;
    private String created;
    private String last_updated;
    private String company_name;
    private String mobile;
    private Object owner_name;
    private Object dealer_code;
    private Object gstn_no;
    private Object pan_no;
    private boolean blocked;
    private Object user;
    private List<ProductsBean> products;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DealerAddressBean getDealer_address() {
        return dealer_address;
    }

    public void setDealer_address(DealerAddressBean dealer_address) {
        this.dealer_address = dealer_address;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Object getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(Object owner_name) {
        this.owner_name = owner_name;
    }

    public Object getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(Object dealer_code) {
        this.dealer_code = dealer_code;
    }

    public Object getGstn_no() {
        return gstn_no;
    }

    public void setGstn_no(Object gstn_no) {
        this.gstn_no = gstn_no;
    }

    public Object getPan_no() {
        return pan_no;
    }

    public void setPan_no(Object pan_no) {
        this.pan_no = pan_no;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public List<ProductsBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsBean> products) {
        this.products = products;
    }

    public static class DealerAddressBean {
        /**
         * id : 1
         * created : 2021-06-24T22:31:59.176244+05:30
         * last_updated : 2021-06-25T08:49:01.669441+05:30
         * basic_address : F-11, Nemi Nagar, Vaishali Nagar
         * landmark : Near Yes bank
         * city : Jaipur
         * pincode : 302021
         * state : Rajasthan
         * country : India
         * latitude : 91
         * longitude : 98.09
         */

        private int id;
        private String created;
        private String last_updated;
        private String basic_address;
        private String landmark;
        private String city;
        private int pincode;
        private String state;
        private String country;
        private double latitude;
        private double longitude;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getBasic_address() {
            return basic_address;
        }

        public void setBasic_address(String basic_address) {
            this.basic_address = basic_address;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(int latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }

    public static class ProductsBean {
        /**
         * id : 8
         * type : [{"id":5,"price":800,"created":"2021-06-25T08:49:40.368558+05:30","last_updated":"2021-06-25T08:49:40.368589+05:30","name":"B"},{"id":6,"price":900,"created":"2021-06-25T08:49:59.400121+05:30","last_updated":"2021-06-25T08:49:59.400152+05:30","name":"C"},{"id":7,"price":1100,"created":"2021-06-25T08:50:06.592044+05:30","last_updated":"2021-06-25T08:50:06.592073+05:30","name":"D"}]
         * created : 2021-06-25T08:50:11.808418+05:30
         * last_updated : 2021-06-25T08:50:11.808449+05:30
         * icon : /upload/files/timetable.png
         * name : Oxygen Cylinder
         * description :
         * item_code : null
         * blocked : false
         */

        private int id;
        private String created;
        private String last_updated;
        private String icon;
        private String name;
        private String description;
        private Object item_code;
        private boolean blocked;
        private List<TypeBean> type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getItem_code() {
            return item_code;
        }

        public void setItem_code(Object item_code) {
            this.item_code = item_code;
        }

        public boolean isBlocked() {
            return blocked;
        }

        public void setBlocked(boolean blocked) {
            this.blocked = blocked;
        }

        public List<TypeBean> getType() {
            return type;
        }

        public void setType(List<TypeBean> type) {
            this.type = type;
        }

        public static class TypeBean {
            /**
             * id : 5
             * price : 800
             * created : 2021-06-25T08:49:40.368558+05:30
             * last_updated : 2021-06-25T08:49:40.368589+05:30
             * name : B
             */

            private int id;
            private int price_id;
            private float price;
            private String created;
            private String last_updated;
            private String name;
            private float security_deposit;

            public float getSecurity_deposit() {
                return security_deposit;
            }

            public int getPrice_id() {
                return price_id;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public float getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public String getCreated() {
                return created;
            }

            public void setCreated(String created) {
                this.created = created;
            }

            public String getLast_updated() {
                return last_updated;
            }

            public void setLast_updated(String last_updated) {
                this.last_updated = last_updated;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
