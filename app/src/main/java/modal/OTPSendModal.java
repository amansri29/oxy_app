package modal;

public class OTPSendModal {

    /**
     * msg : OTP send
     */

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
