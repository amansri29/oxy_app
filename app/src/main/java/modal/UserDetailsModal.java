package modal;

public class UserDetailsModal {


    /**
     * id : 10
     * created : 2021-06-19T16:15:58.597650+05:30
     * last_updated : 2021-06-19T16:15:58.597686+05:30
     * name : dee
     * mobile : 8619368171
     * slug : dee-4
     * dob : 2017-06-17
     * gender : Male
     * aadhar_number : null
     * medical_report : null
     * prescription_document : null
     * user : null
     * default_address : 4
     * prescribe_by : null
     */

    private int id;
    private String created;
    private String last_updated;
    private String name;
    private String mobile;
    private String slug;
    private String dob;
    private String gender;
    private Object aadhar_number;
    private Object medical_report;
    private Object prescription_document;
    private Object user;
    private int default_address;
    private Object prescribe_by;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getAadhar_number() {
        return aadhar_number;
    }

    public void setAadhar_number(Object aadhar_number) {
        this.aadhar_number = aadhar_number;
    }

    public Object getMedical_report() {
        return medical_report;
    }

    public void setMedical_report(Object medical_report) {
        this.medical_report = medical_report;
    }

    public Object getPrescription_document() {
        return prescription_document;
    }

    public void setPrescription_document(Object prescription_document) {
        this.prescription_document = prescription_document;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public int getDefault_address() {
        return default_address;
    }

    public void setDefault_address(int default_address) {
        this.default_address = default_address;
    }

    public Object getPrescribe_by() {
        return prescribe_by;
    }

    public void setPrescribe_by(Object prescribe_by) {
        this.prescribe_by = prescribe_by;
    }
}
