package modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllOrdersGetModal {

    /**
     * id : 410
     * order_customer : 82
     * items : [{"id":411,"product_name":"Oxygen Cylinder","created":"2021-07-26T23:29:09.393361+05:30","last_updated":"2021-07-26T23:29:09.393395+05:30","item_name":"B","amount":800,"security_deposit":0,"quantity":2,"item":17}]
     * total_amount : 31600
     * order_type : New
     * cylender_number : null
     * expected_delivery_date : 2021-07-29
     * repeat_order_days : 0
     * dealer_name :
     * delivery_code : 8775
     * repeat_till : null
     * delivery_address : 364
     * payment_mode : Cash on delivery
     * status : Payment Pending
     * payment_token : null
     * customer_name : DEEPENDRA SINGH
     * address_details : {"id":364,"created":"2021-07-21T10:38:47.804886+05:30","last_updated":"2021-07-21T10:38:47.804922+05:30","basic_address":"Jaipur, Rajasthan, India","landmark":"b-93)","city":"Dausa","pincode":null,"state":"Rajasthan","country":"India","latitude":27.023422399999998,"longitude":76.8800565}
     */

    private int id;
    private int order_customer;
    private int total_amount;
    private String order_type;
    private Object cylender_number;
    private String expected_delivery_date;
    private int repeat_order_days;
    private String dealer_name;
    private String delivery_code;
    private Object repeat_till;
    private int delivery_address;
    private String payment_mode;
    private String status;
    private Object payment_token;
    private String customer_name;
    private AddressDetailsBean address_details;
    private List<ItemsBean> items;
    private List<ItemsBean> securityItems;
    @Expose(serialize = false, deserialize = false)
    private boolean showRefundButton = false;
    @SerializedName("return_date")
    private String returnDate;
    @Expose(serialize = false, deserialize = false)
    private String prettyReturnDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_customer() {
        return order_customer;
    }

    public void setOrder_customer(int order_customer) {
        this.order_customer = order_customer;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public Object getCylender_number() {
        return cylender_number;
    }

    public void setCylender_number(Object cylender_number) {
        this.cylender_number = cylender_number;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public int getRepeat_order_days() {
        return repeat_order_days;
    }

    public void setRepeat_order_days(int repeat_order_days) {
        this.repeat_order_days = repeat_order_days;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getDelivery_code() {
        return delivery_code;
    }

    public void setDelivery_code(String delivery_code) {
        this.delivery_code = delivery_code;
    }

    public Object getRepeat_till() {
        return repeat_till;
    }

    public void setRepeat_till(Object repeat_till) {
        this.repeat_till = repeat_till;
    }

    public int getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(int delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(Object payment_token) {
        this.payment_token = payment_token;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public AddressDetailsBean getAddress_details() {
        return address_details;
    }

    public void setAddress_details(AddressDetailsBean address_details) {
        this.address_details = address_details;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public void addSecurityDeposit(int securityDeposit){
        items.add(new ItemsBean("Security Amount","S",securityDeposit,1,items.get(items.size()-1).getCreated()));
    }

    public boolean isShowRefundButton() {
        return showRefundButton;
    }

    public void setShowRefundButton(boolean showRefundButton) {
        this.showRefundButton = showRefundButton;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public List<ItemsBean> getSecurityItems() {
        return securityItems;
    }

    public void setSecurityItems(List<ItemsBean> securityItems) {
        this.securityItems = securityItems;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public void setPrettyReturnDate(String prettyReturnDate) {
        this.prettyReturnDate = prettyReturnDate;
    }

    public String getPrettyReturnDate() {
        return prettyReturnDate;
    }

    public static class AddressDetailsBean {
        /**
         * id : 364
         * created : 2021-07-21T10:38:47.804886+05:30
         * last_updated : 2021-07-21T10:38:47.804922+05:30
         * basic_address : Jaipur, Rajasthan, India
         * landmark : b-93)
         * city : Dausa
         * pincode : null
         * state : Rajasthan
         * country : India
         * latitude : 27.023422399999998
         * longitude : 76.8800565
         */

        private int id;
        private String created;
        private String last_updated;
        private String basic_address;
        private String landmark;
        private String city;
        private Object pincode;
        private String state;
        private String country;
        private double latitude;
        private double longitude;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getBasic_address() {
            return basic_address;
        }

        public void setBasic_address(String basic_address) {
            this.basic_address = basic_address;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Object getPincode() {
            return pincode;
        }

        public void setPincode(Object pincode) {
            this.pincode = pincode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }


    }

    public static class ItemsBean {
        /**
         * id : 411
         * product_name : Oxygen Cylinder
         * created : 2021-07-26T23:29:09.393361+05:30
         * last_updated : 2021-07-26T23:29:09.393395+05:30
         * item_name : B
         * amount : 800
         * security_deposit : 0
         * quantity : 2
         * item : 17
         */

        private int id;
        private String product_name;
        private String created;
        private String last_updated;
        private String item_name;
        private int amount;
        private int security_deposit;
        private int quantity;
        private int item;

        public ItemsBean(String product_name, String item_name, int amount, int quantity, String created) {
            this.product_name = product_name;
            this.item_name = item_name;
            this.amount = amount;
            this.quantity = quantity;
            this.created = created;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getSecurity_deposit() {
            return security_deposit;
        }

        public void setSecurity_deposit(int security_deposit) {
            this.security_deposit = security_deposit;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getItem() {
            return item;
        }

        public void setItem(int item) {
            this.item = item;
        }
    }
}
